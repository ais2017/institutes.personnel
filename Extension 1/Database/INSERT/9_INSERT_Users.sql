/* Пользователи */
	/* Первое подразделение */
	INSERT INTO public.user_t(
		id, personid, login, password, role) VALUES
			(DEFAULT, 8, 'NKOLESNIKOVA', 'NKOLESNIKOVA', 'Руководитель ОК'),
			(DEFAULT, 9, 'LFILIMONOV', 'LFILIMONOV', 'Сотрудник ОК');
			
	/* Второе подразделение */
	INSERT INTO public.user_t(
		id, personid, login, password, role) VALUES
			(DEFAULT, 15, 'AIBRAGIMOVA', 'AIBRAGIMOVA', 'Сотрудник ОК');
			
	/* Третье подразделение */
	INSERT INTO public.user_t(
		id, personid, login, password, role) VALUES
			(DEFAULT, 19, 'GCHERNYKH', 'GCHERNYKH', 'Администратор'),
			(DEFAULT, 20, 'YKURAEV', 'YKURAEV', 'Сотрудник ОК');