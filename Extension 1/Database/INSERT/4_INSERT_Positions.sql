/* Первое Подразделение */
INSERT INTO public.position_t(
	id, divisionid, positionname, maxemployees, vacantpositions) VALUES
		(DEFAULT, 1, 'Преподаватель', 10, 7),
		(DEFAULT, 1, 'Декан', 1, 0),
		(DEFAULT, 1, 'Секретарь', 5, 2),
		(DEFAULT, 1, 'Руководитель ОК', 1, 0),
		(DEFAULT, 1, 'Сотрудник ОК', 1, 0);
	
/* Второе Подразделение */
INSERT INTO public.position_t(
	id, divisionid, positionname, maxemployees, vacantpositions) VALUES
		(DEFAULT, 2, 'Преподаватель', 10, 5),
		(DEFAULT, 2, 'Сотрудник ОК', 3, 1);
	
/* Третье Подразделение */
INSERT INTO public.position_t(
	id, divisionid, positionname, maxemployees, vacantpositions) VALUES
		(DEFAULT, 3, 'Преподаватель', 15, 13),
		(DEFAULT, 3, 'Администратор', 3, 2),
		(DEFAULT, 3, 'Сотрудник ОК', 1, 0);