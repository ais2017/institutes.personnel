/* Первое Подразделение */
INSERT INTO public.division_t(
	id, organizationid, divisioncode, divisionname)
	VALUES (DEFAULT, 1, '0101', 'Первое подразделение');
	
/* Второе Подразделение */
INSERT INTO public.division_t(
	id, organizationid, divisioncode, divisionname)
	VALUES (DEFAULT, 1, '0102', 'Второе подразделение');
	
/* Третье Подразделение */
INSERT INTO public.division_t(
	id, organizationid, divisioncode, divisionname)
	VALUES (DEFAULT, 1, '0103', 'Третье подразделение');