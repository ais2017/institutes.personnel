/* Пользователи */
	/* Первое подразделение */
	INSERT INTO public.user_t(
		id, personid, login, password, role) VALUES
			(1, 8, 'NKOLESNIKOVA', 'NKOLESNIKOVA', 'HR Manager'),
			(2, 9, 'LFILIMONOV', 'LFILIMONOV', 'HR Employee');
			
	/* Второе подразделение */
	INSERT INTO public.user_t(
		id, personid, login, password, role) VALUES
			(3, 15, 'AIBRAGIMOVA', 'AIBRAGIMOVA', 'HR Employee');
			
	/* Третье подразделение */
	INSERT INTO public.user_t(
		id, personid, login, password, role) VALUES
			(4, 19, 'GCHERNYKH', 'GCHERNYKH', 'Administrator'),
			(5, 20, 'YKURAEV', 'YKURAEV', 'HR Employee');