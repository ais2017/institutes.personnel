/* Научные степени */
INSERT INTO public.sciencedegree_t(
	id, sciencedegree) VALUES
		(1, 'Отсутствует'),
		(2, 'Кандидат наук'),
		(3, 'Доктор наук');