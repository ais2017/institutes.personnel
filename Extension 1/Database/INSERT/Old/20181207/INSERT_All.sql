/* 1. Приказы о приеме на работу */
	/* Первое подразделение */
	INSERT INTO public.order_t(
		id, type)
		VALUES (1, 'Прием на работу');
			

			
	/* Второе подразделение */
	INSERT INTO public.order_t(
		id, type)
		VALUES (2, 'Прием на работу');
				
				
				
	/* Третье подразделение */
	INSERT INTO public.order_t(
		id, type)
		VALUES (3, 'Прием на работу');
		
		
		
/* 2. Организации */
	INSERT INTO public.organization_t(
		id)
		VALUES (1);
	
	
	
/* 3. Подразделения */
	/* Первое Подразделение */
	INSERT INTO public.division_t(
		id, organizationid, divisioncode, divisionname)
		VALUES (1, 1, '0101', 'Первое подразделение');
		
	/* Второе Подразделение */
	INSERT INTO public.division_t(
		id, organizationid, divisioncode, divisionname)
		VALUES (2, 1, '0102', 'Второе подразделение');
		
	/* Третье Подразделение */
	INSERT INTO public.division_t(
		id, organizationid, divisioncode, divisionname)
		VALUES (3, 1, '0103', 'Третье подразделение');
	
	
	
/* 4. Должности */
	/* Первое Подразделение */
	INSERT INTO public.position_t(
		id, divisionid, positionname, maxemployees, vacantpositions) VALUES
			(1, 1, 'Преподаватель', 10, 7),
			(2, 1, 'Декан', 1, 0),
			(3, 1, 'Секретарь', 5, 2),
			(4, 1, 'Руководитель ОК', 1, 0),
			(5, 1, 'Сотрудник ОК', 1, 0);
		
	/* Второе Подразделение */
	INSERT INTO public.position_t(
		id, divisionid, positionname, maxemployees, vacantpositions) VALUES
			(6, 2, 'Преподаватель', 10, 5),
			(7, 2, 'Сотрудник ОК', 3, 1);
		
	/* Третье Подразделение */
	INSERT INTO public.position_t(
		id, divisionid, positionname, maxemployees, vacantpositions) VALUES
			(8, 3, 'Преподаватель', 15, 13),
			(9, 3, 'Администратор', 3, 2),
			(10, 3, 'Сотрудник ОК', 1, 0);
		
		
		
/* 5. Сотрудники */
	/* Первое подразделение */
		/* Преподаватели */
		INSERT INTO public.person_t(
			id, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
			(1, 1, 'Сергеев', 'Владислав', 'Сергеевич', '1959-11-01', 4101, 401101, 101, 'Доцент', 'Кандидат наук', '2005-8-01', '2018-12-01', 'vsergeev@domain.com', ''),
			(2, 1, 'Иванов', 'Иван', 'Иванович', '1969-11-01', 4102, 401102, 102, 'Доцент', 'Кандидат наук', '2005-8-01', '2018-12-20', 'iivanov@domain.com', ''),
			(3, 1, 'Сидоров', 'Николай', 'Григорьевич', '1971-11-01', 4103, 401103, 103, 'Доцент', 'Кандидат наук', '2005-8-01', '2031-12-20', 'nsidorov@domain.com', '');
			
		/* Декан */
		INSERT INTO public.person_t(
			id, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
			(4, 2, 'Соколова', 'Алена', 'Геннадьевна', '1959-11-01', 4104, 401104, 104, 'Доцент', 'Кандидат наук', '2007-8-01', '2031-12-01', 'asokolova@domain.com', '');
			
		/* Секретари */
		INSERT INTO public.person_t(
			id, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
			(5, 3, 'Фролова', 'Татьяна', 'Валерьевна', '1959-11-01', 4105, 401105, 105, 'Отсутствует', 'Отсутствует', '2005-8-01', '2031-12-01', 'tfrolova@domain.com', ''),
			(6, 3, 'Степанова', 'Богдана', 'Валерьевна', '1959-11-01', 4106, 401106, 106, 'Отсутствует', 'Отсутствует', '2005-8-01', '2018-12-09', 'tfrolova@domain.com', ''),
			(7, 3, 'Аникеева', 'Валерия', 'Валерьевна', '1959-11-01', 4107, 401107, 107, 'Отсутствует', 'Отсутствует', '2005-8-01', '2018-12-19', 'vanikeeva@domain.com', '');
			
		/* Руководитель ОК */
		INSERT INTO public.person_t(
			id, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
			(8, 4, 'Колесникова', 'Ника', 'Семеновна', '1959-11-01', 4108, 401108, 108, 'Отсутствует', 'Отсутствует', '2007-8-01', '2031-12-01', 'nkolesnikova@domain.com', '');
			
		/* Сотрудник ОК */
		INSERT INTO public.person_t(
			id, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
			(9, 5, 'Филимонов', 'Лука', 'Геннадьевич', '1959-11-01', 4109, 401109, 109, 'Отсутствует', 'Отсутствует', '2007-8-01', '2031-12-01', 'lfilimonov@domain.com', '');
		

		
	/* Второе подразделение */
		/* Преподаватели */
		INSERT INTO public.person_t(
			id, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
			(10, 6, 'Владиславов', 'Владислав', 'Сергеевич', '1959-11-01', 4201, 402101, 201, 'Доцент', 'Кандидат наук', '2005-8-01', '2018-12-01', 'vvladislavov@domain.com', ''),
			(11, 6, 'Березкин', 'Иван', 'Иванович', '1969-11-01', 4202, 402102, 202, 'Доцент', 'Кандидат наук', '2005-8-01', '2018-12-20', 'iberezkin@domain.com', ''),
			(12, 6, 'Липов', 'Николай', 'Григорьевич', '1971-11-01', 4203, 402103, 203, 'Доцент', 'Кандидат наук', '2005-8-01', '2031-12-20', 'nlipov@domain.com', ''),
			(13, 6, 'Пихтов', 'Антон', 'Олегович', '1969-11-01', 4204, 402104, 204, 'Доцент', 'Кандидат наук', '2005-8-01', '2018-12-20', 'apihtov@domain.com', ''),
			(14, 6, 'Облепихин', 'Николай', 'Григорьевич', '1971-11-01', 4205, 402105, 205, 'Доцент', 'Кандидат наук', '2005-8-01', '2031-12-20', 'noblepihin@domain.com', '');
			
		/* Сотрудники ОК */
		INSERT INTO public.person_t(
			id, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
			(15, 7, 'Ибрагимова', 'Агафья', 'Всеволодовна', '1959-11-01', 4206, 402106, 206, 'Отсутствует', 'Отсутствует', '2007-8-01', '2031-12-01', 'aibragimova@domain.com', ''),
			(16, 7, 'Абоймова', 'Кира', 'Александровна', '1959-11-01', 4207, 402107, 207, 'Отсутствует', 'Отсутствует', '2007-8-01', '2031-12-01', 'kaboimova@domain.com', '');
			

		
	/* Третье подразделение */
		/* Преподаватели */
		INSERT INTO public.person_t(
			id, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
			(17, 8, 'Новичкова', 'Ольга', 'Никитевна', '1959-11-01', 4301, 403101, 301, 'Доцент', 'Кандидат наук', '2005-8-01', '2018-12-01', 'onovichkova@domain.com', ''),
			(18, 8, 'Лобов', 'Август', 'Геннадьевич', '1969-11-01', 4302, 403102, 302, 'Доцент', 'Кандидат наук', '2005-8-01', '2031-12-20', 'alobov@domain.com', '');
			
		/* Администраторы */
		INSERT INTO public.person_t(
			id, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
			(19, 9, 'Черных', 'Гавриил', 'Макарович', '1959-11-01', 4303, 403103, 303, 'Отсутствует', 'Отсутствует', '2005-8-01', '2038-12-01', 'gchernykh@domain.com', '');
			
		/* Сотрудники ОК */
		INSERT INTO public.person_t(
			id, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
			(20, 10, 'Кураев', 'Ярослав', 'Давыдович', '1959-11-01', 4304, 403104, 304, 'Отсутствует', 'Отсутствует', '2007-8-01', '2031-12-01', 'ykuraev@domain.com', '');
		

		
/* 6. Строки приказов о приеме на работу */
	/* Первое подразделение */
		--Преподаватели
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES 
				(1, 1, 1, 1, 1, '2005-08-01', '2018-12-01'),
				(2, 1, 1, 1, 2, '2005-08-01', '2018-12-20'),
				(3, 1, 1, 1, 3, '2005-08-01', '2031-12-20');
			
		--Декан	
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES
				(4, 1, 1, 2, 4, '2007-08-01', '2031-12-01');
			
		--Секретарь	
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES
				(5, 1, 1, 3, 5, '2005-08-01', '2031-12-01'),
				(6, 1, 1, 3, 6, '2005-08-01', '2018-12-09'),
				(7, 1, 1, 3, 7, '2005-08-01', '2018-12-19');
		
		--Руководитель ОК
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES
				(8, 1, 1, 4, 8, '2007-08-01', '2031-12-01');
		
		--Сотрудник ОК
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES
				(9, 1, 1, 5, 9, '2007-08-01', '2031-12-01');
			

			
	/* Второе подразделение */
		--Преподаватели
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES 
				(10, 2, 2, 6, 10, '2005-08-01', '2018-12-01'),
				(11, 2, 2, 6, 11, '2005-08-01', '2018-12-20'),
				(12, 2, 2, 6, 12, '2005-08-01', '2031-12-20'),
				(13, 2, 2, 6, 13, '2005-08-01', '2018-12-20'),
				(14, 2, 2, 6, 14, '2005-08-01', '2031-12-20');
				
		--Сотрудник ОК
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES
				(15, 2, 2, 7, 15, '2007-08-01', '2031-12-01'),
				(16, 2, 2, 7, 16, '2007-08-01', '2031-12-01');
				
				
				
	/* Третье подразделение */
		--Преподаватели
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES 
				(17, 3, 3, 8, 17, '2005-08-01', '2018-12-01'),
				(18, 3, 3, 8, 18, '2005-08-01', '2018-12-20');
				
		--Администратор
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES
				(19, 3, 3, 9, 19, '2007-08-01', '2038-12-01');
				
		--Сотрудник ОК
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES
				(20, 3, 3, 10, 20, '2007-08-01', '2031-12-01');
				
				
				
/* 7. Научные звания */
	INSERT INTO public.scientifictitle_t(
		id, scientifictitle) VALUES
			(1, 'Отсутствует'),
			(2, 'Доцент'),
			(3, 'Профессор');
			
			
			
/* 8. Научные степени */
	INSERT INTO public.sciencedegree_t(
		id, sciencedegree) VALUES
			(1, 'Отсутствует'),
			(2, 'Кандидат наук'),
			(3, 'Доктор наук');
			
			
			
/* 9. Пользователи */
	/* Первое подразделение */
	INSERT INTO public.user_t(
		id, personid, login, password, role) VALUES
			(1, 8, 'NKOLESNIKOVA', 'NKOLESNIKOVA', 'HR Manager'),
			(2, 9, 'LFILIMONOV', 'LFILIMONOV', 'HR Employee');
			
	/* Второе подразделение */
	INSERT INTO public.user_t(
		id, personid, login, password, role) VALUES
			(3, 15, 'AIBRAGIMOVA', 'AIBRAGIMOVA', 'HR Employee');
			
	/* Третье подразделение */
	INSERT INTO public.user_t(
		id, personid, login, password, role) VALUES
			(4, 19, 'GCHERNYKH', 'GCHERNYKH', 'Administrator'),
			(5, 20, 'YKURAEV', 'YKURAEV', 'HR Employee');