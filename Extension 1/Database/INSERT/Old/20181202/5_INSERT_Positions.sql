/* Первое Подразделение */
INSERT INTO public.position_t(
	id, divisionid, positionname, maxemployees, vacantpositions) VALUES
		(1, 1, 'Преподаватель', 10, 7),
		(2, 1, 'Декан', 1, 0),
		(3, 1, 'Секретарь', 5, 2),
		(4, 1, 'Руководитель ОК', 1, 0),
		(5, 1, 'Сотрудник ОК', 1, 0);
	
/* Второе Подразделение */
INSERT INTO public.position_t(
	id, divisionid, positionname, maxemployees, vacantpositions) VALUES
		(6, 2, 'Преподаватель', 10, 5),
		(7, 2, 'Сотрудник ОК', 3, 1);
	
/* Третье Подразделение */
INSERT INTO public.position_t(
	id, divisionid, positionname, maxemployees, vacantpositions) VALUES
		(8, 3, 'Преподаватель', 15, 13),
		(9, 3, 'Администратор', 3, 2),
		(10, 3, 'Сотрудник ОК', 1, 0);