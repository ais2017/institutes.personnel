/* Приказы о приеме на работу */
	/* Первое подразделение */
	INSERT INTO public.ordert(
		id, type)
		VALUES (1, 'Прием на работу');
		
		/* Строки */
		--Преподаватели
		INSERT INTO public.orderlinet(
			id, orderid, startdate, enddate) VALUES 
				(1, 1, '2005-08-01', '2018-12-01'),
				(2, 1, '2005-08-01', '2018-12-20'),
				(3, 1, '2005-08-01', '2031-12-20');
			
		--Декан	
		INSERT INTO public.orderlinet(
			id, orderid, startdate, enddate) VALUES
				(4, 1, '2007-08-01', '2031-12-01');
			
		--Секретарь	
		INSERT INTO public.orderlinet(
			id, orderid, startdate, enddate) VALUES
				(5, 1, '2005-08-01', '2031-12-01'),
				(6, 1, '2005-08-01', '2018-12-09'),
				(7, 1, '2005-08-01', '2018-12-19');
		
		--Руководитель ОК
		INSERT INTO public.orderlinet(
			id, orderid, startdate, enddate) VALUES
				(8, 1, '2007-08-01', '2031-12-01');
		
		--Сотрудник ОК
		INSERT INTO public.orderlinet(
			id, orderid, startdate, enddate) VALUES
				(9, 1, '2007-08-01', '2031-12-01');
			

			
	/* Второе подразделение */
	INSERT INTO public.ordert(
		id, type)
		VALUES (2, 'Прием на работу');
		
		/* Строки */
		--Преподаватели
		INSERT INTO public.orderlinet(
			id, orderid, startdate, enddate) VALUES 
				(10, 2, '2005-08-01', '2018-12-01'),
				(11, 2, '2005-08-01', '2018-12-20'),
				(12, 2, '2005-08-01', '2031-12-20'),
				(13, 2, '2005-08-01', '2018-12-20'),
				(14, 2, '2005-08-01', '2031-12-20');
				
		--Сотрудник ОК
		INSERT INTO public.orderlinet(
			id, orderid, startdate, enddate) VALUES
				(15, 2, '2007-08-01', '2031-12-01'),
				(16, 2, '2007-08-01', '2031-12-01');
				
				
				
	/* Третье подразделение */
	INSERT INTO public.ordert(
		id, type)
		VALUES (3, 'Прием на работу');
		
		--Преподаватели
		INSERT INTO public.orderlinet(
			id, orderid, startdate, enddate) VALUES 
				(17, 3, '2005-08-01', '2018-12-01'),
				(18, 3, '2005-08-01', '2018-12-20');
				
		--Администратор
		INSERT INTO public.orderlinet(
			id, orderid, startdate, enddate) VALUES
				(19, 3, '2007-08-01', '2038-12-01');
				
		--Сотрудник ОК
		INSERT INTO public.orderlinet(
			id, orderid, startdate, enddate) VALUES
				(20, 3, '2007-08-01', '2031-12-01');