/* Первое Подразделение */
INSERT INTO public.positiont(
	id, divisionid, positionname, maxemployees, vacantpositions) VALUES
		(1, 1, 'Преподаватель', 10, 10),
		(2, 1, 'Декан', 1, 1),
		(3, 1, 'Секретарь', 5, 5),
		(4, 1, 'Руководитель ОК', 1, 1),
		(5, 1, 'Сотрудник ОК', 1, 1);
	
/* Второе Подразделение */
INSERT INTO public.positiont(
	id, divisionid, positionname, maxemployees, vacantpositions) VALUES
		(6, 2, 'Преподаватель', 10, 10),
		(7, 2, 'Сотрудник ОК', 3, 3);
	
/* Третье Подразделение */
INSERT INTO public.positiont(
	id, divisionid, positionname, maxemployees, vacantpositions) VALUES
		(8, 3, 'Преподаватель', 15, 15),
		(9, 3, 'Администратор', 3, 3),
		(10, 3, 'Сотрудник ОК', 1, 1);