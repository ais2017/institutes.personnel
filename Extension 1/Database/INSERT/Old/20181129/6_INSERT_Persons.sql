/* Первое подразделение */
	/* Преподаватели */
	INSERT INTO public.person_t(
		id, orderlineid, staffid, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
		(1, 1, 1, 1, 'Сергеев', 'Владислав', 'Сергеевич', '1959-11-01', 4101, 401101, 101, 'Доцент', 'Кандидат наук', '2005-8-01', '2018-12-01', 'vsergeev@domain.com', ''),
		(2, 2, 1, 1, 'Иванов', 'Иван', 'Иванович', '1969-11-01', 4102, 401102, 102, 'Доцент', 'Кандидат наук', '2005-8-01', '2018-12-20', 'iivanov@domain.com', ''),
		(3, 3, 1, 1, 'Сидоров', 'Николай', 'Григорьевич', '1971-11-01', 4103, 401103, 103, 'Доцент', 'Кандидат наук', '2005-8-01', '2031-12-20', 'nsidorov@domain.com', '');
		
	/* Декан */
	INSERT INTO public.person_t(
		id, orderlineid, staffid, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
		(4, 4, 1, 2, 'Соколова', 'Алена', 'Геннадьевна', '1959-11-01', 4104, 401104, 104, 'Доцент', 'Кандидат наук', '2007-8-01', '2031-12-01', 'asokolova@domain.com', '');
		
	/* Секретари */
	INSERT INTO public.person_t(
		id, orderlineid, staffid, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
		(5, 5, 1, 3, 'Фролова', 'Татьяна', 'Валерьевна', '1959-11-01', 4105, 401105, 105, 'Отсутствует', 'Отсутствует', '2005-8-01', '2031-12-01', 'tfrolova@domain.com', ''),
		(6, 6, 1, 3, 'Степанова', 'Богдана', 'Валерьевна', '1959-11-01', 4106, 401106, 106, 'Отсутствует', 'Отсутствует', '2005-8-01', '2018-12-09', 'tfrolova@domain.com', ''),
		(7, 7, 1, 3, 'Аникеева', 'Валерия', 'Валерьевна', '1959-11-01', 4107, 401107, 107, 'Отсутствует', 'Отсутствует', '2005-8-01', '2018-12-19', 'vanikeeva@domain.com', '');
		
	/* Руководитель ОК */
	INSERT INTO public.person_t(
		id, orderlineid, staffid, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
		(8, 8, 1, 4, 'Колесникова', 'Ника', 'Семеновна', '1959-11-01', 4108, 401108, 108, 'Отсутствует', 'Отсутствует', '2007-8-01', '2031-12-01', 'nkolesnikova@domain.com', '');
		
	/* Сотрудник ОК */
	INSERT INTO public.person_t(
		id, orderlineid, staffid, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
		(9, 9, 1, 5, 'Филимонов', 'Лука', 'Геннадьевич', '1959-11-01', 4109, 401109, 109, 'Отсутствует', 'Отсутствует', '2007-8-01', '2031-12-01', 'lfilimonov@domain.com', '');
	

	
/* Второе подразделение */
INSERT INTO public.person_t(
	id, orderlineid, staffid, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
	(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
	
	/* Преподаватели */
	INSERT INTO public.person_t(
		id, orderlineid, staffid, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
		(10, 10, 1, 6, 'Владиславов', 'Владислав', 'Сергеевич', '1959-11-01', 4201, 402101, 201, 'Доцент', 'Кандидат наук', '2005-8-01', '2018-12-01', 'vvladislavov@domain.com', ''),
		(11, 11, 1, 6, 'Березкин', 'Иван', 'Иванович', '1969-11-01', 4202, 402102, 202, 'Доцент', 'Кандидат наук', '2005-8-01', '2018-12-20', 'iberezkin@domain.com', ''),
		(12, 12, 1, 6, 'Липов', 'Николай', 'Григорьевич', '1971-11-01', 4203, 402103, 203, 'Доцент', 'Кандидат наук', '2005-8-01', '2031-12-20', 'nlipov@domain.com', ''),
		(13, 13, 1, 6, 'Пихтов', 'Антон', 'Олегович', '1969-11-01', 4204, 402104, 204, 'Доцент', 'Кандидат наук', '2005-8-01', '2018-12-20', 'apihtov@domain.com', ''),
		(14, 14, 1, 6, 'Облепихин', 'Николай', 'Григорьевич', '1971-11-01', 4205, 402105, 205, 'Доцент', 'Кандидат наук', '2005-8-01', '2031-12-20', 'noblepihin@domain.com', '');
		
	/* Сотрудники ОК */
	INSERT INTO public.person_t(
		id, orderlineid, staffid, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
		(15, 15, 1, 7, 'Ибрагимова', 'Агафья', 'Всеволодовна', '1959-11-01', 4206, 402106, 206, 'Отсутствует', 'Отсутствует', '2007-8-01', '2031-12-01', 'aibragimova@domain.com', ''),
		(16, 16, 1, 7, 'Абоймова', 'Кира', 'Александровна', '1959-11-01', 4207, 402107, 207, 'Отсутствует', 'Отсутствует', '2007-8-01', '2031-12-01', 'kaboimova@domain.com', '');
		

	
/* Третье подразделение */
	/* Преподаватели */
	INSERT INTO public.person_t(
		id, orderlineid, staffid, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
		(17, 17, 1, 8, 'Новичкова', 'Ольга', 'Никитевна', '1959-11-01', 4301, 403101, 301, 'Доцент', 'Кандидат наук', '2005-8-01', '2018-12-01', 'onovichkova@domain.com', ''),
		(18, 18, 1, 8, 'Лобов', 'Август', 'Геннадьевич', '1969-11-01', 4302, 403102, 302, 'Доцент', 'Кандидат наук', '2005-8-01', '2031-12-20', 'alobov@domain.com', '');
		
	/* Администраторы */
	INSERT INTO public.person_t(
		id, orderlineid, staffid, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
		(19, 19, 1, 9, 'Черных', 'Гавриил', 'Макарович', '1959-11-01', 4303, 403103, 303, 'Отсутствует', 'Отсутствует', '2005-8-01', '2038-12-01', 'gchernykh@domain.com', '');
		
	/* Сотрудники ОК */
	INSERT INTO public.person_t(
		id, orderlineid, staffid, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) VALUES
		(20, 20, 1, 10, 'Кураев', 'Ярослав', 'Давыдович', '1959-11-01', 4304, 403104, 304, 'Отсутствует', 'Отсутствует', '2007-8-01', '2031-12-01', 'ykuraev@domain.com', '');
		
