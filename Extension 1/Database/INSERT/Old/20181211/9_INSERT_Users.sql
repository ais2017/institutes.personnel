/* Пользователи */
	/* Первое подразделение */
	INSERT INTO public.user_t(
		id, personid, login, password, role) VALUES
			(DEFAULT, 8, 'NKOLESNIKOVA', 'NKOLESNIKOVA', 'HR Manager'),
			(DEFAULT, 9, 'LFILIMONOV', 'LFILIMONOV', 'HR Employee');
			
	/* Второе подразделение */
	INSERT INTO public.user_t(
		id, personid, login, password, role) VALUES
			(DEFAULT, 15, 'AIBRAGIMOVA', 'AIBRAGIMOVA', 'HR Employee');
			
	/* Третье подразделение */
	INSERT INTO public.user_t(
		id, personid, login, password, role) VALUES
			(DEFAULT, 19, 'GCHERNYKH', 'GCHERNYKH', 'Administrator'),
			(DEFAULT, 20, 'YKURAEV', 'YKURAEV', 'HR Employee');