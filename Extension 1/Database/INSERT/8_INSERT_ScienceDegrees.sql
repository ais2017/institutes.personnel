/* Научные степени */
INSERT INTO public.sciencedegree_t(
	id, sciencedegree) VALUES
		(DEFAULT, 'Отсутствует'),
		(DEFAULT, 'Кандидат наук'),
		(DEFAULT, 'Доктор наук');