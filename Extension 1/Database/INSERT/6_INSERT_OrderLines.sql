/* Строки приказов о приеме на работу */
	/* Первое подразделение */
		--Преподаватели
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES 
				(DEFAULT, 1, 1, 1, 1, '2005-08-01', '2018-12-01'),
				(DEFAULT, 1, 1, 1, 2, '2005-08-01', '2018-12-20'),
				(DEFAULT, 1, 1, 1, 3, '2005-08-01', '2031-12-20');
			
		--Декан	
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES
				(DEFAULT, 1, 1, 2, 4, '2007-08-01', '2031-12-01');
			
		--Секретарь	
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES
				(DEFAULT, 1, 1, 3, 5, '2005-08-01', '2031-12-01'),
				(DEFAULT, 1, 1, 3, 6, '2005-08-01', '2018-12-09'),
				(DEFAULT, 1, 1, 3, 7, '2005-08-01', '2018-12-19');
		
		--Руководитель ОК
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES
				(DEFAULT, 1, 1, 4, 8, '2007-08-01', '2031-12-01');
		
		--Сотрудник ОК
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES
				(DEFAULT, 1, 1, 5, 9, '2007-08-01', '2031-12-01');
			

			
	/* Второе подразделение */
		--Преподаватели
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES 
				(DEFAULT, 2, 2, 6, 10, '2005-08-01', '2018-12-01'),
				(DEFAULT, 2, 2, 6, 11, '2005-08-01', '2018-12-20'),
				(DEFAULT, 2, 2, 6, 12, '2005-08-01', '2031-12-20'),
				(DEFAULT, 2, 2, 6, 13, '2005-08-01', '2018-12-20'),
				(DEFAULT, 2, 2, 6, 14, '2005-08-01', '2031-12-20');
				
		--Сотрудник ОК
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES
				(DEFAULT, 2, 2, 7, 15, '2007-08-01', '2031-12-01'),
				(DEFAULT, 2, 2, 7, 16, '2007-08-01', '2031-12-01');
				
				
				
	/* Третье подразделение */
		--Преподаватели
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES 
				(DEFAULT, 3, 3, 8, 17, '2005-08-01', '2018-12-01'),
				(DEFAULT, 3, 3, 8, 18, '2005-08-01', '2018-12-20');
				
		--Администратор
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES
				(DEFAULT, 3, 3, 9, 19, '2007-08-01', '2038-12-01');
				
		--Сотрудник ОК
		INSERT INTO public.orderline_t(
			id, orderid, divisionid, positionid, personid, startdate, enddate) VALUES
				(DEFAULT, 3, 3, 10, 20, '2007-08-01', '2031-12-01');