--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

--CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

--COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: division_t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.division_t (
    id integer NOT NULL,
    organizationid integer NOT NULL,
    divisioncode character varying(10) NOT NULL,
    divisionname character varying(255) NOT NULL
);


--
-- Name: division_t_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.division_t_id_seq
--    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: division_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.division_t_id_seq OWNED BY public.division_t.id;


--
-- Name: order_t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.order_t (
    id integer NOT NULL,
    type character varying(100)
);


--
-- Name: order_t_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.order_t_id_seq
--    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.order_t_id_seq OWNED BY public.order_t.id;


--
-- Name: orderline_t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.orderline_t (
    id integer NOT NULL,
    orderid integer NOT NULL,
    divisionid integer NOT NULL,
    positionid integer NOT NULL,
    personid integer NOT NULL,
    startdate date NOT NULL,
    enddate date
);


--
-- Name: orderline_t_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.orderline_t_id_seq
--    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orderline_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.orderline_t_id_seq OWNED BY public.orderline_t.id;


--
-- Name: organization_t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.organization_t (
    id integer NOT NULL
);


--
-- Name: organization_t_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.organization_t_id_seq
--    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: organization_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.organization_t_id_seq OWNED BY public.organization_t.id;


--
-- Name: person_t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.person_t (
    id integer NOT NULL,
    positionid integer NOT NULL,
    lastname character varying(50) NOT NULL,
    firstname character varying(50) NOT NULL,
    middlename character varying(50) NOT NULL,
    birthday date NOT NULL,
    passportseries integer NOT NULL,
    passportnumber integer NOT NULL,
    personnelnumber integer NOT NULL,
    scientifictitle character varying(100) NOT NULL,
    sciencedegree character varying(100) NOT NULL,
    contractstartdate date NOT NULL,
    contractexpirationdate date,
    email character varying(50) NOT NULL,
    discriminator character varying(255) NOT NULL
);


--
-- Name: person_t_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.person_t_id_seq
--    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: person_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.person_t_id_seq OWNED BY public.person_t.id;


--
-- Name: position_t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.position_t (
    id integer NOT NULL,
    divisionid integer NOT NULL,
    positionname character varying(255) NOT NULL,
    maxemployees integer NOT NULL,
    vacantpositions integer NOT NULL
);


--
-- Name: position_t_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.position_t_id_seq
--    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: position_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.position_t_id_seq OWNED BY public.position_t.id;


--
-- Name: sciencedegree_t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sciencedegree_t (
    id integer NOT NULL,
    sciencedegree character varying(255) NOT NULL
);


--
-- Name: sciencedegree_t_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sciencedegree_t_id_seq
--    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sciencedegree_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sciencedegree_t_id_seq OWNED BY public.sciencedegree_t.id;


--
-- Name: scientifictitle_t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.scientifictitle_t (
    id integer NOT NULL,
    scientifictitle character varying(255) NOT NULL
);


--
-- Name: scientifictitle_t_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.scientifictitle_t_id_seq
--    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: scientifictitle_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.scientifictitle_t_id_seq OWNED BY public.scientifictitle_t.id;


--
-- Name: user_t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_t (
    id integer NOT NULL,
    personid integer NOT NULL,
    login character varying(50) NOT NULL,
    password character varying(50) NOT NULL,
    role character varying(50) NOT NULL
);


--
-- Name: user_t_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_t_id_seq
--    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_t_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_t_id_seq OWNED BY public.user_t.id;


--
-- Name: division_t id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.division_t ALTER COLUMN id SET DEFAULT nextval('public.division_t_id_seq'::regclass);


--
-- Name: order_t id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.order_t ALTER COLUMN id SET DEFAULT nextval('public.order_t_id_seq'::regclass);


--
-- Name: orderline_t id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orderline_t ALTER COLUMN id SET DEFAULT nextval('public.orderline_t_id_seq'::regclass);


--
-- Name: organization_t id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.organization_t ALTER COLUMN id SET DEFAULT nextval('public.organization_t_id_seq'::regclass);


--
-- Name: person_t id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.person_t ALTER COLUMN id SET DEFAULT nextval('public.person_t_id_seq'::regclass);


--
-- Name: position_t id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.position_t ALTER COLUMN id SET DEFAULT nextval('public.position_t_id_seq'::regclass);


--
-- Name: sciencedegree_t id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sciencedegree_t ALTER COLUMN id SET DEFAULT nextval('public.sciencedegree_t_id_seq'::regclass);


--
-- Name: scientifictitle_t id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scientifictitle_t ALTER COLUMN id SET DEFAULT nextval('public.scientifictitle_t_id_seq'::regclass);


--
-- Name: user_t id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_t ALTER COLUMN id SET DEFAULT nextval('public.user_t_id_seq'::regclass);


--
-- Data for Name: division_t; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.division_t (id, organizationid, divisioncode, divisionname) FROM stdin;
1	1	0101	Первое подразделение
2	1	0102	Второе подразделение
3	1	0103	Третье подразделение
\.


--
-- Data for Name: order_t; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.order_t (id, type) FROM stdin;
1	Прием на работу
2	Прием на работу
3	Прием на работу
\.


--
-- Data for Name: orderline_t; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.orderline_t (id, orderid, divisionid, positionid, personid, startdate, enddate) FROM stdin;
1	1	1	1	1	2005-08-01	2018-12-01
2	1	1	1	2	2005-08-01	2018-12-20
3	1	1	1	3	2005-08-01	2031-12-20
4	1	1	2	4	2007-08-01	2031-12-01
5	1	1	3	5	2005-08-01	2031-12-01
6	1	1	3	6	2005-08-01	2018-12-09
7	1	1	3	7	2005-08-01	2018-12-19
8	1	1	4	8	2007-08-01	2031-12-01
9	1	1	5	9	2007-08-01	2031-12-01
10	2	2	6	10	2005-08-01	2018-12-01
11	2	2	6	11	2005-08-01	2018-12-20
12	2	2	6	12	2005-08-01	2031-12-20
13	2	2	6	13	2005-08-01	2018-12-20
14	2	2	6	14	2005-08-01	2031-12-20
15	2	2	7	15	2007-08-01	2031-12-01
16	2	2	7	16	2007-08-01	2031-12-01
17	3	3	8	17	2005-08-01	2018-12-01
18	3	3	8	18	2005-08-01	2018-12-20
19	3	3	9	19	2007-08-01	2038-12-01
20	3	3	10	20	2007-08-01	2031-12-01
\.


--
-- Data for Name: organization_t; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.organization_t (id) FROM stdin;
1
\.


--
-- Data for Name: person_t; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.person_t (id, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator) FROM stdin;
1	1	Сергеев	Владислав	Сергеевич	1959-11-01	4101	401101	101	Доцент	Кандидат наук	2005-08-01	2018-12-01	vsergeev@domain.com	
2	1	Иванов	Иван	Иванович	1969-11-01	4102	401102	102	Доцент	Кандидат наук	2005-08-01	2018-12-20	iivanov@domain.com	
3	1	Сидоров	Николай	Григорьевич	1971-11-01	4103	401103	103	Доцент	Кандидат наук	2005-08-01	2031-12-20	nsidorov@domain.com	
4	2	Соколова	Алена	Геннадьевна	1959-11-01	4104	401104	104	Доцент	Кандидат наук	2007-08-01	2031-12-01	asokolova@domain.com	
5	3	Фролова	Татьяна	Валерьевна	1959-11-01	4105	401105	105	Отсутствует	Отсутствует	2005-08-01	2031-12-01	tfrolova@domain.com	
6	3	Степанова	Богдана	Валерьевна	1959-11-01	4106	401106	106	Отсутствует	Отсутствует	2005-08-01	2018-12-09	tfrolova@domain.com	
7	3	Аникеева	Валерия	Валерьевна	1959-11-01	4107	401107	107	Отсутствует	Отсутствует	2005-08-01	2018-12-19	vanikeeva@domain.com	
8	4	Колесникова	Ника	Семеновна	1959-11-01	4108	401108	108	Отсутствует	Отсутствует	2007-08-01	2031-12-01	nkolesnikova@domain.com	
9	5	Филимонов	Лука	Геннадьевич	1959-11-01	4109	401109	109	Отсутствует	Отсутствует	2007-08-01	2031-12-01	lfilimonov@domain.com	
10	6	Владиславов	Владислав	Сергеевич	1959-11-01	4201	402101	201	Доцент	Кандидат наук	2005-08-01	2018-12-01	vvladislavov@domain.com	
11	6	Березкин	Иван	Иванович	1969-11-01	4202	402102	202	Доцент	Кандидат наук	2005-08-01	2018-12-20	iberezkin@domain.com	
12	6	Липов	Николай	Григорьевич	1971-11-01	4203	402103	203	Доцент	Кандидат наук	2005-08-01	2031-12-20	nlipov@domain.com	
13	6	Пихтов	Антон	Олегович	1969-11-01	4204	402104	204	Доцент	Кандидат наук	2005-08-01	2018-12-20	apihtov@domain.com	
14	6	Облепихин	Николай	Григорьевич	1971-11-01	4205	402105	205	Доцент	Кандидат наук	2005-08-01	2031-12-20	noblepihin@domain.com	
15	7	Ибрагимова	Агафья	Всеволодовна	1959-11-01	4206	402106	206	Отсутствует	Отсутствует	2007-08-01	2031-12-01	aibragimova@domain.com	
16	7	Абоймова	Кира	Александровна	1959-11-01	4207	402107	207	Отсутствует	Отсутствует	2007-08-01	2031-12-01	kaboimova@domain.com	
17	8	Новичкова	Ольга	Никитевна	1959-11-01	4301	403101	301	Доцент	Кандидат наук	2005-08-01	2018-12-01	onovichkova@domain.com	
18	8	Лобов	Август	Геннадьевич	1969-11-01	4302	403102	302	Доцент	Кандидат наук	2005-08-01	2031-12-20	alobov@domain.com	
19	9	Черных	Гавриил	Макарович	1959-11-01	4303	403103	303	Отсутствует	Отсутствует	2005-08-01	2038-12-01	gchernykh@domain.com	
20	10	Кураев	Ярослав	Давыдович	1959-11-01	4304	403104	304	Отсутствует	Отсутствует	2007-08-01	2031-12-01	ykuraev@domain.com	
\.


--
-- Data for Name: position_t; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.position_t (id, divisionid, positionname, maxemployees, vacantpositions) FROM stdin;
1	1	Преподаватель	10	7
2	1	Декан	1	0
3	1	Секретарь	5	2
4	1	Руководитель ОК	1	0
5	1	Сотрудник ОК	1	0
6	2	Преподаватель	10	5
7	2	Сотрудник ОК	3	1
8	3	Преподаватель	15	13
9	3	Администратор	3	2
10	3	Сотрудник ОК	1	0
\.


--
-- Data for Name: sciencedegree_t; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.sciencedegree_t (id, sciencedegree) FROM stdin;
1	Отсутствует
2	Кандидат наук
3	Доктор наук
\.


--
-- Data for Name: scientifictitle_t; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.scientifictitle_t (id, scientifictitle) FROM stdin;
1	Отсутствует
2	Доцент
3	Профессор
\.


--
-- Data for Name: user_t; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.user_t (id, personid, login, password, role) FROM stdin;
1	8	NKOLESNIKOVA	NKOLESNIKOVA	Руководитель ОК
2	9	LFILIMONOV	LFILIMONOV	Сотрудник ОК
3	15	AIBRAGIMOVA	AIBRAGIMOVA	Сотрудник ОК
4	19	GCHERNYKH	GCHERNYKH	Администратор
5	20	YKURAEV	YKURAEV	Сотрудник ОК
\.


--
-- Name: division_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.division_t_id_seq', 3, true);


--
-- Name: order_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.order_t_id_seq', 3, true);


--
-- Name: orderline_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.orderline_t_id_seq', 20, true);


--
-- Name: organization_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.organization_t_id_seq', 1, true);


--
-- Name: person_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.person_t_id_seq', 20, true);


--
-- Name: position_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.position_t_id_seq', 10, true);


--
-- Name: sciencedegree_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sciencedegree_t_id_seq', 3, true);


--
-- Name: scientifictitle_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.scientifictitle_t_id_seq', 3, true);


--
-- Name: user_t_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.user_t_id_seq', 5, true);


--
-- Name: division_t division_t_divisioncode_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.division_t
    ADD CONSTRAINT division_t_divisioncode_key UNIQUE (divisioncode);


--
-- Name: division_t division_t_divisionname_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.division_t
    ADD CONSTRAINT division_t_divisionname_key UNIQUE (divisionname);


--
-- Name: division_t division_t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.division_t
    ADD CONSTRAINT division_t_pkey PRIMARY KEY (id);


--
-- Name: order_t order_t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.order_t
    ADD CONSTRAINT order_t_pkey PRIMARY KEY (id);


--
-- Name: orderline_t orderline_t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orderline_t
    ADD CONSTRAINT orderline_t_pkey PRIMARY KEY (id);


--
-- Name: organization_t organization_t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.organization_t
    ADD CONSTRAINT organization_t_pkey PRIMARY KEY (id);


--
-- Name: person_t person_t_personnelnumber_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.person_t
    ADD CONSTRAINT person_t_personnelnumber_key UNIQUE (personnelnumber);


--
-- Name: person_t person_t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.person_t
    ADD CONSTRAINT person_t_pkey PRIMARY KEY (id);


--
-- Name: position_t position_t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.position_t
    ADD CONSTRAINT position_t_pkey PRIMARY KEY (id);


--
-- Name: sciencedegree_t sciencedegree_t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sciencedegree_t
    ADD CONSTRAINT sciencedegree_t_pkey PRIMARY KEY (id);


--
-- Name: scientifictitle_t scientifictitle_t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scientifictitle_t
    ADD CONSTRAINT scientifictitle_t_pkey PRIMARY KEY (id);


--
-- Name: person_t uq_person_t; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.person_t
    ADD CONSTRAINT uq_person_t UNIQUE (passportseries, passportnumber);


--
-- Name: user_t user_t_login_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_t
    ADD CONSTRAINT user_t_login_key UNIQUE (login);


--
-- Name: user_t user_t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_t
    ADD CONSTRAINT user_t_pkey PRIMARY KEY (id);


--
-- Name: division_t fkdivision_t259254; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.division_t
    ADD CONSTRAINT fkdivision_t259254 FOREIGN KEY (organizationid) REFERENCES public.organization_t(id);


--
-- Name: orderline_t fkorderline_119590; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orderline_t
    ADD CONSTRAINT fkorderline_119590 FOREIGN KEY (orderid) REFERENCES public.order_t(id);


--
-- Name: orderline_t fkorderline_489246; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orderline_t
    ADD CONSTRAINT fkorderline_489246 FOREIGN KEY (divisionid) REFERENCES public.division_t(id);


--
-- Name: orderline_t fkorderline_590067; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orderline_t
    ADD CONSTRAINT fkorderline_590067 FOREIGN KEY (personid) REFERENCES public.person_t(id);


--
-- Name: orderline_t fkorderline_836882; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orderline_t
    ADD CONSTRAINT fkorderline_836882 FOREIGN KEY (positionid) REFERENCES public.position_t(id);


--
-- Name: person_t fkperson_t116686; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.person_t
    ADD CONSTRAINT fkperson_t116686 FOREIGN KEY (positionid) REFERENCES public.position_t(id);


--
-- Name: position_t fkposition_t743544; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.position_t
    ADD CONSTRAINT fkposition_t743544 FOREIGN KEY (divisionid) REFERENCES public.division_t(id);


--
-- Name: user_t fkuser_t545643; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_t
    ADD CONSTRAINT fkuser_t545643 FOREIGN KEY (personid) REFERENCES public.person_t(id);


--
-- PostgreSQL database dump complete
--

