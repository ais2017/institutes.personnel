/* Количество строк по приказам */
SELECT o.id, COUNT(1)
FROM public.orderline ol
JOIN public."Order" o ON (ol.ORDERID = o.id)
GROUP BY o.id
ORDER BY o.id ASC;