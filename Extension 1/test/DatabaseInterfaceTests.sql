/*1. saveEmployee */

SELECT *
FROM public.person_t p;

DELETE FROM public.person_t p WHERE(p.personnelnumber = 190);

/*2. savePosition */

SELECT *
FROM public.position_t p;

DELETE FROM public.position_t p WHERE(p.id > 10);

UPDATE position_t SET maxemployees = 10 WHERE(id = 1);

/*3. saveOrder */

SELECT *
FROM public.order_t o;

SELECT *
FROM public.orderline_t o;

DELETE FROM public.orderline_t o WHERE(o.orderid > 3);
DELETE FROM public.order_t o WHERE(o.id > 3);

/*4. saveUser */

SELECT *
FROM public.user_t u;

DELETE FROM public.user_t u WHERE(u.id > 5);

UPDATE user_t SET password = 'NKOLESNIKOVA' WHERE(login = 'NKOLESNIKOVA');