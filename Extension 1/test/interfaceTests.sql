/* 1. hireEmployee */
SELECT *
FROM public.person_t p;

SELECT *
FROM public.position_t;

SELECT *
FROM public.order_t;

SELECT *
FROM public.orderline_t;



DELETE FROM orderline_t WHERE(id > 20);
DELETE FROM order_t WHERE (id > 3);
DELETE FROM person_t WHERE(id > 20);

UPDATE position_t SET vacantpositions = 7 WHERE(id = 1);



/* 2. changeEmployeeData */
SELECT p.*
FROM public.person_t p
JOIN public.position_t ptn ON (p.positionid = ptn.id)
JOIN public.division_t d ON (ptn.divisionid = d.id)
WHERE(d.id = 1);

SELECT *
FROM public.position_t ptn
WHERE(ptn.divisionid = 1);



/* 3. addDataToStaffList */
SELECT *
FROM public.position_t ptn;

DELETE FROM position_t WHERE(id > 10);



/* 4. changeDataInStaffList */
SELECT *
FROM public.position_t ptn;



/* 5. registerUser */
SELECT *
FROM public.person_t p;

SELECT *
FROM public.user_t u;

SELECT *
FROM public.position_t ptn;

SELECT p.*
FROM public.person_t p
JOIN public.position_t ptn ON (p.positionid = ptn.id)
WHERE(ptn.id = 7);

SELECT u.*
FROM public.person_t p
JOIN public.position_t ptn ON (p.positionid = ptn.id)
JOIN public.user_t u ON (p.id = u.personid)
WHERE(ptn.id = 7);

DELETE FROM user_t WHERE (personid = 16);



/* 6. setAccessRights */

SELECT *
FROM public.user_t u;