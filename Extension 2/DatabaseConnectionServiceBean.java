package com.institutes.personnel.service.impl;

import com.institutes.personnel.service.DatabaseConnectionService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnectionServiceBean implements DatabaseConnectionService {

    private final String username = "postgres";

    private final String password = "postgres";

    private final String dbms = "postgresql";

    private final String serverName = "localhost";

    private final String portNumber = "5432";

    private final String database = "InstitutesPersonnel";

    @Override
    public Connection getConnection() throws SQLException {

        Connection connection = null;
        Properties connectionProperties = new Properties();
        connectionProperties.put("user", this.username);
        connectionProperties.put("password", this.password);

        if(this.dbms.equals("postgresql")) {

            //jdbc:postgresql://host:port/database

            connection = DriverManager.getConnection("jdbc:" + this.dbms + "://" +
                            this.serverName +
                            ":" + this.portNumber + "/" + this.database,
                            connectionProperties);
        }

        return connection;

    }
}
