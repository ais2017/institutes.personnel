package com.institutes.personnel.service;

import com.institutes.personnel.model.*;
import com.institutes.personnel.service.classes.Credentials;
import javafx.application.Application;
import javafx.stage.Stage;

import java.util.Collection;

/* Взаимодействие с пользователем в части ввода/вывода данных */
public interface UserInteractionService {

//    public void start(Stage primaryStage) throws Exception;

//    public void test() throws Exception;

    /** Бизнес-процесс Авторизация пользователя в системе. Возвращает логин/пароль **/
    public Credentials getCredentials();

    /** Бизнес-процесс Изменение данных о существующем Сотруднике. Возвращает выбранного пользователем Сотрудника **/
    public Employee getEmployeeToChange();

    /** Бизнес-процесс Изменение данных о существующем Сотруднике. Возвращает измененные данные о Сотруднике **/
    public Employee getChangedEmployee(Employee employeeToChange);

    /** Бизнес-процесс Внесение данных о новых Сотрудниках. Возвращает Сотрудника, которого будем принимать **/
    public Employee getEmployeeToCreate();

    /** Бизнес-процесс Внесение данных о новых Сотрудниках. Возвращает список Сотрудников, которых будем принимать **/
    public Collection<Employee> getEmployeesToCreate();

    /** Бизнес-процесс Внесение новых данных в штатное расписание. Возвращает целевое Подразделение, в котором создадим новую Должность **/
    public Division getTargetDivisionForStaffListPosition();

    /** Бизнес-процесс Внесение новых данных в штатное расписание. Возвращает Должность, которую необходимо добавить в Подразделение **/
    public Position getNewPositionForStaffListDivision(Division division);

    /** Бизнес-процесс Изменение данных в штатном расписании. Возвращает Должность, которую необходимо изменить **/
    public Position getPositionInStaffListToChange(Division division);

    /** Бизнес-процесс Изменение данных в штатном расписании. Возвращает Должность после изменения **/
    public Position getPositionInStaffListAfterChange(Position positionToChange);

    /** Бизнес-процесс Выдача по запросу списка свободных Должностей. Возвращает Подразделение, в рамках которого будет происходить поиск. **/
    public Division getDivisionToSearchVacantPositions();

    /** Бизнес-процесс Регистрация в системе Пользователя. Возвращает Сотрудника, для которого необходимо создать Пользователя. **/
    public Employee getTargetEmployeeForCreatingUser();

    /** Бизнес-процесс Регистрация в системе Пользователя. Возвращает Пользователя, который был создан на основе выбранного Сотрудника. **/
    public User getCreatedUser(Employee employee);

    /** Бизнес-процесс Настройка прав доступа, изменение прав доступа. Возвращает Пользователя, которого будем изменять. **/
    public User getUserToChange(Collection<User> users);

    /** Бизнес-процесс Настройка прав доступа, изменение прав доступа. Возвращает измененного Пользователя. **/
    public User getChangedUser(User userToChange);

    /** Показать свободные должности **/
    public void showVacantPositions(Collection<Position> vacantPositions);

    /** Отправить пользователю сообщение **/
    public void showMessage(String message);

    /** Отправить пользователю отчет об ошибке **/
    public void showError(String message);

    /** Показать основную форму для Руководитель ОК **/
    public void showMainHRManagerForm();

    /** Показать основную форму для Сотрудник ОК **/
    public void showMainHREmployeeForm();

    /** Показать основную форму для Администратор **/
    public void showMainAdministratorForm();

    public Organization getOrganization();

    public void setOrganization(Organization organization);

    public User getAuthorizedUser();

    public void setAuthorizedUser(User user);

}
