package com.institutes.personnel.model;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;

public class Staff {

    private Collection<Employee> employees;

    public Staff() {
        this.employees = new LinkedHashSet<Employee>();
    }

    public Staff(Collection<Employee> employees) {
        this.employees = employees;
    }

    public void addEmployee(Employee employee){
        this.employees.add(employee);
    }

    public Collection<Employee> getEmployees() {
        return employees;
    }

    public void deleteEmployee(Employee employee) {
        this.employees.remove(employee);
    }

    public void setEmployees(Collection<Employee> employees) {
        this.employees = employees;
    }

    public Collection<Employee> searchEmployeesByFullName(String lN, String fN, String mN) {
        //throw new NotImplementedException();

        if(this.employees.size() < 1) {
            return null;
        }

        Collection<Employee> foundEmployees = new LinkedHashSet<Employee>();

        //Проходим в цикле все элементы сета employees, для каждого элемента сравниваем
        for(Employee employee: this.employees) {
            if(employee.getLastName().equals(lN) && employee.getFirstName().equals(fN) && employee.getMiddleName().equals(mN))
                foundEmployees.add(employee);
        }

        //Возвращаем null, в случае, если предыдущий цикл не нашел элементов
        if(foundEmployees.size() < 1)
            return null;
        else return foundEmployees;
    }

    public Employee searchEmployeeByPersonnelNumber(int pN) {
        //throw new NotImplementedException();
        if(this.employees.size() < 1) {
            return null;
        }

        //Проходим в цикле все элементы сета employees, для каждого элемента сравниваем
        for(Employee employee: this.employees) {
            if(employee.getPersonnelNumber() == pN)
                return employee;
        }

        //Возвращаем null, в случае, если предыдущий цикл не нашел элемент
        return null;
    }

    public Employee searchEmployeeByPassportData(int passportSeries, int passportNumber) {
        //throw new NotImplementedException();
        if(this.employees.size() < 1) {
            return null;
        }

        //Проходим в цикле все элементы сета employees, для каждого элемента сравниваем
        for(Employee employee: this.employees) {
            if(employee.getPassportSeries() == passportSeries && employee.getPassportNumber() == passportNumber)
                return employee;
        }

        //Возвращаем null, в случае, если предыдущий цикл не нашел элемент
        return null;
    }
}
