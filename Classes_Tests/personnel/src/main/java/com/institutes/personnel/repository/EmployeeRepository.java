package com.institutes.personnel.repository;

import com.institutes.personnel.model.Division;
import com.institutes.personnel.model.Employee;

import java.util.Collection;

public interface EmployeeRepository {

    public Employee saveEmployee(Employee employee);

    public Collection<Employee> saveEmployees(Collection<Employee> employees);

    public Collection<Employee> findAll();

    /** Получить список всех Сотрудников определенного Подразделения **/
    public Collection<Employee> findEmployeesByDivision(Division division);

    /** Поиск Сотрудника по идентификатору **/
    public Employee findEmployeeById(int id);

    /** Поиск всех Сотрудников, занимающих определенную Должность.
     *  Этот метод возвращает коллекцию Сотрудников, у которых currentPosition = null!
     *  Предполагаемое применение: в методе PositionRepository.findPositionById
     *  для корректной инициализации Должности. Каждому Сотруднику
     *  будет присвоен currentPosition в конструкторе Position**/
    public Collection<Employee> findEmployeesByPositionIdForCreatingPosition(int positionId);


}
