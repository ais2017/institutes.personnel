package com.institutes.personnel.service.impl;

import com.institutes.personnel.model.*;
import com.institutes.personnel.repository.*;
import com.institutes.personnel.repository.impl.UserRepositoryService;
import com.institutes.personnel.service.AuthorizationService;
import com.institutes.personnel.service.BusinessProcessService;
import com.institutes.personnel.service.DatabaseOperationService;
import com.institutes.personnel.service.UserInteractionService;
import com.institutes.personnel.service.classes.Credentials;

import javax.swing.*;
import java.util.*;

public class BusinessProcessServiceBean implements BusinessProcessService {

    /* Взаимодействие с пользователем в части ввода/вывода данных */
    private UserInteractionService userInteractionService;

    /* Интерфейс к операциям с базой */
    private DatabaseOperationService databaseOperationService;

    /* Количество попыток входа в систему */
    private final int retriesAuth = 3;

    /** Конструктор только с интерейсом операций с базой и взаимодействия с пользователем **/
    public BusinessProcessServiceBean(UserInteractionService userInteractionService, DatabaseOperationService databaseOperationService) {
        this.userInteractionService = userInteractionService;
        this.databaseOperationService = databaseOperationService;
    }

    /** Авторизация пользователя в системе
     *  В случае успеха возвращает экземпляр User - текущего пользователя, в случае неудачи null.**/
    @Override
    public User authorization() {

        int retries = 0;
        boolean authorized = false;
        User user = null;
        Credentials credentials = null;

        while (retries++ < this.retriesAuth && !authorized) {
            credentials = userInteractionService.getCredentials();
            authorized = databaseOperationService.checkCredentials(credentials.getLogin(), credentials.getPassword());

            if(!authorized)
                userInteractionService.showMessage("Ошибка авторизации! Осталось попыток: " + (this.retriesAuth - retries));
        }

        //Если авторизация успешна, ищем этого пользователя в хранилище по логину
        if(authorized) {
            user = databaseOperationService.findUserByLogin(credentials.getLogin());
        }

        return user;
    }

    /** Изменение данных о существующих сотрудниках
     *  В случае успеха возвращает сохраненного сотрудника, в случае неудачи null.**/
    @Override
    public Employee changeEmployeeData(User user) {

     //   User user = null;
        Position oldPosition, newPosition;
        Order order = null;

        /** Авторизация **/
     //   user = this.authorization();

        if(user == null) {
         //   throw new RuntimeException("Login failed");
            userInteractionService.showError("Пользователь не определен");
            return null;
        }

        /** Операция доступна только пользователю с ролью Сотрудник ОК **/
        if(!user.getRole().equals("Сотрудник ОК")) {
            userInteractionService.showError("Операция доступна только пользователю с ролью Сотрудник ОК");
            return null;
        }

        /** Пользователь выбирает Сотрудника, которого надо изменить. **/
        Employee employeeToChange = userInteractionService.getEmployeeToChange();
        //Сохраняем текущую Должность сотрудника; Если она изменится, нужно сделать Приказ
        if(employeeToChange != null) {
            oldPosition = employeeToChange.getCurrentPosition();
        }
        else {
            userInteractionService.showError("Сотрудник не определен");
            return null;
        }

        /** Изменять данные о сотруднике может только сотрудник ОК, закрепленный за тем же подразделением **/
        if(!user.getRole().equals("Сотрудник ОК") || !user.getEmployee().getCurrentPosition().getParentDivision().equals(employeeToChange.getCurrentPosition().getParentDivision())) {
         //   throw new RuntimeException("Access Denied");
            userInteractionService.showError("Доступ запрещен");
            return null;
        }

        /** Передаем пользователю Сотрудника для изменения. Получаем новый объект класса Employee. **/
        Employee changedEmployee = userInteractionService.getChangedEmployee(employeeToChange);
        if(changedEmployee == null) {
            userInteractionService.showError("Ошибка при получении измененного сотрудника");
            return null;
        }

        newPosition = changedEmployee.getCurrentPosition();

        /** Проверяем, изменилась ли Должность **/
        if(newPosition == null) {
            //Сотрудник был уволен. Необходимо создать приказ на увольнение и убрать его из текущей Должности.
            //Убираем сотрудника из массива сотрудников старой должности
            oldPosition.deleteEmployee(employeeToChange);

            //Создаем Приказ
            OrderLine orderLine = new OrderLine(changedEmployee, oldPosition, oldPosition.getParentDivision(), employeeToChange.getContractStartDate(), employeeToChange.getContractExpirationDate());
            Collection<OrderLine> orderLines = new LinkedHashSet<OrderLine>();
            orderLines.add(orderLine);
            order = new Order(orderLines, "Увольнение");
         //   this.orders.add(order);
        }
        else if(!oldPosition.equals(changedEmployee.getCurrentPosition())) {
            //Сотрудник был переведен
            //Убираем сотрудника из массива сотрудников старой должности, добавляем в новую
            try {
                newPosition.addEmployee(changedEmployee);
            }
            catch (RuntimeException e) {
                userInteractionService.showError("Невозможно перевести сотрудника на новую должность. Проверьте наличие свободных должностей.");
                return null;
            }
            oldPosition.deleteEmployee(employeeToChange);

            //Создаем Приказ
            OrderLine orderLine = new OrderLine(changedEmployee, newPosition, newPosition.getParentDivision(), employeeToChange.getContractStartDate(), employeeToChange.getContractExpirationDate());
            Collection<OrderLine> orderLines = new LinkedHashSet<OrderLine>();
            orderLines.add(orderLine);
            order = new Order(orderLines, "Перевод на другую Должность");
         //   this.orders.add(order);
        }
        else {
            //Изменились только личные данные. Но объект вернули уже другой. Чтобы корректно это обработать, необходимо исходный объект удалить из Position.employees, а новый добавить.
            employeeToChange.getCurrentPosition().deleteEmployee(employeeToChange);
            changedEmployee.getCurrentPosition().addEmployee(changedEmployee);
        }

        /** Сохранение **/
        Employee savedEmployee = databaseOperationService.saveEmployee(changedEmployee);
        if(savedEmployee == null) {
            userInteractionService.showError("Возникли проблемы при сохранении данных сотрудника");
            return null;
        }

        if(oldPosition != null) {
            Position savedOldPosition = databaseOperationService.savePosition(oldPosition);
            if(savedOldPosition == null) {
                userInteractionService.showError("Возникли проблемы при сохранении предыдущей должности");
                return null;
            }
        }

        /* Если старая и новая должности не равны, сохраняем новую */
        if(!oldPosition.equals(newPosition)) {
            Position savedNewPosition = databaseOperationService.savePosition(newPosition);
            if (savedNewPosition == null) {
                userInteractionService.showError("Возникли проблемы при сохранении новой должности");
                return null;
            }
        }

        /* Экземпляру сотрудника после сохранения был присвоен id. Нужно проставить его всем строкам. */
        if(order != null) {
            for (OrderLine ol : order.getOrderLines()) {
                ol.setPersonId(savedEmployee.getId());
                ol.setPositionId(savedEmployee.getCurrentPosition().getId());
                ol.setDivisionId(savedEmployee.getCurrentPosition().getParentDivision().getId());
            }
        }

        if(order != null) {
            /** Если был создан Приказ, его надо сохранить **/
            Order savedOrder = databaseOperationService.saveOrder(order);
            if(savedOrder == null) {
                userInteractionService.showError("Возникли проблемы при сохранении приказа о приеме на работу");
                return null;
            }
        }

        /** Возвращаем измененного Сотрудника **/
        return changedEmployee;
    }

    @Override
    public Employee hireEmployee(User user) {
     //   User user = null;
        Collection<OrderLine> orderLines = new LinkedHashSet<OrderLine>();

        /** Авторизация **/
     //   user = this.authorization();

        if(user == null) {
         //   throw new RuntimeException("Login failed");
            userInteractionService.showError("Пользователь не определен");
            return null;
        }

        /** Операция доступна только пользователю с ролью Сотрудник ОК **/
        if(!user.getRole().equals("Сотрудник ОК")) {
            userInteractionService.showError("Операция доступна только пользователю с ролью Сотрудник ОК");
            return null;
        }

        /** Пользователь вводит данные о новых сотрудниках. **/
        Employee employeeToCreate = userInteractionService.getEmployeeToCreate();

        if(employeeToCreate == null) {
            userInteractionService.showError("Создаваемый сотрудник не определен");
            return null;
        }
        else {
            /* Можно создавать только сотрудников в своем подразделении */
            if(!employeeToCreate.getCurrentPosition().getParentDivision().equals(user.getEmployee().getCurrentPosition().getParentDivision())) {
             //   throw new RuntimeException("Insufficient privileges");
                userInteractionService.showError("Доступ запрещен");
                return null;
            }
            try {
                /* Если с подразделением все в порядке, пытаемся создать сотрудника */
                employeeToCreate.getCurrentPosition().addEmployee(employeeToCreate);
                orderLines.add(new OrderLine(employeeToCreate, employeeToCreate.getCurrentPosition(), employeeToCreate.getCurrentPosition().getParentDivision(), employeeToCreate.getContractStartDate(), employeeToCreate.getContractExpirationDate()));
            }
            catch (RuntimeException e) {
                userInteractionService.showError("Невозможно создать сотрудника, проверьте наличие свободных Должностей для " +
                        "Должности " + employeeToCreate.getCurrentPosition().getPositionName() + ", Подразделения " + employeeToCreate.getCurrentPosition().getParentDivision().getDivisionName() + ".");
                return null;
            }
        }

        Order order = new Order(orderLines, "Прием на работу");

        Employee savedEmployee = databaseOperationService.saveEmployee(employeeToCreate);

        if(savedEmployee == null) {
            userInteractionService.showError("Возникли проблемы при сохранении данных о сотруднике");
            return null;
        }

        /* Экземпляру сотрудника после сохранения был присвоен id. Нужно проставить его всем строкам. */
        for(OrderLine ol: order.getOrderLines()) {
            ol.setPersonId(savedEmployee.getId());
            ol.setPositionId(savedEmployee.getCurrentPosition().getId());
            ol.setDivisionId(savedEmployee.getCurrentPosition().getParentDivision().getId());
        }

        Order savedOrder = databaseOperationService.saveOrder(order);
        if(savedOrder == null) {
            userInteractionService.showError("Возникли проблемы при сохранении данных о приказе");
            return null;
         //   throw new RuntimeException("Возникли проблемы при сохранении данных.");
        }
        Position savedPosition = databaseOperationService.savePosition(employeeToCreate.getCurrentPosition());
        if(savedPosition == null) {
            userInteractionService.showError("Возникли проблемы при сохранении данных о должности");
            return null;
        }
        //this.orders.add(order);

        return employeeToCreate;
    }

    /** Внесение новых данных в штатное расписание **/
    @Override
    public Position addDataToStaffList(User user) {
     //   User user;
        Division division;
        Position newPosition;

     //   user = this.authorization();

        if(user == null) {
         //   throw new RuntimeException("User is null");
            userInteractionService.showError("Пользователь не определен");
            return null;
        }

        if(!user.getRole().equals("Руководитель ОК")) {
         //   throw new RuntimeException("Access Denied");
            userInteractionService.showError("Доступ запрещен: недопустимая роль");
            return null;
        }

        //Выбираем Подразделение, в котором создается запись Штатного расписания (новая Должность)
        division = userInteractionService.getTargetDivisionForStaffListPosition();
        if(division == null) {
         //   throw new RuntimeException("Cannot add Position to Division null");
            userInteractionService.showError("Подразделение не выбрано");
            return null;
        }

        //Ожидаем ввода данных о новой Должности штатного расписания
        newPosition = userInteractionService.getNewPositionForStaffListDivision(division);

        if(newPosition == null) {
         //   throw new RuntimeException("Incorrect null data");
            userInteractionService.showError("Введены некорректные данные - null");
            return null;
        }
        else if(newPosition.getEmployees().size() > 0 || newPosition.getVacantPositions() != newPosition.getMaxEmployees()) {
            userInteractionService.showError("Неверно введены данные для Должности штатного расписания. Новая Должность не должна ссылаться на сотрудников и все позиции должны быть свободными");
            return null;
         //   throw new RuntimeException("Неверно введены данные для Должности штатного расписания. Новая Должность не должна ссылаться на сотрудников и все позиции должны быть свободными");
        }

        /* Проверка, что родительское подразделение не содержит должности с таким названием */
        for(Position position: newPosition.getParentDivision().getPositions()) {
            if(position.getPositionName().equals(newPosition.getPositionName())) {
                userInteractionService.showError("Родительское Подразделение уже содержит Должность с таким названием");
                return null;
                //   throw new RuntimeException("Родительское Подразделение уже содержит Должность с таким названием");
            }
        }
        /** Введены корректные данные, создаем запись **/
        division.addPosition(newPosition);

        /** Сохраняем Должность и Подразделение **/
        Position savedPosition = databaseOperationService.savePosition(newPosition);
        if(savedPosition == null) {
            userInteractionService.showError("Возникли проблемы при сохранении Должности");
            return null;
         //   throw new RuntimeException("Возникли проблемы при сохранении Должности");
        }

     /*   Division savedDivision = databaseOperationService.saveDivision(division);
        if(savedDivision == null)
            throw new RuntimeException("Возникли проблемы при сохранении Подразделения"); */

        return newPosition;
    }

    /** Изменение данных в штатном расписании **/
    @Override
    public Position changeDataInStaffList(User user) {
     //   User user;
        Division division;
        Position positionToChange, positionAfterChange;

     //   user = this.authorization();

        if(user == null) {
         //   throw new RuntimeException("User is null");
            userInteractionService.showError("Пользователь не определен");
            return null;
        }

        if(!user.getRole().equals("Руководитель ОК")) {
         //   throw new RuntimeException("Access Denied");
            userInteractionService.showError("Доступно только Руководителю ОК");
            return null;
        }

        //Выбираем Подразделение, в котором будем выбирать Должность
        division = userInteractionService.getTargetDivisionForStaffListPosition();
        if(division == null) {
            //   throw new RuntimeException("Cannot add Position to Division null");
            userInteractionService.showError("Подразделение не выбрано");
            return null;
        }

        //Ожидаем выбора Должности, которую надо изменить
        positionToChange = userInteractionService.getPositionInStaffListToChange(division);

        if(positionToChange == null) {
            userInteractionService.showError("Должность не выбрана");
            return null;
        }

        //Сохраняем данные перед изменением их Пользователем, чтобы после изменения осуществить проверки
        Division oldDivision = positionToChange.getParentDivision();
        Collection<Employee> oldEmployees = positionToChange.getEmployees();

        //Предлагаем пользователю изменить данные о Должности
        positionAfterChange = userInteractionService.getPositionInStaffListAfterChange(positionToChange);

        //Осуществляем проверки
        if(positionAfterChange == null) {
            //Пользователь хочет удалить Должность, проверяем, можно ли это сделать.
            if (positionToChange.getEmployees().size() == 0 && positionToChange.getMaxEmployees() == positionToChange.getVacantPositions()) {
                //Нет занятых Должностей, можно удалить
                positionToChange.getParentDivision().deletePosition(positionToChange);
            }
            else {
                //Невозможно удалить
             //   throw new RuntimeException("Невозможно удалить Должность, так как есть Сотрудники, занимающие эту Должность");
                userInteractionService.showError("Невозможно удалить Должность, так как есть Сотрудники, занимающие эту Должность");
                return null;
            }
        }

        if(!positionAfterChange.getParentDivision().equals(oldDivision)) {
            //Нельзя менять Подразделение Должности
         //   throw new RuntimeException("Нельзя изменять Подразделение существующей Должности");
            userInteractionService.showError("Нельзя изменять Подразделение существующей Должности");
            return null;
        }

        if(positionAfterChange.getEmployees().size() != positionToChange.getEmployees().size() || !positionAfterChange.getEmployees().equals(positionToChange.getEmployees())) {
            //Нельзя изменять Сотрудников, занимающих Должность. Этим занимается Сотрудник ОК в других бизнес-процессах.
         //   throw new RuntimeException("Нельзя менять Сотрудников, занимающих Должность");
            userInteractionService.showError("Нельзя менять Сотрудников, занимающих Должность");
            return null;
        }

        if(positionAfterChange.getMaxEmployees() != positionToChange.getMaxEmployees() || positionAfterChange.getVacantPositions() != positionToChange.getVacantPositions()) {
            //Должно соблюдаться правило: Количество свободных Должностей + Количество Сотрудников, занимающих Должность = Максимальное Количество Сотрудников, занимающих Должность
            if((positionAfterChange.getVacantPositions() + positionAfterChange.getEmployees().size()) != positionAfterChange.getMaxEmployees()) {
                userInteractionService.showError("Должно соблюдаться правило: Количество свободных Должностей + Количество Сотрудников, занимающих Должность = Максимальное Количество Сотрудников, занимающих Должность");
                return null;
             //   throw new RuntimeException("Должно соблюдаться правило: Количество свободных Должностей + Количество Сотрудников, занимающих Должность = Максимальное Количество Сотрудников, занимающих Должность");
            }
        }

        if(!positionAfterChange.getPositionName().equals(positionToChange.getPositionName())) {
            if(positionAfterChange.getPositionName() == null || positionAfterChange.getPositionName().equals("")) {
                userInteractionService.showError("Недопустимое название Должности");
                return null;
             //   throw new RuntimeException("Недопустимое название Должности");
            }
            else {
                //Проверяем, что родительское Подразделение не содержит больше Должностей с такими именами
                for(Position position: positionAfterChange.getParentDivision().getPositions()) {
                    if(position.getPositionName().equals(positionAfterChange.getPositionName())) {
                        userInteractionService.showError("Родительское Подразделение уже содержит Должность с таким названием");
                        return null;
                     //   throw new RuntimeException("Родительское Подразделение уже содержит Должность с таким названием");
                    }
                }
            }
        }

        /** Сохраняем изменения **/

     //   Division savedDivision;
        Position savedPosition;

        if(positionAfterChange == null) {
            //Должность была удалена. Необходимо убрать ее из хранилища.
            databaseOperationService.deletePosition(positionToChange);

         /*   savedDivision = databaseOperationService.saveDivision(oldDivision);
            if(savedDivision == null)
                throw new RuntimeException("Возникли проблемы при сохранении Подразделения"); */
        }
        else {
            //Объект вернули уже другой. Чтобы корректно это обработать, необходимо исходный объект удалить из Division.positions, а новый добавить.
            division.deletePosition(positionToChange);
            division.addPosition(positionAfterChange);
            savedPosition = databaseOperationService.savePosition(positionAfterChange);
            if(savedPosition == null) {
                userInteractionService.showError("Возникли проблемы при сохранении Должности");
                return null;
             //   throw new RuntimeException("Возникли проблемы при сохранении Должности");
            }
        }

        return positionAfterChange;
    }

    /** Выдача списка свободных Должностей по Подразделению **/
    @Override
    public Collection<Position> getVacantPositionListByDivision(User user) {
     //   User user;
        Division division;
        Collection<Position> vacantPositions = new LinkedHashSet<>();

     //   user = this.authorization();

        if(user == null) {
         //   throw new RuntimeException("User is null");
            userInteractionService.showError("Пользователь не определен");
            return null;
        }

        if(!user.getRole().equals("Руководитель ОК") && !user.getRole().equals("Сотрудник ОК")) {
         //   throw new RuntimeException("Access Denied");
            userInteractionService.showError("Доступ запрещен");
            return null;
        }

        /** Система загружает информацию о Подразделениях, из которых Пользователь будет выбирать **/


        /** Пользователь выбирает Подразделение, по которому будем искать свободные Должности **/
        division = userInteractionService.getDivisionToSearchVacantPositions();

        if(division == null) {
         //   throw new RuntimeException("Нельзя искать по Подразделению null");
            userInteractionService.showError("Не выбрано подразделение");
            return null;
        }

        /** Ищем по подразделению **/
        for(Position position: division.getPositions()) {
            if(position.getVacantPositions() > 0)
                vacantPositions.add(position);
        }

        return vacantPositions;

     /*   if(vacantPositions.size() > 0)
            return vacantPositions;
        else
            return null; */
    }

    /** Здесь возможно стоит сделать загрузку Организации из organizationRepository вместо использования this **/
    /** Выдача списка свободных Должностей по всем Подразделениям **/
    @Override
    public Collection<Position> getVacantPositionListByOrganization(User user) {
     //   User user;
        Collection<Position> vacantPositions = new LinkedHashSet<>();
        Organization organization;

     //   user = this.authorization();

        if(user == null) {
         //   throw new RuntimeException("User is null");
            userInteractionService.showError("Пользователь не определен");
            return null;
        }

        /** Получаем Организацию **/
        organization = databaseOperationService.findOrganization();

        if(!user.getRole().equals("Руководитель ОК") && !user.getRole().equals("Сотрудник ОК")) {
         //   throw new RuntimeException("Access Denied");
            userInteractionService.showError("Доступ запрещен: недопустимая роль");
            return null;
        }
        else if(organization == null || organization.getDivisions().size() == 0) {
         //   throw new RuntimeException("Отстутствуют данные для поиска");
            userInteractionService.showError("Организация не определена");
            return null;
        }

        /** Ищем по всем Подразделениям **/
        for(Division division: organization.getDivisions()) {
            for (Position position : division.getPositions()) {
                if (position.getVacantPositions() > 0)
                    vacantPositions.add(position);
            }
        }

        return vacantPositions;

     /*   if(vacantPositions.size() > 0)
            return vacantPositions;
        else
            return null; */
    }

    /** Регистрация в системе Пользователя **/
    @Override
    public User registerUser(User currentUser) {
     //   User currentUser;
        User registeredUser;
        Employee employee;

     //   currentUser = this.authorization();

        if(currentUser == null) {
            userInteractionService.showError("Пользователь не определен");
            return null;
         //   throw new RuntimeException("User is null");
        }
        else if(!currentUser.getRole().equals("Администратор")) {
            userInteractionService.showError("Доступно только администратору");
            return null;
         //   throw new RuntimeException("Access Denied");
        }

        /** Администратор создает Пользователя для конкретного Сотрудника **/
        employee = userInteractionService.getTargetEmployeeForCreatingUser();

        if(employee == null) {
            userInteractionService.showError("Не выбран Сотрудник");
            return null;
         //   throw new RuntimeException("Не выбран Сотрудник");
        }
        else if(employee.getCurrentPosition() == null) {
            userInteractionService.showError("Нельзя создать Пользователя для Сотрудника без Должности");
            return null;
         //   throw new RuntimeException("Нельзя создать Пользователя для Сотрудника без Должности");
        }

        /** Создание Пользователя для выбранного Сотрудника **/
        registeredUser = userInteractionService.getCreatedUser(employee);

        if(registeredUser == null) {
            userInteractionService.showError("Создаваемый пользователь не определен");
            return null;
         //   throw new RuntimeException("Пользователь null");
        }
        else if(registeredUser.getEmployee() == null || !registeredUser.getEmployee().equals(employee))
            registeredUser.setEmployee(employee);
        else if(!registeredUser.getRole().equals("Руководитель ОК") && !registeredUser.getRole().equals("Сотрудник ОК") && registeredUser.getRole().equals("Администратор")) {
            userInteractionService.showError("Некорректная роль Пользователя");
            return null;
         //   throw new RuntimeException("Некорректная роль Пользователя");
        }

        /** Сохранение данных **/
        User savedUser = databaseOperationService.saveUser(registeredUser);
        if(savedUser == null) {
            userInteractionService.showError("Произошла ошибка при сохранении данных");
            return null;
         //   throw new RuntimeException("Произошла ошибка при сохранении данных");
        }
        else
            return registeredUser;
    }

    /** Настройка прав доступа, изменение прав доступа **/
    @Override
    public User setAccessRights(User currentUser) {
     //   User currentUser;
        User userToChange, changedUser;
        Employee employee;

     //   currentUser = this.authorization();

        if(currentUser == null) {
            userInteractionService.showError("Пользователь не определен");
            return null;
         //   throw new RuntimeException("User is null");
        }
        else if(!currentUser.getRole().equals("Администратор")) {
            userInteractionService.showError("Доступно только администратору");
            return null;
         //   throw new RuntimeException("Access Denied");
        }

        /** Загружаем всех пользователей **/
        Collection<User> users = databaseOperationService.findUsers();

        /** Администратор выбирает Пользователя, которому он хочет изменить Роль **/
        userToChange = userInteractionService.getUserToChange(users);

        if(userToChange == null) {
            userInteractionService.showError("Не выбран Пользователь");
            return null;
         //   throw new RuntimeException("Не выбран Пользователь");
        }

        /** Получаем измененного Пользователя **/
        changedUser = userInteractionService.getChangedUser(userToChange);

        if(changedUser == null) {
            userInteractionService.showError("Пользователь не определен");
            return null;
        }

        /** Проверка правильности введенных данных **/
     /*   if(changedUser != null) {
            if(!userToChange.getEmployee().equals(changedUser.getEmployee())) {
                //Нельзя менять Сотрудника
                userInteractionService.showError("Нельзя менять данные о Сотруднике");
                return null;
             //   throw new RuntimeException("Нельзя менять данные о Сотруднике");
            }
        }
        else if(changedUser == null) {
            //Администратор хочет удалить Пользователя. Это можно сделать, только если Сотрудник, связанный с Пользователем, не привязан к Должности (уволен)
            if(userToChange.getEmployee().getCurrentPosition() != null) {
                userInteractionService.showError("Нельзя удалить Пользователя, который не был уволен с Должности");
                return null;
             //   throw new RuntimeException("Нельзя удалить Пользователя, который не был уволен с Должности");
            }
            else {
                databaseOperationService.deleteUser(userToChange);
                return changedUser;
            }
        } */

        /** Сохранение данных **/
        User savedUser = databaseOperationService.saveUser(changedUser);
        if(savedUser == null) {
            userInteractionService.showError("Произошла ошибка при сохранении данных");
            return null;
         //   throw new RuntimeException("Произошла ошибка при сохранении данных");
        }
        else
            return changedUser;
    }

    /** Оповещение Сотрудников, чьи контракты в скором времени заканчиваются **/
    @Override
    public Collection<Employee> getEmployeesWithExpiringContracts() {
        Collection<Employee> employeesWithExpiringContracts = new LinkedHashSet<>();
        Organization organization;
        Collection<Employee> employees;

        GregorianCalendar currentDate = new GregorianCalendar();
        Date date = new Date();    //Текушая дата - время в миллисекундах

        GregorianCalendar contractExpirationDate = new GregorianCalendar();


        //Collection<Employee> allEmployees = employeeRepository.findAll();

        /** Получаем Организацию **/
        organization = databaseOperationService.findOrganization();

        employees = organization.getAllEmployeesOfOrganization();

        for(Employee employee: employees) {
            contractExpirationDate.setTime(employee.getContractExpirationDate());
            currentDate.setTime(date);    //Текушая дата - время в миллисекундах
            if(contractExpirationDate.compareTo(currentDate) > 0) {
                //Имеет смысл проверять контракты, дата завершения которых больше текущей
                currentDate.add(Calendar.DATE, 60);
                if(currentDate.compareTo(contractExpirationDate) > 0)
                    employeesWithExpiringContracts.add(employee);
            }
        }

        if(employeesWithExpiringContracts.size() > 0)
            return employeesWithExpiringContracts;
        else return null;
    }

    /** Внесение данных о новых сотрудниках (сразу несколько, если потребуется - в BPMN только по одному)
     *  В случае успеха возвращает список сохраненных сотрудников, в случае неудачи null**/
    @Override
    public Collection<Employee> hireEmployees(User user) {
        int retries = 0;
        boolean authorized = false;
     //   User user = null;
        Collection<OrderLine> orderLines = new LinkedHashSet<OrderLine>();

        /** Авторизация **/
     //   user = this.authorization();

        if(user == null)
            throw new RuntimeException("Login failed");

        /** Операция доступна только пользователю с ролью Сотрудник ОК **/
        if(!user.getRole().equals("Сотрудник ОК")) {
            userInteractionService.showMessage("Операция доступна только пользователю с ролью Сотрудник ОК");
            return null;
        }

        /** Пользователь вводит данные о новых сотрудниках. **/
        Collection<Employee> employeesToCreate = userInteractionService.getEmployeesToCreate();

        if(employeesToCreate != null && employeesToCreate.size() > 0) {
            for (Employee employee: employeesToCreate) {
                /* Можно создавать только сотрудников в своем подразделении */
                if(!employee.getCurrentPosition().getParentDivision().equals(user.getEmployee().getCurrentPosition().getParentDivision())) {
                    throw new RuntimeException("Insufficient privileges");
                }
                try {
                    /* Если с подразделением все в порядке, пытаемся создать сотрудника */
                    employee.getCurrentPosition().addEmployee(employee);
                    orderLines.add(new OrderLine(employee, employee.getCurrentPosition(), employee.getCurrentPosition().getParentDivision(), employee.getContractStartDate(), employee.getContractExpirationDate()));
                }
                catch (RuntimeException e) {
                    userInteractionService.showError("Невозможно создать всех сотрудников, проверьте наличие свободных Должностей.");
                    return null;
                }
            }
        }
        else return null;

        Collection<Employee> savedEmployees = null;
        if(orderLines.size() > 0) {
            savedEmployees = databaseOperationService.saveEmployees(employeesToCreate);
            Order order = new Order(orderLines, "Прием на работу");
            Order savedOrder = databaseOperationService.saveOrder(order);
         //   this.orders.add(order);
        }

        return savedEmployees;
    }
}
