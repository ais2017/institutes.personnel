package com.institutes.personnel.repository.impl;

import com.institutes.personnel.model.Division;
import com.institutes.personnel.model.Employee;
import com.institutes.personnel.model.Position;
import com.institutes.personnel.repository.EmployeeRepository;
import com.institutes.personnel.repository.PositionRepository;
import com.institutes.personnel.service.DatabaseConnectionService;
import com.institutes.personnel.service.impl.DatabaseConnectionServiceBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;

public class EmployeeRepositoryService implements EmployeeRepository {

/*    private DatabaseConnectionService databaseConnectionService;

    private PositionRepository positionRepository;

    public EmployeeRepositoryService() {
        this.databaseConnectionService = new DatabaseConnectionServiceBean();
        this.positionRepository = new PositionRepositoryService();
    } */

    @Override
    public Employee saveEmployee(Employee employee) {
        return employee;
    }

    @Override
    public Collection<Employee> saveEmployees(Collection<Employee> employees) {
        return employees;
    }

    @Override
    public Collection<Employee> findAll() {
        return null;
    }

    @Override
    public Collection<Employee> findEmployeesByDivision(Division division) {
        return null;
    }

    @Override
    public Employee findEmployeeById(int id) {
        return null;
    }

    @Override
    public Collection<Employee> findEmployeesByPositionIdForCreatingPosition(int positionId) {
        return null;
    }

    //    @Override
//    public Collection<Employee> findEmployeesByPositionIdForCreatingPosition(int positionId) {
//        Collection<Employee> employees = null;
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        String query = "SELECT per.*" +
//                       "FROM person_t per" +
//                       "JOIN position_t pos ON (per.POSITIONID = pos.ID)" +
//                       "WHERE(pos.ID = ?)";
//
//        try {
//            /** Получаем соединение **/
//            connection = databaseConnectionService.getConnection();
//            if(connection == null)
//                throw new RuntimeException("Not connected to database");
//
//            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
//             *  необходим курсор, с помощью которого можно перемещаться по записям **/
//            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//            statement.setInt(1, positionId);
//
//            resultSet = statement.executeQuery();
//
//            /** Получаем количество строк, которое было получено после выполнения запроса **/
//            resultSet.last();
//            int numRows = resultSet.getRow();
//
//            /** Если не было найдено Сотрудников, занимающих эту Должность, возвращаем коллекцию размера 0 **/
//            if(numRows < 1) {
//                employees = new LinkedHashSet<>();
//                return employees;
//            }
//            /** Если были найдены Сотрудники, создаем объекты для них и добавляем в коллекцию **/
//            else {
//                employees = new LinkedHashSet<>();
//                resultSet.beforeFirst();
//                while(resultSet.next()) {
//                    String lastName = resultSet.getString("LASTNAME");
//                    String firstName = resultSet.getString("FIRSTNAME");
//                    String middleName = resultSet.getString("MIDDLENAME");
//                    Date birthday = resultSet.getDate("BIRTHDAY");
//                    int passportSeries = resultSet.getInt("PASSPORTSERIES");
//                    int passportNumber = resultSet.getInt("PASSPORTNUMBER");
//                    int currentPositionId = resultSet.getInt("POSITIONID");
//                    int personnelNumber = resultSet.getInt("PERSONNELNUMBER");
//                    String scientificTitle = resultSet.getString("SCIENTIFICTITLE");
//                    String scienceDegree = resultSet.getString("SCIENCEDEGREE");
//                    String email = resultSet.getString("EMAIL");
//                    Date contractStartDate = resultSet.getDate("CONTRACTSTARTDATE");
//                    Date contractExpirationDate = resultSet.getDate("CONTRACTEXPIRATIONDATE");
//
//                    /** Создаем объект Сотрудника **/
//                    Employee employee = new Employee(lastName, firstName, middleName, birthday,
//                            passportSeries, passportNumber, personnelNumber,
//                            scientificTitle, scienceDegree, email, contractStartDate, contractExpirationDate);
//
//                    /** Добавляем объект Сотрудника в коллекцию **/
//                    employees.add(employee);
//                }
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return employees;
//    }
//
//    @Override
//    public Employee findEmployeeById(int id) {
//        Employee employee = null;
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        String query = "SELECT p.*" +
//                       "FROM person_t p" +
//                       "WHERE(p.id = ?)";
//
//        try {
//
//            /** Получаем соединение **/
//            connection = databaseConnectionService.getConnection();
//            if(connection == null)
//                throw new RuntimeException("Not connected to database");
//
//            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
//             *  необходим курсор, с помощью которого можно перемещаться по записям **/
//            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//            statement.setInt(1, id);
//
//            resultSet = statement.executeQuery();
//
//            /** Получаем количество строк, которое было получено после выполнения запроса **/
//            resultSet.last();
//            int numRows = resultSet.getRow();
//
//            /** Если запрос вернул несколько строк, выбрасываем исключение **/
//            if(numRows > 1)
//                throw new RuntimeException("Several Employees with id " + id + "found");
//            else if(numRows == 0)
//                return null;
//
//            resultSet.beforeFirst();
//            while(resultSet.next()) {
//                String lastName = resultSet.getString("LASTNAME");
//                String firstName = resultSet.getString("FIRSTNAME");
//                String middleName = resultSet.getString("MIDDLENAME");
//                Date birthday = resultSet.getDate("BIRTHDAY");
//                int passportSeries = resultSet.getInt("PASSPORTSERIES");
//                int passportNumber = resultSet.getInt("PASSPORTNUMBER");
//                int currentPositionId = resultSet.getInt("POSITIONID");
//                int personnelNumber = resultSet.getInt("PERSONNELNUMBER");
//                String scientificTitle = resultSet.getString("SCIENTIFICTITLE");
//                String scienceDegree = resultSet.getString("SCIENCEDEGREE");
//                String email = resultSet.getString("EMAIL");
//                Date contractStartDate = resultSet.getDate("CONTRACTSTARTDATE");
//                Date contractExpirationDate = resultSet.getDate("CONTRACTEXPIRATIONDATE");
//
//                /** Получаем объект Должности, с которой связан этот Сотрудник **/
//                Position currentPosition = this.positionRepository.findPositionById(currentPositionId);
//
//                /** Если объект Сотрудника не удалось инциализировать - поиск считаем неуспешным
//                 *  ведь без Сотрудника нельзя создать Пользователя **/
//                if(currentPosition == null)
//                    return null;
//
//                /** Создаем объект Сотрудника **/
//                employee = new Employee(lastName, firstName, middleName, birthday,
//                        passportSeries, passportNumber, currentPosition, personnelNumber,
//                        scientificTitle, scienceDegree, email, contractStartDate, contractExpirationDate);
//            }
//
//            //Закрываем
//            resultSet.close();
//            statement.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//            return null;
//        }
//
//
//
//
//        return employee;
//    }
}
