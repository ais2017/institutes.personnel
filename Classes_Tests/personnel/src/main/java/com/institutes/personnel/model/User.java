package com.institutes.personnel.model;

import java.util.Date;

public class User{

    /** Primary key в таблице user_t **/
    private int id;

    /** Foreign key к таблице person_t **/
    private int personId;

    private Employee employee;

    private String login;

    private String password;

    //Руководитель ОК, Сотрудник ОК, Администратор
    private String role;

    public User() {
        this.employee = null;
        this.login = null;
        this.password = null;
        this.role = null;
    }

    /* Конструктор для репозитория */
    public User(int id, int personId, Employee employee, String login, String password, String role) {
        if(employee == null || login == null || password == null || role == null || login.equals("") || password.equals(""))
            throw new RuntimeException("Обнаружен null во входных данных");

        this.id = id;
        this.personId = personId;

        this.employee = employee;
        this.login = login;
        this.password = password;

        if(role.equals("Руководитель ОК") || role.equals("Сотрудник ОК") || role.equals("Администратор"))
            this.role = role;
        else throw new RuntimeException("Неверная роль");
    }

    /* Конструктор для метода DatabaseOperationService.findUsers */
    public User(int id, int personId, String login, String password, String role) {
        this.id = id;
        this.personId = personId;
        this.login = login;
        this.password = password;
        this.role = role;

        /* в контексте Администратора нам не нужен Сотрудник */
        this.employee = null;
    }

    /* Основной конструктор */
    public User(Employee employee, String login, String password, String role) {
        if(employee == null || login == null || password == null || role == null || login.equals("") || password.equals(""))
            throw new RuntimeException("Обнаружен null во входных данных");

        /* Новый объект */
        this.id = -1;
        this.personId = employee.getId();

        this.employee = employee;
        this.login = login;
        this.password = password;

        if(role.equals("Руководитель ОК") || role.equals("Сотрудник ОК") || role.equals("Администратор"))
            this.role = role;
        else throw new RuntimeException("Неверная роль");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        if(role.equals("Руководитель ОК") || role.equals("Сотрудник ОК") || role.equals("Администратор"))
            this.role = role;
        else throw new RuntimeException("Неверная роль");
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}

/* old
public class User extends Employee{

    private Employee employee;

    private String login;

    private String password;

    //Руководитель ОК, Сотрудник ОК, Администратор
    private String role;

    public User(String lastName, String firstName, String middleName, Date birthday, int passportSeries, int passportNumber,
                Position currentPosition, int personnelNumber, String scientificTitle, String scienceDegree, String email,
                Date contractStartDate, Date contractExpirationDate, String login, String password, String role) {
        super(lastName, firstName, middleName, birthday, passportSeries, passportNumber, currentPosition, personnelNumber,
                scientificTitle, scienceDegree, email, contractStartDate, contractExpirationDate);
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
} */