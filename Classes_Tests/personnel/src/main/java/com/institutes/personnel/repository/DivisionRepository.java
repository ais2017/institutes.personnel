package com.institutes.personnel.repository;

import com.institutes.personnel.model.Division;

import java.util.Collection;

public interface DivisionRepository {

    public Division saveDivision(Division division);

    public Collection<Division> findAll();

    /** Поиск Подразделения по идентификатору **/
    public Division findDivisionById(int id);


}
