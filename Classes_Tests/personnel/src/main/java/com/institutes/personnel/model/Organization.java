package com.institutes.personnel.model;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Collection;
import java.util.LinkedHashSet;

public class Organization {

    private Collection<Division> divisions;

    public Organization() {
        divisions = new LinkedHashSet<Division>();
    }

    public Organization(Collection<Division> divisions) {
        this.divisions = divisions;
    }

    public Collection<Division> getDivisions() {
        return divisions;
    }

    public void addDivision(Division division) {
        this.divisions.add(division);
    }

    public void deleteDivision(Division division) {
        this.divisions.remove(division);
    }

    //Возвращает объект Division в случае успеха. В случае, если объект не найден - null возвращается.
    public Division getDivisionByCode(String divisionCode) {
        if(this.divisions.size() < 1) {
            return null;
        }

        //Проходим в цикле все элементы сета divisions, для каждого элемента сравниваем divisionCode
        for(Division division: this.divisions) {
            if(division.getDivisionCode().equals(divisionCode))
                return division;
        }

        //Возвращаем null, в случае, если предыдущий цикл не нашел элемент
        return null;
    }

    //Возвращает объект Division в случае успеха. В случае, если объект не найден - null возвращается.
    public Division getDivisionByName(String divisionName) {
        if(this.divisions.size() < 1) {
            return null;
        }

        //Проходим в цикле все элементы сета divisions, для каждого элемента сравниваем divisionName
        for(Division division: this.divisions) {
            if(division.getDivisionName().equals(divisionName))
                return division;
        }

        //Возвращаем null, в случае, если предыдущий цикл не нашел элемент
        return null;
    }

    //Возвращает объект Division в случае успеха. В случае, если объект не найден - null возвращается.
    public Division getDivisionByCodeAndName(String divisionCode, String divisionName) {
        //throw new NotImplementedException();
        if(this.divisions.size() < 1) {
            return null;
        }

        //Проходим в цикле все элементы сета divisions, для каждого элемента сравниваем divisionCode и divisionName
        for(Division division: this.divisions) {
            if(division.getDivisionCode().equals(divisionCode) && division.getDivisionName().equals(divisionName))
                return division;
        }

        //Возвращаем null, в случае, если предыдущий цикл не нашел элемент
        return null;
    }

    //Возвращает коллекцию Сотрудников, принадлежащих Организации
    public Collection<Employee> getAllEmployeesOfOrganization() {
        Collection<Employee> employees = new LinkedHashSet<>();

        for(Division division: this.getDivisions()) {
            for (Position position : division.getPositions()) {
                for (Employee employee : position.getEmployees()) {
                    employees.add(employee);
                }
            }
        }

        if(employees.size() > 0)
            return employees;
        else return null;
    }
}
