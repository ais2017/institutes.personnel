package com.institutes.personnel.model;

import com.sun.istack.internal.NotNull;

import java.util.Collection;

//Класс Должность
public class Position {

    /** Primary key в таблице position_t **/
    private int id;

    /** Foreign key к таблице division_t **/
    private int divisionId;

    private String positionName;

    private int maxEmployees;

    private int vacantPositions;

    private Division parentDivision;

    private Collection<Employee> employees;

    /** Конструктор, используемый в репозитории. Не использовать в интерфейсе! **/
    public Position(int id, int divisionId, String positionName, int maxEmployees, int vacantPositions, Collection<Employee> employees) {

        if(maxEmployees < 1 || vacantPositions < 0 || vacantPositions > maxEmployees || positionName == null || positionName.equals("") || employees == null) {
            throw new RuntimeException("Cannot create Object Position with specified parameters");
        }

        this.id = id;
        this.divisionId = divisionId;

        this.positionName = positionName;
        this.maxEmployees = maxEmployees;
        this.vacantPositions = vacantPositions;
        this.parentDivision = null;
        this.employees = employees;

        //set currentPosition for employee in employees
        for(Employee employee: this.employees)
            employee.setCurrentPosition(this);

        /* Добавляем Должность в Division.positions родительского подразделения */
        //this.parentDivision.addPosition(this);
    }

    /** Основной конструктор Должности **/
    public Position(String positionName, int maxEmployees, int vacantPositions, Division parentDivision, Collection<Employee> employees) {

        if(maxEmployees < 1 || vacantPositions < 0 || vacantPositions > maxEmployees || positionName == null || positionName.equals("") || parentDivision == null || employees == null) {
            throw new RuntimeException("Cannot create Object Position with specified parameters");
        }

        this.id = -1;
        this.divisionId = parentDivision.getId();

        this.positionName = positionName;
        this.maxEmployees = maxEmployees;
        this.vacantPositions = vacantPositions;
        this.parentDivision = parentDivision;
        this.employees = employees;

        //set currentPosition for employee in employees
        for(Employee employee: this.employees)
            employee.setCurrentPosition(this);

        /* Добавляем Должность в Division.positions родительского подразделения */
        //this.parentDivision.addPosition(this);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public int getMaxEmployees() {
        return maxEmployees;
    }

    public void setMaxEmployees(int maxEmployees) {
        if(maxEmployees < 1 || this.vacantPositions > maxEmployees) {
            throw new RuntimeException("Incorrect maxEmployees");
        }
        else this.maxEmployees = maxEmployees;
    }

    public int getVacantPositions() {
        return vacantPositions;
    }

    public void setVacantPositions(int vacantPositions) {
        if(vacantPositions < 0 || vacantPositions > this.maxEmployees) {
            throw new RuntimeException("Incorrect vacantPositions");
        }
        else this.vacantPositions = vacantPositions;
    }

    public Division getParentDivision() {
        return parentDivision;
    }

    public void setParentDivision(Division parentDivision) {

        /* Добавляем должность в Division.positions родительского подразделения
         * Если Подразделение не совпадает с текущим, необходимо удалить эту должность из текущего Подразделения*/
        /*if(!this.parentDivision.equals(parentDivision))
            this.parentDivision.deletePosition(this); */
        this.parentDivision = parentDivision;
        //this.parentDivision.addPosition(this);
    }

    public void addEmployee(Employee employee) {
        //true if this collection changed as a result of the call
     //   if(employee != null && this.employees.add(employee)) {
        if(employee != null) {
            this.takePosition();
            this.employees.add(employee);
            employee.setCurrentPosition(this);
        }
    }

    public void deleteEmployee(Employee employee) {
        //true if an element was removed as a result of this call
     /*   if(this.employees.remove(employee))
            this.releasePosition(); */
        if(employee != null) {
            this.releasePosition();
            this.employees.remove(employee);
        }
    }

    public Collection<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Collection<Employee> employees) {
        this.employees = employees;
    }

    public void takePosition() {

        //Если свободных должностей 0, взять не можем
        if(this.vacantPositions < 1) {
            throw new RuntimeException("No vacant Positions");
        }
        this.vacantPositions--;
    }

    public void releasePosition() {

        //Количество свободных должностей не должно превышать максимального количества
        if(this.vacantPositions >= this.maxEmployees) {
            throw new RuntimeException("Maximum vacant Positions");
        }
        this.vacantPositions++;
    }

    public Employee searchEmployeeByPersonnelNumber(int pN) {
        //throw new NotImplementedException();
        if(this.employees.size() < 1) {
            return null;
        }

        //Проходим в цикле все элементы сета employees, для каждого элемента сравниваем
        for(Employee employee: this.employees) {
            if(employee.getPersonnelNumber() == pN)
                return employee;
        }

        //Возвращаем null, в случае, если предыдущий цикл не нашел элемент
        return null;
    }

    public Employee searchEmployeeByPassportData(int passportSeries, int passportNumber) {
        //throw new NotImplementedException();
        if(this.employees.size() < 1) {
            return null;
        }

        //Проходим в цикле все элементы сета employees, для каждого элемента сравниваем
        for(Employee employee: this.employees) {
            if(employee.getPassportSeries() == passportSeries && employee.getPassportNumber() == passportNumber)
                return employee;
        }

        //Возвращаем null, в случае, если предыдущий цикл не нашел элемент
        return null;
    }
}
