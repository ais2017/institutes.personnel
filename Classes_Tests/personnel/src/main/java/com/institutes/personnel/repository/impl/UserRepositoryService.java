package com.institutes.personnel.repository.impl;

import com.institutes.personnel.model.Employee;
import com.institutes.personnel.model.User;
import com.institutes.personnel.repository.EmployeeRepository;
import com.institutes.personnel.repository.UserRepository;
import com.institutes.personnel.service.DatabaseConnectionService;
import com.institutes.personnel.service.DatabaseOperationService;
import com.institutes.personnel.service.impl.DatabaseConnectionServiceBean;
import com.institutes.personnel.service.impl.DatabaseOperationServiceBean;

import java.sql.*;
import java.util.Collection;


public class UserRepositoryService implements UserRepository {

//    private DatabaseConnectionService databaseConnectionService;
//
//    private DatabaseOperationService databaseOperationService;
//
//    private EmployeeRepository employeeRepository;
//
//    public UserRepositoryService() {
//        databaseConnectionService = new DatabaseConnectionServiceBean();
//        databaseOperationService = new DatabaseOperationServiceBean();
//        employeeRepository = new EmployeeRepositoryService();
//    }

    @Override
    public Collection<User> findAll() {
        return null;
    }

//    @Override
//    public User findUserByLogin(String login) {
//        User user = this.databaseOperationService.findUserByLogin(login);
//
//        return user;
//    }

    @Override
    public User findUserByLogin(String login) {
        return null;
    }


/** Было: return null **/
    /*@Override
    public User findUserByLogin(String login) {

        User user = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT * FROM user_t u WHERE(u.LOGIN = ?)";

        try {

            *//** Получаем соединение **//*
            connection = databaseConnectionService.getConnection();
            if(connection == null)
                throw new RuntimeException("Not connected to database");

            *//** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
             *  необходим курсор, с помощью которого можно перемещаться по записям **//*
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setString(1, login);

            resultSet = statement.executeQuery();

            *//** Получаем количество строк, которое было получено после выполнения запроса **//*
            resultSet.last();
            int numRows = resultSet.getRow();

            *//** Если запрос вернул несколько строк, выбрасываем исключение **//*
            if(numRows > 1)
                throw new RuntimeException("Several Users with login " + login + "found");
            else if(numRows == 0)
                return null;

            resultSet.beforeFirst();
            while(resultSet.next()) {
                String lg = resultSet.getString("LOGIN");
                String pswd = resultSet.getString("PASSWORD");
                String role = resultSet.getString("ROLE");
                int personId = resultSet.getInt("PERSONID");

                *//** Получаем объект Сотрудника, с которым связан этот Пользователь **//*
                Employee employee = this.employeeRepository.findEmployeeById(personId);

                *//** Если объект Сотрудника не удалось инциализировать - поиск считаем неуспешным
                 *  ведь без Сотрудника нельзя создать Пользователя **//*
                if(employee == null)
                    return null;

                *//** Создаем объект Пользователя **//*
                user = new User(employee, lg, pswd, role);
            }

            //Закрываем
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

        return user;

        //return null;
    }*/

    @Override
    public User saveUser(User user) {
        return user;
    }

    @Override
    public void deleteUser(User user) {

    }
}
