package com.institutes.personnel.model;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.util.Date;

public class Person {

    @NotNull
    private String lastName;

    @NotNull
    private String firstName;

    @NotNull
    private String middleName;

    @NotNull
    private Date birthday;

    @NotNull
    private int passportSeries;

    @NotNull
    private int passportNumber;

    public Person(String lastName, String firstName, String middleName, Date birthday, int passportSeries, int passportNumber) {

        /* Проверка not null */
        if(lastName == null || firstName == null || middleName == null || birthday == null || lastName.equals("") || firstName.equals("") || middleName.equals(""))
            throw new RuntimeException("Null fields not allowed");
        else {
            this.lastName = lastName;
            this.firstName = firstName;
            this.middleName = middleName;
            this.birthday = birthday;
        }

        /* Проверки на корректность серии/номера паспорта */
        if(passportSeries < 1000 || passportSeries > 9999)
            throw new RuntimeException("passportSeries is incorrect");
        else this.passportSeries = passportSeries;

        if(passportNumber < 100000 || passportNumber > 999999)
            throw new RuntimeException("passportNumber is incorrect");
        else this.passportNumber = passportNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getPassportSeries() {
        return passportSeries;
    }

    public void setPassportSeries(int passportSeries) {
        this.passportSeries = passportSeries;
    }

    public int getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(int passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
}
