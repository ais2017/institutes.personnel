package com.institutes.personnel.service;

import java.sql.Connection;
import java.sql.SQLException;

public interface DatabaseConnectionService {

    public Connection getConnection() throws SQLException;

}
