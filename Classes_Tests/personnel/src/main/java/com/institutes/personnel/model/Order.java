package com.institutes.personnel.model;

import java.util.Collection;
import java.util.LinkedHashSet;

public class Order {

    private Collection<OrderLine> orderLines;

    /** Primary key в таблице order_t **/
    private int id;

    //Прием на работу, Перевод на другую Должность, Увольнение
    private String type;

    public Order(String type) {
        this.id = -1;
        this.orderLines = new LinkedHashSet<OrderLine>();
        this.type = type;
    }

    public Order(Collection<OrderLine> orderLines, String type) {
        this.id = -1;
        this.orderLines = orderLines;
        this.type = type;
    }

    public Order(int id, Collection<OrderLine> orderLines, String type) {
        this.id = id;
        this.orderLines = orderLines;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Collection<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(Collection<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void addOrderLine(OrderLine orderLine) {
        this.orderLines.add(orderLine);
    }

    public void deleteOrderLine(OrderLine orderLine) {
        this.orderLines.remove(orderLine);
    }

    //Возвращает экземпляр OrderLine в случае успеха, null в случае неудачи
    public OrderLine searchOrderLineByEmployee(Employee employee) {
        /* Новый объект, которого раньше не было в БД */
        this.id = -1;

        if(this.orderLines.size() < 1)
            return null;

        for(OrderLine orderLine: this.orderLines) {
            if(orderLine.getEmployee().equals(employee))
                return orderLine;
        }

        //Если предыдущий цикл не дал результата возвратить null
        return null;
    }

    //Дополнительный конструктор с id
    public OrderLine searchOrderLineByEmployee(int id, Employee employee) {
        this.id = id;

        if(this.orderLines.size() < 1)
            return null;

        for(OrderLine orderLine: this.orderLines) {
            if(orderLine.getEmployee().equals(employee))
                return orderLine;
        }

        //Если предыдущий цикл не дал результата возвратить null
        return null;
    }
}
