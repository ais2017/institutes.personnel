package com.institutes.personnel.flow;

public interface ExecutionFlowService {

    /** Начало работы. Здесь определяется, что за Пользователь хочет воспользоваться системой - в соответствии с этим
     *  поток выполнения, функциональность системы и интерфейсы будут меняться **/
    public void start();

    /** После авторизации, работать как Руководитель ОК **/
    public void startHRManager();

    /** После авторизации, работать как Сотрудник ОК **/
    public void startHREmployee();

    /** После авторизации, работать как Администратор **/
    public void startAdministrator();

    /** Загрузка данных о текущей организации, всех ее подразделениях, должностях, сотрудниках **/
    public void loadOrganization();
}
