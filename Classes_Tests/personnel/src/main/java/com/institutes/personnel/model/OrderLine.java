package com.institutes.personnel.model;

import java.util.Date;

public class OrderLine {

    /** Primary key в таблице orderline_t **/
    private int id;

    /** Foreign key к таблице order_t **/
    private int orderId;

    /** Foreign key к таблице division_t **/
    private int divisionId;

    /** Foreign key к таблице position_t **/
    private int positionId;

    /** Foreign key к таблице person_t **/
    private int personId;

    private Employee employee;

    private Position position;

    private Division division;

    private Date startDate;

    private Date endDate;

    //Основной конструктор, изначально использовавшийся
    public OrderLine(Employee employee, Position position, Division division, Date startDate, Date endDate) {
        //Чтобы не ломать старую логику. Все идентификаторы = -1, новый объект, которого еще не было в БД
        this.id = -1;
        this.orderId = -1;
        this.divisionId = division.getId();
        this.positionId = position.getId();
        this.personId = employee.getId();

        this.employee = employee;
        this.position = position;
        this.division = division;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    //Новый конструктор, со всеми идентификаторами
    public OrderLine(int id, int orderId, int divisionId, int positionId, int personId, Employee employee, Position position, Division division, Date startDate, Date endDate) {
        this.id = id;
        this.orderId = orderId;
        this.divisionId = divisionId;
        this.positionId = positionId;
        this.personId = personId;
        this.employee = employee;
        this.position = position;
        this.division = division;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Division getDivision() {
        return division;
    }

    public void setDivision(Division division) {
        this.division = division;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }
}
