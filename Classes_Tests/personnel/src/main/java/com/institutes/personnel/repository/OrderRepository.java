package com.institutes.personnel.repository;

import com.institutes.personnel.model.Order;

import java.util.Collection;

public interface OrderRepository {

    public Order saveOrder(Order order);

    public Collection<Order> findAll();

    public Collection<Order> saveAll(Collection<Order> orders);

}
