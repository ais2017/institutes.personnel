package com.institutes.personnel.service.mock;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;

public interface ScientificTitleService {

    /* Получить список заданных научных званий */
    /* Заглушка на время отсутствия БД */
    static Collection<String> findAll() {
        return new LinkedHashSet<String>(Arrays.asList("Отсутствует", "Доцент", "Профессор"));
    }

}
