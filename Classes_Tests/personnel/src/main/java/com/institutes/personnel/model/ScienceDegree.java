package com.institutes.personnel.model;

import com.institutes.personnel.service.mock.ScienceDegreeService;

import java.util.LinkedHashSet;

//Список предопределенных значений
public class ScienceDegree {

    private LinkedHashSet<String> scienceDegrees;

    public ScienceDegree() {
        this.scienceDegrees = new LinkedHashSet<String>(ScienceDegreeService.findAll());
    }

    public ScienceDegree(LinkedHashSet<String> scienceDegrees) {
        this.scienceDegrees = scienceDegrees;
    }

    public LinkedHashSet<String> getScienceDegrees() {
        return scienceDegrees;
    }

    public void setScienceDegrees(LinkedHashSet<String> scienceDegrees) {
        this.scienceDegrees = scienceDegrees;
    }

    public void addScienceDegree(String scienceDegree) {
        for (String item: this.scienceDegrees) {
            if(item.equals(scienceDegree)) {
                return;
            }
        }

        this.scienceDegrees.add(scienceDegree);
    }

    public boolean deleteScienceDegree(String scienceDegree) {
        boolean setContainedElement = this.scienceDegrees.remove(scienceDegree);

        //true if the set contained the specified element
        return setContainedElement;
    }

    //Проверяет, есть ли строка scienceDegree в коллекции scienceDegrees
    public boolean checkScienceDegreeExists(String scienceDegree) {
        //Returns true if this set contains the specified element.
        //More formally, returns true if and only if this set contains an element e such that (o==null ? e==null : o.equals(e))
        if(this.scienceDegrees.contains(scienceDegree))
            return true;
        else return false;
    }

}
