package com.institutes.personnel.service;

import com.institutes.personnel.model.*;

import java.sql.SQLException;
import java.util.Collection;

public interface DatabaseOperationService {

    /** Поиск Сотрудника по идентификатору **/
    public Employee findEmployeeById(int id);

    /** Поиск Должности по идентификатору **/
    public Position findPositionById(int id);

    /** Поиск Подразделения по идентификатору **/
    public Division findDivisionById(int id);

    /** Поиск всех Подразделений, входящих в Организацию **/
    public Collection<Division> findDivisionsByOrganizationId(int organizationId);

    /** Найти пользователя по логину **/
    public User findUserByLogin(String login);

    /** Только одна Организация **/
    public Organization findOrganization();

    /** Есть только один объект Персонал **/
    public Staff getStaff();

    /** Загрузка всех пользователей **/
    public Collection<User> findUsers();

    /** Поиск всех Сотрудников, занимающих определенную Должность.
     *  Этот метод возвращает коллекцию Сотрудников, у которых currentPosition = null!
     *  Предполагаемое применение: в методе PositionRepository.findPositionById
     *  для корректной инициализации Должности. Каждому Сотруднику
     *  будет присвоен currentPosition в конструкторе Position**/
    public Collection<Employee> findEmployeesByPositionIdForCreatingPosition(int positionId);

    /** Поиск всех Должностей, принадлежащих определенному Подразделению.
     *  Этот метод возвращает коллекцию Должностей, у которых parentDivision = null!
     *  Предполагаемое применение: в методе DivisionRepository.findDivisionById
     *  для корректной инициализации Подразделения. Каждой Должности
     *  будет присвоен parentDivision в конструкторе Division **/
    public Collection<Position> findPositionsByDivisionIdForCreatingDivision(int divisionId);

    public Employee saveEmployee(Employee employee);

    public Order saveOrder(Order order);

    public Collection<OrderLine> saveOrderLines(Collection<OrderLine> orderLines);

    /* Возвращает null в случае неудачи */
    public Position savePosition(Position position);

    public Division saveDivision(Division division);

    /** Сохранить Пользователя **/
    public User saveUser(User user);

    /** Внесение данных о новых сотрудниках (сразу несколько, если потребуется - в BPMN только по одному)
     *  В случае успеха возвращает список сохраненных сотрудников, в случае неудачи null**/
    public Collection<Employee> saveEmployees(Collection<Employee> employees);

    public void deletePosition(Position position);

    /** Удалить Пользователя **/
    public void deleteUser(User user);

    /** Проверка логина\пароля **/
    public boolean checkCredentials(String login, String password);

}
