package com.institutes.personnel.repository;

import com.institutes.personnel.model.User;

import java.util.Collection;

/* Интерфейс к хранилищу пользователей */
public interface UserRepository {

    /* Получить всех пользователей */
    public Collection<User> findAll();

    /* Найти пользователя по логину */
    public User findUserByLogin(String login);

    /* Сохранить Пользователя */
    public User saveUser(User user);

    /* Удалить Пользователя */
    public void deleteUser(User user);

}
