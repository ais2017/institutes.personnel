package com.institutes.personnel.service;

import com.institutes.personnel.model.Employee;
import com.institutes.personnel.model.Position;
import com.institutes.personnel.model.Staff;
import com.institutes.personnel.model.User;

import java.util.Collection;

/* Основные бизнес-процессы */
public interface BusinessProcessService {

    /** Авторизация пользователя в системе **/
    public User authorization();

    /** Изменение данных о существующих сотрудниках **/
    public Employee changeEmployeeData(User user);

    /** Внесение данных о новых сотрудниках (только об одном) **/
    public Employee hireEmployee(User user);

    /** Внесение новых данных в штатное расписание **/
    public Position addDataToStaffList(User user);

    /** Изменение данных в штатном расписании **/
    public Position changeDataInStaffList(User user);

    /** Выдача списка свободных Должностей по Подразделению **/
    public Collection<Position> getVacantPositionListByDivision(User user);

    /** Выдача списка свободных Должностей по всем Подразделениям **/
    public Collection<Position> getVacantPositionListByOrganization(User user);

    /** Регистрация в системе Пользователя **/
    public User registerUser(User currentUser);

    /** Настройка прав доступа, изменение прав доступа **/
    public User setAccessRights(User currentUser);

                                                                                                /** Оповещение Сотрудников, чьи контракты в скором времени заканчиваются **/
    public Collection<Employee> getEmployeesWithExpiringContracts();

    /** Внесение данных о новых сотрудниках (сразу несколько, если потребуется - в BPMN только по одному) **/
    public Collection<Employee> hireEmployees(User user);

}
