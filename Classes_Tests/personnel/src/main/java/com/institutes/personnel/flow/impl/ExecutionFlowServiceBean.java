package com.institutes.personnel.flow.impl;

import com.institutes.personnel.flow.ExecutionFlowService;
import com.institutes.personnel.model.Employee;
import com.institutes.personnel.model.Organization;
import com.institutes.personnel.model.Staff;
import com.institutes.personnel.model.User;
import com.institutes.personnel.service.BusinessProcessService;
import com.institutes.personnel.service.DatabaseOperationService;
import com.institutes.personnel.service.UserInteractionService;
import com.institutes.personnel.service.impl.DatabaseOperationServiceBean;
import com.institutes.personnel.service.impl.UserInteractionServiceBean;

import javax.swing.*;

public class ExecutionFlowServiceBean implements ExecutionFlowService {

    private UserInteractionService userInteractionService;
    private BusinessProcessService businessProcessService;
    private DatabaseOperationService databaseOperationService;

    /* Авторизованный пользователь приложения */
    private User authorizedUser;

    /* Объект организации */
    private Organization organization;

    public ExecutionFlowServiceBean() throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        this.databaseOperationService = new DatabaseOperationServiceBean();
     //   this.userInteractionService = new UserInteractionServiceBean();
        this.userInteractionService = new UserInteractionServiceBean(this);
        this.businessProcessService = ((UserInteractionServiceBean) this.userInteractionService).getBusinessProcessService();
        this.authorizedUser = null;
        this.organization = null;
    }

    public UserInteractionService getUserInteractionService() {
        return userInteractionService;
    }

    public void setUserInteractionService(UserInteractionService userInteractionService) {
        this.userInteractionService = userInteractionService;
    }

    public BusinessProcessService getBusinessProcessService() {
        return businessProcessService;
    }

    public void setBusinessProcessService(BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

    public DatabaseOperationService getDatabaseOperationService() {
        return databaseOperationService;
    }

    public void setDatabaseOperationService(DatabaseOperationService databaseOperationService) {
        this.databaseOperationService = databaseOperationService;
    }

    @Override
    public void start() {

        /* Авторизация пользователя в системе */
        this.authorizedUser = this.businessProcessService.authorization();

        if(this.authorizedUser == null) {
            this.userInteractionService.showError("Превышено количество попыток входа в систему! Отказано в доступе");
            System.exit(1);
        }

     //   System.out.println("User " + this.authorizedUser.getLogin());
     //   this.userInteractionService.showMessage("Добрый день, " + this.authorizedUser.getEmployee().getFirstName() + " " + this.authorizedUser.getEmployee().getMiddleName());

        /* Загружаем данные об организации */
        this.loadOrganization();

        switch(this.authorizedUser.getRole()) {
            case "Руководитель ОК":
                this.startHRManager();
                break;
            case "Сотрудник ОК":
                this.startHREmployee();
                break;
            case "Администратор":
                this.startAdministrator();
                break;
            default:
                this.userInteractionService.showMessage("Роль " + this.authorizedUser.getRole() + " пользователя некорректна");
                System.exit(1);
        }

     //   System.exit(0);

    }

    @Override
    public void loadOrganization() {

        /* Загружаем объект организации */
        this.organization = this.databaseOperationService.findOrganization();

        if(this.organization == null) {
            this.userInteractionService.showMessage("Ошибка при загрузке данных об Организации");
            System.exit(1);
        }

        /* Передаем информацию об организации в userInteractionService */
        this.userInteractionService.setOrganization(this.organization);


        /* Так как был создан объект организации, в ней содержится тот же объект пользователя, что и у authorizedUser, но с другой ссылкой.
         * Поэтому необходимо заменить работника для текущего пользователя на новый из организации */
        Staff staff = new Staff(this.organization.getAllEmployeesOfOrganization());
        Employee newEmployee = staff.searchEmployeeByPassportData(this.authorizedUser.getEmployee().getPassportSeries(), this.authorizedUser.getEmployee().getPassportNumber());
        if(newEmployee != null) {
            this.authorizedUser.setEmployee(newEmployee);

            /* Передаем информацию о пользователе в userInteractionService */
            this.userInteractionService.setAuthorizedUser(this.authorizedUser);
        }
        else {
            this.userInteractionService.showMessage("Ошибка при поиске сотрудника текущего пользователя в рамках загруженной организации");
            System.exit(1);
        }
    }

    @Override
    public void startHRManager() {
        this.userInteractionService.showMainHRManagerForm();
    }

    @Override
    public void startHREmployee() {
        this.userInteractionService.showMainHREmployeeForm();
    }

    @Override
    public void startAdministrator() {
        this.userInteractionService.showMainAdministratorForm();
    }
}