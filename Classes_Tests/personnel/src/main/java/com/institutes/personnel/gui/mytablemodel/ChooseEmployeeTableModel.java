package com.institutes.personnel.gui.mytablemodel;

import com.institutes.personnel.model.Employee;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

public class ChooseEmployeeTableModel implements TableModel {

    private Collection<TableModelListener> listeners = new LinkedHashSet<>();

    private List<Employee> employees;

    public ChooseEmployeeTableModel(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public int getRowCount() {
        return employees.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Должность";
            case 1:
                return "Фамилия";
            case 2:
                return "Имя";
            case 3:
                return "Отчество";
            case 4:
                return "Серия паспорта";
            case 5:
                return "Номер паспорта";
            case 6:
                return "Табельный номер";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Employee employee = employees.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return employee.getCurrentPosition().getPositionName();
            case 1:
                return employee.getLastName();
            case 2:
                return employee.getFirstName();
            case 3:
                return employee.getMiddleName();
            case 4:
                return employee.getPassportSeries();
            case 5:
                return employee.getPassportNumber();
            case 6:
                return employee.getPersonnelNumber();
        }
        return "";
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }
}
