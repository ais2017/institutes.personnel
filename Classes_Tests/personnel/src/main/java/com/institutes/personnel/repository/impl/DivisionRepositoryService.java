package com.institutes.personnel.repository.impl;

import com.institutes.personnel.model.Division;
import com.institutes.personnel.model.Position;
import com.institutes.personnel.repository.DivisionRepository;
import com.institutes.personnel.repository.PositionRepository;
import com.institutes.personnel.service.DatabaseConnectionService;
import com.institutes.personnel.service.impl.DatabaseConnectionServiceBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

public class DivisionRepositoryService implements DivisionRepository {

/*    private DatabaseConnectionService databaseConnectionService;

    private PositionRepository positionRepository;

    public DivisionRepositoryService() {

        this.databaseConnectionService = new DatabaseConnectionServiceBean();
        this.positionRepository = new PositionRepositoryService();

    }*/

    @Override
    public Division saveDivision(Division division) {
        return division;
    }

    @Override
    public Collection<Division> findAll() {
        return null;
    }

    @Override
    public Division findDivisionById(int id) {
        return null;
    }

    //    @Override
//    public Division findDivisionById(int id) {
//        Division division = null;
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        String query = "SELECT d.*" +
//                       "FROM division_t d" +
//                       "WHERE(d.ID = ?)";
//
//        try {
//            connection = databaseConnectionService.getConnection();
//            if(connection == null)
//                throw new RuntimeException("Not connected to database");
//
//            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
//             *  необходим курсор, с помощью которого можно перемещаться по записям **/
//            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//            statement.setInt(1, id);
//
//            resultSet = statement.executeQuery();
//
//            /** Получаем количество строк, которое было получено после выполнения запроса **/
//            resultSet.last();
//            int numRows = resultSet.getRow();
//
//            /** Если запрос вернул несколько строк, выбрасываем исключение **/
//            if(numRows > 1)
//                throw new RuntimeException("Several Divisions with id " + id + "found");
//            else if(numRows == 0)
//                return null;
//
//            resultSet.beforeFirst();
//            while(resultSet.next()) {
//                String divisionCode = resultSet.getString("DIVISIONCODE");
//                String divisionName = resultSet.getString("DIVISIONNAME");
//
//                /** Получаем коллекцию Должностей, связанных с этим Подразделением **/
//                Collection<Position> positions = this.positionRepository.findPositionsByDivisionIdForCreatingDivision(id);
//
//                /** Создаем объект Подразделения **/
//                division = new Division(divisionCode, divisionName, positions);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return division;
//    }
}
