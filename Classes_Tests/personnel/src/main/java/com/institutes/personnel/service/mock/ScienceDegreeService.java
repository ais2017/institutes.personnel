package com.institutes.personnel.service.mock;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;

public interface ScienceDegreeService {

    /* Получить список заданных научных степеней */
    /* Заглушка на время отсутствия БД */
    static Collection<String> findAll() {
        return new LinkedHashSet<String>(Arrays.asList("Отсутствует", "Кандидат наук", "Доктор наук"));
    }

}
