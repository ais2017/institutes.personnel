package com.institutes.personnel.gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedHashSet;

import com.institutes.personnel.model.Employee;
import com.institutes.personnel.model.Position;
import com.institutes.personnel.model.ScientificTitle;
import com.institutes.personnel.model.User;
import com.institutes.personnel.service.mock.ScienceDegreeService;
import com.institutes.personnel.service.mock.ScientificTitleService;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.demo.DateChooserPanel;

public class EmployeeEditorForm extends JFrame {
    private JTextField lastNameTextField;
    private JPanel rootPanel;
    private JLabel lastNameLabel;
    private JLabel firstNameLabel;
    private JTextField firstNameTextField;
    private JLabel middleNameLabel;
    private JTextField middleNameTextField;
    private JLabel birthdayLabel;
    private JLabel passportSeriesLabel;
    private JLabel passportNumberLabel;
    private JLabel personnelNumberLabel;
    private JTextField passportSeriesTextField;
    private JTextField passportNumberTextField;
    private JTextField personnelNumberTextField;
    private JLabel scientificTitleLabel;
    //    private JTextField scientificTitleTextField;
    private JLabel scienceDegreeLabel;
    //    private JTextField scienceDegreeTextField;
    private JLabel emailLabel;
    private JTextField emailTextField;
    private JLabel contractStartDateLabel;
    private JLabel contractExpirationDateLabel;
    private JButton confirmButton;
    private JDateChooser birthdayDateChooser;
    private JDateChooser contractStartDateChooser;
    private JDateChooser contractExpirationDateChooser;
    private JLabel employeeEditorLabel;
    private JLabel currentPositionLabel;
    private JComboBox currentPositionComboBox;
    private JComboBox scientificTitleComboBox;
    private JComboBox scienceDegreeComboBox;

    private boolean confirmButtonClicked;
    private User authorizedUser;

    public EmployeeEditorForm(User user) {

        add(rootPanel);
        setTitle("Карточка сотрудника");
        setSize(550, 500);
        rootPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        confirmButtonClicked = false;
        authorizedUser = user;

        /* Настройка выпадающих списков */
        for (Position position : user.getEmployee().getCurrentPosition().getParentDivision().getPositions()) {
            currentPositionComboBox.addItem(position.getPositionName());
        }

        Collection<String> scientificTitleStrings = ScientificTitleService.findAll();
        for (String scientificTitle : scientificTitleStrings)
            scientificTitleComboBox.addItem(scientificTitle);

        Collection<String> scienceDegreeStrings = ScienceDegreeService.findAll();
        for (String scienceDegree : scienceDegreeStrings)
            scienceDegreeComboBox.addItem(scienceDegree);


        /* DateChooser */
        //    birthdayDateChooser = new JDateChooser();
        //    birthdayDateChooser.setSize(200, 30);
        //   birthdayDateChooser.setBounds(20, 20, 200, 30); // Modify depending on your preference
        //    getContentPane().add(birthdayDateChooser);

        //   contractStartDateChooser = new JDateChooser();
        //   contractStartDateChooser.setSize(200, 30);
        //contractStartDateChooser.setBounds(20, 20, 200, 30); // Modify depending on your preference
        //   getContentPane().add(contractStartDateChooser);

        //    contractExpirationChooser = new JDateChooser();
        //   contractStartDateChooser.setSize(200, 30);
        //contractExpirationChooser.setBounds(20, 20, 200, 30); // Modify depending on your preference
        //   getContentPane().add(contractExpirationChooser);

        confirmButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                confirmButtonClicked = true;
            }
        });

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    public void clearFormFields() {
        this.lastNameTextField.setText("");
        this.firstNameTextField.setText("");
        this.middleNameTextField.setText("");
        this.birthdayDateChooser.setDate(null);
        this.contractStartDateChooser.setDate(null);
        this.contractExpirationDateChooser.setDate(null);
        this.passportSeriesTextField.setText("");
        this.passportNumberTextField.setText("");
        this.emailTextField.setText("");
        this.personnelNumberTextField.setText("");
        this.confirmButtonClicked = false;
    }

    public void setFormFields(Employee employee) {
        this.lastNameTextField.setText(employee.getLastName());
        this.firstNameTextField.setText(employee.getFirstName());
        this.middleNameTextField.setText(employee.getMiddleName());
        this.birthdayDateChooser.setDate(employee.getBirthday());
        this.contractStartDateChooser.setDate(employee.getContractStartDate());
        this.contractExpirationDateChooser.setDate(employee.getContractExpirationDate());
        this.passportSeriesTextField.setText(Integer.toString(employee.getPassportSeries()));
        this.passportNumberTextField.setText(Integer.toString(employee.getPassportNumber()));
        this.emailTextField.setText(employee.getEmail());
        this.personnelNumberTextField.setText(Integer.toString(employee.getPersonnelNumber()));
        this.currentPositionComboBox.setSelectedItem(employee.getCurrentPosition().getPositionName());
        this.scientificTitleComboBox.setSelectedItem(employee.getScientificTitle());
        this.scienceDegreeComboBox.setSelectedItem(employee.getScienceDegree());
        this.confirmButtonClicked = false;
    }

    public boolean isConfirmButtonClicked() {
        return confirmButtonClicked;
    }

    public void setConfirmButtonClicked(boolean confirmButtonClicked) {
        this.confirmButtonClicked = confirmButtonClicked;
    }

    public JTextField getLastNameTextField() {
        return lastNameTextField;
    }

    public void setLastNameTextField(JTextField lastNameTextField) {
        this.lastNameTextField = lastNameTextField;
    }

    public JTextField getFirstNameTextField() {
        return firstNameTextField;
    }

    public void setFirstNameTextField(JTextField firstNameTextField) {
        this.firstNameTextField = firstNameTextField;
    }

    public JTextField getMiddleNameTextField() {
        return middleNameTextField;
    }

    public void setMiddleNameTextField(JTextField middleNameTextField) {
        this.middleNameTextField = middleNameTextField;
    }

    public JTextField getPassportSeriesTextField() {
        return passportSeriesTextField;
    }

    public void setPassportSeriesTextField(JTextField passportSeriesTextField) {
        this.passportSeriesTextField = passportSeriesTextField;
    }

    public JTextField getPassportNumberTextField() {
        return passportNumberTextField;
    }

    public void setPassportNumberTextField(JTextField passportNumberTextField) {
        this.passportNumberTextField = passportNumberTextField;
    }

    public JTextField getPersonnelNumberTextField() {
        return personnelNumberTextField;
    }

    public void setPersonnelNumberTextField(JTextField personnelNumberTextField) {
        this.personnelNumberTextField = personnelNumberTextField;
    }

    public JTextField getEmailTextField() {
        return emailTextField;
    }

    public void setEmailTextField(JTextField emailTextField) {
        this.emailTextField = emailTextField;
    }

    public JDateChooser getBirthdayDateChooser() {
        return birthdayDateChooser;
    }

    public void setBirthdayDateChooser(JDateChooser birthdayDateChooser) {
        this.birthdayDateChooser = birthdayDateChooser;
    }

    public JDateChooser getContractStartDateChooser() {
        return contractStartDateChooser;
    }

    public void setContractStartDateChooser(JDateChooser contractStartDateChooser) {
        this.contractStartDateChooser = contractStartDateChooser;
    }

    public JDateChooser getContractExpirationDateChooser() {
        return contractExpirationDateChooser;
    }

    public void setContractExpirationDateChooser(JDateChooser contractExpirationChooser) {
        this.contractExpirationDateChooser = contractExpirationChooser;
    }

    public JComboBox getCurrentPositionComboBox() {
        return currentPositionComboBox;
    }

    public void setCurrentPositionComboBox(JComboBox currentPositionComboBox) {
        this.currentPositionComboBox = currentPositionComboBox;
    }

    public JComboBox getScientificTitleComboBox() {
        return scientificTitleComboBox;
    }

    public void setScientificTitleComboBox(JComboBox scientificTitleComboBox) {
        this.scientificTitleComboBox = scientificTitleComboBox;
    }

    public JComboBox getScienceDegreeComboBox() {
        return scienceDegreeComboBox;
    }

    public void setScienceDegreeComboBox(JComboBox scienceDegreeComboBox) {
        this.scienceDegreeComboBox = scienceDegreeComboBox;
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        rootPanel = new JPanel();
        rootPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(15, 4, new Insets(0, 0, 0, 0), -1, -1));
        lastNameLabel = new JLabel();
        lastNameLabel.setText("Фамилия");
        rootPanel.add(lastNameLabel, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lastNameTextField = new JTextField();
        rootPanel.add(lastNameTextField, new com.intellij.uiDesigner.core.GridConstraints(1, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        firstNameLabel = new JLabel();
        firstNameLabel.setText("Имя");
        rootPanel.add(firstNameLabel, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        firstNameTextField = new JTextField();
        rootPanel.add(firstNameTextField, new com.intellij.uiDesigner.core.GridConstraints(2, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        middleNameLabel = new JLabel();
        middleNameLabel.setText("Отчество");
        rootPanel.add(middleNameLabel, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        middleNameTextField = new JTextField();
        rootPanel.add(middleNameTextField, new com.intellij.uiDesigner.core.GridConstraints(3, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        birthdayLabel = new JLabel();
        birthdayLabel.setText("Дата рождения");
        rootPanel.add(birthdayLabel, new com.intellij.uiDesigner.core.GridConstraints(4, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        passportSeriesLabel = new JLabel();
        passportSeriesLabel.setText("Серия паспорта");
        rootPanel.add(passportSeriesLabel, new com.intellij.uiDesigner.core.GridConstraints(5, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        passportNumberLabel = new JLabel();
        passportNumberLabel.setText("Номер паспорта");
        rootPanel.add(passportNumberLabel, new com.intellij.uiDesigner.core.GridConstraints(6, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        personnelNumberLabel = new JLabel();
        personnelNumberLabel.setText("Табельный номер");
        rootPanel.add(personnelNumberLabel, new com.intellij.uiDesigner.core.GridConstraints(8, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        passportSeriesTextField = new JTextField();
        rootPanel.add(passportSeriesTextField, new com.intellij.uiDesigner.core.GridConstraints(5, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        passportNumberTextField = new JTextField();
        rootPanel.add(passportNumberTextField, new com.intellij.uiDesigner.core.GridConstraints(6, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        personnelNumberTextField = new JTextField();
        rootPanel.add(personnelNumberTextField, new com.intellij.uiDesigner.core.GridConstraints(8, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        scientificTitleLabel = new JLabel();
        scientificTitleLabel.setText("Научное звание");
        rootPanel.add(scientificTitleLabel, new com.intellij.uiDesigner.core.GridConstraints(9, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        scienceDegreeLabel = new JLabel();
        scienceDegreeLabel.setText("Научная степень");
        rootPanel.add(scienceDegreeLabel, new com.intellij.uiDesigner.core.GridConstraints(10, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        emailLabel = new JLabel();
        emailLabel.setText("Email");
        rootPanel.add(emailLabel, new com.intellij.uiDesigner.core.GridConstraints(11, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        emailTextField = new JTextField();
        rootPanel.add(emailTextField, new com.intellij.uiDesigner.core.GridConstraints(11, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        contractStartDateLabel = new JLabel();
        contractStartDateLabel.setText("Дата начала действия контракта");
        rootPanel.add(contractStartDateLabel, new com.intellij.uiDesigner.core.GridConstraints(12, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        contractExpirationDateLabel = new JLabel();
        contractExpirationDateLabel.setText("Дата истечения действия контракта");
        rootPanel.add(contractExpirationDateLabel, new com.intellij.uiDesigner.core.GridConstraints(13, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JToolBar.Separator toolBar$Separator1 = new JToolBar.Separator();
        rootPanel.add(toolBar$Separator1, new com.intellij.uiDesigner.core.GridConstraints(9, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JToolBar.Separator toolBar$Separator2 = new JToolBar.Separator();
        rootPanel.add(toolBar$Separator2, new com.intellij.uiDesigner.core.GridConstraints(12, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        confirmButton = new JButton();
        confirmButton.setText("Подтвердить");
        rootPanel.add(confirmButton, new com.intellij.uiDesigner.core.GridConstraints(14, 1, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        birthdayDateChooser = new JDateChooser();
        rootPanel.add(birthdayDateChooser, new com.intellij.uiDesigner.core.GridConstraints(4, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        contractStartDateChooser = new JDateChooser();
        rootPanel.add(contractStartDateChooser, new com.intellij.uiDesigner.core.GridConstraints(12, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        contractExpirationDateChooser = new JDateChooser();
        rootPanel.add(contractExpirationDateChooser, new com.intellij.uiDesigner.core.GridConstraints(13, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        employeeEditorLabel = new JLabel();
        Font employeeEditorLabelFont = this.$$$getFont$$$(null, Font.BOLD, 16, employeeEditorLabel.getFont());
        if (employeeEditorLabelFont != null) employeeEditorLabel.setFont(employeeEditorLabelFont);
        employeeEditorLabel.setText("Редактирование информации о сотруднике");
        rootPanel.add(employeeEditorLabel, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        currentPositionLabel = new JLabel();
        currentPositionLabel.setText("Должность");
        rootPanel.add(currentPositionLabel, new com.intellij.uiDesigner.core.GridConstraints(7, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        currentPositionComboBox = new JComboBox();
        rootPanel.add(currentPositionComboBox, new com.intellij.uiDesigner.core.GridConstraints(7, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        scientificTitleComboBox = new JComboBox();
        rootPanel.add(scientificTitleComboBox, new com.intellij.uiDesigner.core.GridConstraints(9, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        scienceDegreeComboBox = new JComboBox();
        rootPanel.add(scienceDegreeComboBox, new com.intellij.uiDesigner.core.GridConstraints(10, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return rootPanel;
    }
}
