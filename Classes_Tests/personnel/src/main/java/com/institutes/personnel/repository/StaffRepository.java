package com.institutes.personnel.repository;

import com.institutes.personnel.model.Staff;

public interface StaffRepository {

    /** Есть только один объект Персонал **/
    public Staff getStaff();

}
