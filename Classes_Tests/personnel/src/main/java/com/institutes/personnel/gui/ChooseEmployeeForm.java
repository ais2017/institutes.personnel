package com.institutes.personnel.gui;

import com.institutes.personnel.gui.mytablemodel.ChooseEmployeeTableModel;
import com.institutes.personnel.model.Employee;
import com.institutes.personnel.model.User;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class ChooseEmployeeForm extends JFrame {
    private JTable employeeTable;
    private JPanel rootPanel;
    private JButton confirmButton;
    private JScrollPane scrollPane;
    private JLabel employeeTableLabel;

    private boolean confirmButtonClicked;
    private ChooseEmployeeTableModel chooseEmployeeTableModel;
    private User authorizedUser;

    public ChooseEmployeeForm(User user) {

        add(rootPanel);
        setTitle("Список сотрудников");
        setSize(700, 500);
        rootPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        confirmButtonClicked = false;
        authorizedUser = user;

        List<Employee> employeesList = new ArrayList<>(this.authorizedUser.getEmployee().getCurrentPosition().getParentDivision().getAllEmployeesOfDivision());
        chooseEmployeeTableModel = new ChooseEmployeeTableModel(employeesList);
        employeeTable.setModel(chooseEmployeeTableModel);
        employeeTable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        confirmButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                confirmButtonClicked = true;
            }
        });

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    public boolean isConfirmButtonClicked() {
        return confirmButtonClicked;
    }

    public void setConfirmButtonClicked(boolean confirmButtonClicked) {
        this.confirmButtonClicked = confirmButtonClicked;
    }

    public JTable getEmployeeTable() {
        return employeeTable;
    }

    public void setEmployeeTable(JTable employeeTable) {
        this.employeeTable = employeeTable;
    }

    public ChooseEmployeeTableModel getChooseEmployeeTableModel() {
        return chooseEmployeeTableModel;
    }

    public void setChooseEmployeeTableModel(ChooseEmployeeTableModel chooseEmployeeTableModel) {
        this.chooseEmployeeTableModel = chooseEmployeeTableModel;
    }

    public void updateEmployees(User user) {
        authorizedUser = user;
        List<Employee> employeesList = new ArrayList<>(this.authorizedUser.getEmployee().getCurrentPosition().getParentDivision().getAllEmployeesOfDivision());
     //   chooseEmployeeTableModel.setEmployees(employeesList);

        chooseEmployeeTableModel = new ChooseEmployeeTableModel(employeesList);
        employeeTable.setModel(chooseEmployeeTableModel);
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        rootPanel = new JPanel();
        rootPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(4, 1, new Insets(0, 0, 0, 0), -1, -1));
        scrollPane = new JScrollPane();
        rootPanel.add(scrollPane, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        employeeTable = new JTable();
        scrollPane.setViewportView(employeeTable);
        final com.intellij.uiDesigner.core.Spacer spacer1 = new com.intellij.uiDesigner.core.Spacer();
        rootPanel.add(spacer1, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        confirmButton = new JButton();
        confirmButton.setText("Подтвердить");
        rootPanel.add(confirmButton, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        employeeTableLabel = new JLabel();
        Font employeeTableLabelFont = this.$$$getFont$$$(null, Font.BOLD, 16, employeeTableLabel.getFont());
        if (employeeTableLabelFont != null) employeeTableLabel.setFont(employeeTableLabelFont);
        employeeTableLabel.setText("Список сотрудников");
        rootPanel.add(employeeTableLabel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return rootPanel;
    }
}
