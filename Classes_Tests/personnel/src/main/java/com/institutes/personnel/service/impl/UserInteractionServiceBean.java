package com.institutes.personnel.service.impl;

import com.institutes.personnel.flow.ExecutionFlowService;
import com.institutes.personnel.gui.*;
import com.institutes.personnel.gui.basic.AuthorizationForm;
import com.institutes.personnel.gui.basic.MainFormAdministrator;
import com.institutes.personnel.gui.basic.MainFormHREmployee;
import com.institutes.personnel.gui.basic.MainFormHRManager;
import com.institutes.personnel.model.*;
import com.institutes.personnel.service.BusinessProcessService;
import com.institutes.personnel.service.UserInteractionService;
import com.institutes.personnel.service.classes.Credentials;

import javax.swing.*;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;

public class UserInteractionServiceBean implements UserInteractionService {

    private BusinessProcessService businessProcessService;

    private ExecutionFlowService executionFlowService;

    private User authorizedUser;

    private Organization organization;

    /* Основные формы */
    private AuthorizationForm authorizationForm;
    private MainFormHRManager mainFormHRManager;
    private MainFormHREmployee mainFormHREmployee;
    private MainFormAdministrator mainFormAdministrator;

    /* Вспомогательные формы */
    private EmployeeEditorForm employeeEditorForm;
    private ChooseEmployeeForm chooseEmployeeForm;
    private ChooseDivisionForm chooseDivisionForm;
    private VacantPositionsForm vacantPositionsForm;
    private PositionEditorForm positionEditorForm;
    private ChoosePositionForm choosePositionForm;
    private SearchEmployeeForm searchEmployeeForm;
    private CreateUserForm createUserForm;
    private ChooseUserForm chooseUserForm;
    private UserEditorForm userEditorForm;

    public UserInteractionServiceBean() {

        this.businessProcessService = new BusinessProcessServiceBean(this, new DatabaseOperationServiceBean());
        this.authorizedUser = null;

        this.authorizationForm = null;
        this.mainFormHRManager = null;
        this.mainFormHREmployee = null;
        this.mainFormAdministrator = null;
        this.employeeEditorForm = null;
        this.chooseEmployeeForm = null;
        this.chooseDivisionForm = null;
        this.vacantPositionsForm = null;
        this.positionEditorForm = null;
        this.choosePositionForm = null;
        this.searchEmployeeForm = null;
        this.createUserForm = null;
        this.chooseUserForm = null;
        this.userEditorForm = null;
    }

    public UserInteractionServiceBean(BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
        this.executionFlowService = null;
        this.authorizedUser = null;

        this.authorizationForm = null;
        this.mainFormHRManager = null;
        this.mainFormHREmployee = null;
        this.mainFormAdministrator = null;
        this.employeeEditorForm = null;
        this.chooseEmployeeForm = null;
        this.chooseDivisionForm = null;
        this.vacantPositionsForm = null;
        this.positionEditorForm = null;
        this.choosePositionForm = null;
        this.searchEmployeeForm = null;
        this.createUserForm = null;
        this.chooseUserForm = null;
        this.userEditorForm = null;
    }

    public UserInteractionServiceBean(ExecutionFlowService executionFlowService) {
        this.businessProcessService = new BusinessProcessServiceBean(this, new DatabaseOperationServiceBean());
        this.executionFlowService = executionFlowService;
        this.authorizedUser = null;

        this.authorizationForm = null;
        this.mainFormHRManager = null;
        this.mainFormHREmployee = null;
        this.mainFormAdministrator = null;
        this.employeeEditorForm = null;
        this.chooseEmployeeForm = null;
        this.chooseDivisionForm = null;
        this.vacantPositionsForm = null;
        this.positionEditorForm = null;
        this.choosePositionForm = null;
        this.searchEmployeeForm = null;
        this.createUserForm = null;
        this.chooseUserForm = null;
        this.userEditorForm = null;
    }

    public BusinessProcessService getBusinessProcessService() {
        return businessProcessService;
    }

    public void setBusinessProcessService(BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

    public ExecutionFlowService getExecutionFlowService() {
        return executionFlowService;
    }

    public void setExecutionFlowService(ExecutionFlowService executionFlowService) {
        this.executionFlowService = executionFlowService;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public User getAuthorizedUser() {
        return authorizedUser;
    }

    public void setAuthorizedUser(User authorizedUser) {
        this.authorizedUser = authorizedUser;
    }

    /* Заглушка */
    @Override
    public Credentials getCredentials() {
     //   AtomicReference<Credentials> credentials = null;
        Credentials credentials = null;

        if(this.authorizationForm == null)
            authorizationForm = new AuthorizationForm();

        authorizationForm.setLocationRelativeTo(null);
        authorizationForm.setVisible(true);

        /* Ожидаем нажатия кнопки */
        while(!authorizationForm.isConfirmButtonClicked()) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {}
        }

        authorizationForm.setConfirmButtonClicked(false);
        credentials = new Credentials(authorizationForm.getLoginField().getText(), new String(authorizationForm.getPasswordField().getPassword()));
        authorizationForm.setVisible(false);


        return credentials;
    }

    @Override
    public Employee getEmployeeToChange() {
        Employee employeeToChange = null;

        try {
            if (this.chooseEmployeeForm == null)
                chooseEmployeeForm = new ChooseEmployeeForm(this.authorizedUser);
            else chooseEmployeeForm.updateEmployees(this.authorizedUser);
        }
        catch (NullPointerException e) {
            this.showError("Ошибка создания списка сотрудников. NullPointerException.");
            return null;
        }

        chooseEmployeeForm.setLocationRelativeTo(null);

        while (employeeToChange == null) {
            chooseEmployeeForm.setVisible(true);

            /* Ожидаем нажатия кнопки */
            while (!chooseEmployeeForm.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            chooseEmployeeForm.setConfirmButtonClicked(false);
         //   chooseEmployeeForm.setVisible(false);

            /* Если = -1 - строка не выбрана */
            if(chooseEmployeeForm.getEmployeeTable().getSelectedRowCount() != 1) {
                this.showMessage("Выберите одного сотрудника");
            }
            else {
                employeeToChange = chooseEmployeeForm.getChooseEmployeeTableModel().getEmployees().get(chooseEmployeeForm.getEmployeeTable().getSelectedRow());
            }
        }

        chooseEmployeeForm.setVisible(false);
        return employeeToChange;
     //   return null;
    }

    @Override
    public Employee getChangedEmployee(Employee employeeToChange) {
        Employee changedEmployee = null;

        String lastName, firstName, middleName, scientificTitle, scienceDegree, email;
        Date birthday, contractStartDate, contractExpirationDate;
        int passportSeries, passportNumber, personnelNumber;
        Position currentPosition;

        if(this.employeeEditorForm == null)
            employeeEditorForm = new EmployeeEditorForm(this.authorizedUser);
        else employeeEditorForm.clearFormFields();

        /* Заполняем поля формы данными сотрудника */
        employeeEditorForm.setFormFields(employeeToChange);
        employeeEditorForm.setLocationRelativeTo(null);

        while (changedEmployee == null) {
            employeeEditorForm.setVisible(true);

            /* Ожидаем нажатия кнопки */
            while (!employeeEditorForm.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            employeeEditorForm.setConfirmButtonClicked(false);
         //   employeeEditorForm.setVisible(false);

            if(this.checkEmployeeEditorFormFields(employeeEditorForm)) {
                try {
                    lastName = employeeEditorForm.getLastNameTextField().getText();
                    firstName = employeeEditorForm.getFirstNameTextField().getText();
                    middleName = employeeEditorForm.getMiddleNameTextField().getText();
                    birthday = employeeEditorForm.getBirthdayDateChooser().getDate();
                    passportSeries = Integer.parseInt(employeeEditorForm.getPassportSeriesTextField().getText());
                    passportNumber = Integer.parseInt(employeeEditorForm.getPassportNumberTextField().getText());
                    currentPosition = this.authorizedUser.getEmployee().getCurrentPosition().getParentDivision().getPositionByName((String) employeeEditorForm.getCurrentPositionComboBox().getSelectedItem());
                    personnelNumber = Integer.parseInt(employeeEditorForm.getPersonnelNumberTextField().getText());
                    scientificTitle = (String) employeeEditorForm.getScientificTitleComboBox().getSelectedItem();
                    scienceDegree = (String) employeeEditorForm.getScienceDegreeComboBox().getSelectedItem();
                    email = employeeEditorForm.getEmailTextField().getText();
                    contractStartDate = employeeEditorForm.getContractStartDateChooser().getDate();
                    contractExpirationDate = employeeEditorForm.getContractExpirationDateChooser().getDate();

                    changedEmployee = new Employee(lastName, firstName, middleName, birthday,
                            passportSeries, passportNumber, currentPosition, personnelNumber,
                            scientificTitle, scienceDegree, email,
                            contractStartDate, contractExpirationDate);
                } catch (RuntimeException e) {
                    this.showError("Ошибка. Проверьте корректность данных");
                    changedEmployee = null;
                }
            }
        }

        /* Восстанавливаем id сотрудника, чтобы вместо insert произошел update по DB */
        changedEmployee.setId(employeeToChange.getId());
        employeeEditorForm.setVisible(false);
        return changedEmployee;

     //   return null;
    }

    @Override
    public Employee getEmployeeToCreate() {
        Employee createdEmployee = null;

        String lastName, firstName, middleName, scientificTitle, scienceDegree, email;
        Date birthday, contractStartDate, contractExpirationDate;
        int passportSeries, passportNumber, personnelNumber;
        Position currentPosition;

        if(this.employeeEditorForm == null)
            employeeEditorForm = new EmployeeEditorForm(this.authorizedUser);
        else employeeEditorForm.clearFormFields();

        employeeEditorForm.setLocationRelativeTo(null);

        while (createdEmployee == null) {
            employeeEditorForm.setVisible(true);

            /* Ожидаем нажатия кнопки */
            while (!employeeEditorForm.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            employeeEditorForm.setConfirmButtonClicked(false);
         //   employeeEditorForm.setVisible(false);

            if(this.checkEmployeeEditorFormFields(employeeEditorForm)) {
                try {
                    lastName = employeeEditorForm.getLastNameTextField().getText();
                    firstName = employeeEditorForm.getFirstNameTextField().getText();
                    middleName = employeeEditorForm.getMiddleNameTextField().getText();
                    birthday = employeeEditorForm.getBirthdayDateChooser().getDate();
                    passportSeries = Integer.parseInt(employeeEditorForm.getPassportSeriesTextField().getText());
                    passportNumber = Integer.parseInt(employeeEditorForm.getPassportNumberTextField().getText());
                    currentPosition = this.authorizedUser.getEmployee().getCurrentPosition().getParentDivision().getPositionByName((String) employeeEditorForm.getCurrentPositionComboBox().getSelectedItem());
                    personnelNumber = Integer.parseInt(employeeEditorForm.getPersonnelNumberTextField().getText());
                    scientificTitle = (String) employeeEditorForm.getScientificTitleComboBox().getSelectedItem();
                    scienceDegree = (String) employeeEditorForm.getScienceDegreeComboBox().getSelectedItem();
                    email = employeeEditorForm.getEmailTextField().getText();
                    contractStartDate = employeeEditorForm.getContractStartDateChooser().getDate();
                    contractExpirationDate = employeeEditorForm.getContractExpirationDateChooser().getDate();

                    createdEmployee = new Employee(lastName, firstName, middleName, birthday,
                            passportSeries, passportNumber, currentPosition, personnelNumber,
                            scientificTitle, scienceDegree, email,
                            contractStartDate, contractExpirationDate);
                } catch (RuntimeException e) {
                    this.showError("Невозможно создать сотрудника. Проверьте корректность данных");
                    createdEmployee = null;
                }
            }
        }

        employeeEditorForm.setVisible(false);
        return createdEmployee;
     //   return null;
    }

    @Override
    public Collection<Employee> getEmployeesToCreate() {
        return null;
    }

    @Override
    public Division getTargetDivisionForStaffListPosition() {
        Division division = null;

        try {
            if (this.chooseDivisionForm == null)
                chooseDivisionForm = new ChooseDivisionForm(this.organization);
            else chooseDivisionForm.updateDivisions(this.organization);
        }
        catch (NullPointerException e) {
            this.showError("Ошибка создания списка подразделений. NullPointerException.");
            return null;
        }

        chooseDivisionForm.setLocationRelativeTo(null);

        while (division == null) {
            chooseDivisionForm.setVisible(true);

            /* Ожидаем нажатия кнопки */
            while (!chooseDivisionForm.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            chooseDivisionForm.setConfirmButtonClicked(false);
            //   chooseDivisionForm.setVisible(false);

            /* Если = -1 - строка не выбрана */
            if(chooseDivisionForm.getDivisionTable().getSelectedRowCount() != 1) {
                this.showMessage("Выберите одно подразделение");
            }
            else {
                division = chooseDivisionForm.getChooseDivisionTableModel().getDivisions().get(chooseDivisionForm.getDivisionTable().getSelectedRow());
            }
        }

        chooseDivisionForm.setVisible(false);
        return division;
     //   return null;
    }

    @Override
    public Position getNewPositionForStaffListDivision(Division division) {
        Position createdPosition = null;

        String positionName;
        int maxEmployees;
        Collection<Employee> employees = new LinkedHashSet<>();

        if(this.positionEditorForm == null)
            positionEditorForm = new PositionEditorForm();
        else positionEditorForm.clearFormFields();

        positionEditorForm.setLocationRelativeTo(null);

        while (createdPosition == null) {
            positionEditorForm.setVisible(true);

            /* Ожидаем нажатия кнопки */
            while (!positionEditorForm.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            positionEditorForm.setConfirmButtonClicked(false);
            //   positionEditorForm.setVisible(false);

            try {
                positionName = positionEditorForm.getPositionNameTextField().getText();
                maxEmployees = Integer.parseInt(positionEditorForm.getMaxEmployeesTextField().getText());

                createdPosition = new Position(positionName, maxEmployees, maxEmployees, division, employees);
            } catch (RuntimeException e) {
                this.showError("Невозможно создать должность. Проверьте корректность данных");
                createdPosition = null;
            }
        }

        positionEditorForm.setVisible(false);

        return createdPosition;
     //   return null;
    }

    @Override
    public Position getPositionInStaffListToChange(Division division) {
        Position positionToChange = null;

        try {
            if (this.choosePositionForm == null)
                choosePositionForm = new ChoosePositionForm(division.getPositions());
            else choosePositionForm.updatePositions(division.getPositions());
        }
        catch (NullPointerException e) {
            this.showError("Ошибка создания списка должностей. NullPointerException.");
            return null;
        }

        choosePositionForm.setLocationRelativeTo(null);

        while (positionToChange == null) {
            choosePositionForm.setVisible(true);

            /* Ожидаем нажатия кнопки */
            while (!choosePositionForm.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            choosePositionForm.setConfirmButtonClicked(false);
            //   choosePositionForm.setVisible(false);

            /* Если = -1 - строка не выбрана */
            if(choosePositionForm.getPositionTable().getSelectedRowCount() != 1) {
                this.showMessage("Выберите одноу должность");
            }
            else {
                positionToChange = choosePositionForm.getChoosePositionTableModel().getPositions().get(choosePositionForm.getPositionTable().getSelectedRow());
            }
        }

        choosePositionForm.setVisible(false);
        return positionToChange;
     //   return null;
    }

    @Override
    public Position getPositionInStaffListAfterChange(Position positionToChange) {
        Position changedPosition = null;

        String positionName;
        int newMaxEmployees, newVacantPositions, busyPositions;
        busyPositions = positionToChange.getMaxEmployees() - positionToChange.getVacantPositions();

        if(this.positionEditorForm == null)
            positionEditorForm = new PositionEditorForm();
        else positionEditorForm.clearFormFields();

        /* Заполняем поля формы данными сотрудника */
        positionEditorForm.setFormFields(positionToChange);
        positionEditorForm.setLocationRelativeTo(null);

        while (changedPosition == null) {
            positionEditorForm.setVisible(true);

            /* Ожидаем нажатия кнопки */
            while (!positionEditorForm.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            positionEditorForm.setConfirmButtonClicked(false);
            //   positionEditorForm.setVisible(false);

            try {
                positionName = positionEditorForm.getPositionNameTextField().getText();
                newMaxEmployees = Integer.parseInt(positionEditorForm.getMaxEmployeesTextField().getText());

                /* Необходимо предусмотреть ошибки при обновлении maxEmployees, vacantPositions */
                if(newMaxEmployees > positionToChange.getMaxEmployees())
                    newVacantPositions = positionToChange.getVacantPositions() + (newMaxEmployees - positionToChange.getMaxEmployees());
                else if(newMaxEmployees < positionToChange.getMaxEmployees()) {
                        newVacantPositions = newMaxEmployees - busyPositions;
                }
                else newVacantPositions = positionToChange.getVacantPositions();

                changedPosition = new Position(positionName, newMaxEmployees, newVacantPositions, positionToChange.getParentDivision(), positionToChange.getEmployees());
            } catch (RuntimeException e) {
                this.showError("Невозможно создать должность. Проверьте корректность данных");
                changedPosition = null;
            }
        }

        /* Восстанавливаем id должности, чтобы вместо insert произошел update по DB */
        changedPosition.setId(positionToChange.getId());
        positionEditorForm.setVisible(false);
        return changedPosition;
     //   return null;
    }

    @Override
    public Division getDivisionToSearchVacantPositions() {
        Division division = null;

        try {
            if (this.chooseDivisionForm == null)
                chooseDivisionForm = new ChooseDivisionForm(this.organization);
            else chooseDivisionForm.updateDivisions(this.organization);
        }
        catch (NullPointerException e) {
            this.showError("Ошибка создания списка подразделений. NullPointerException.");
            return null;
        }

        chooseDivisionForm.setLocationRelativeTo(null);

        while (division == null) {
            chooseDivisionForm.setVisible(true);

            /* Ожидаем нажатия кнопки */
            while (!chooseDivisionForm.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            chooseDivisionForm.setConfirmButtonClicked(false);
            //   chooseDivisionForm.setVisible(false);

            /* Если = -1 - строка не выбрана */
            if(chooseDivisionForm.getDivisionTable().getSelectedRowCount() != 1) {
                this.showMessage("Выберите одно подразделение");
            }
            else {
                division = chooseDivisionForm.getChooseDivisionTableModel().getDivisions().get(chooseDivisionForm.getDivisionTable().getSelectedRow());
            }
        }

        chooseDivisionForm.setVisible(false);
        return division;
     //   return null;
    }

    @Override
    public void showVacantPositions(Collection<Position> vacantPositions) {

        if (this.vacantPositionsForm == null)
                vacantPositionsForm = new VacantPositionsForm(vacantPositions);
        else vacantPositionsForm.updateVacantPositions(vacantPositions);

        vacantPositionsForm.setLocationRelativeTo(null);

        vacantPositionsForm.setVisible(true);

        /* Ожидаем нажатия кнопки */
        while (!vacantPositionsForm.isReturnButtonClicked()) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
            }
        }

        vacantPositionsForm.setReturnButtonClicked(false);
        vacantPositionsForm.setVisible(false);
        return;
    }

    @Override
    public Employee getTargetEmployeeForCreatingUser() {
        Employee employee = null;
        int personnelNumber;

        /* Объект персонал, по которому производим поиск */
        Staff staff = new Staff(this.organization.getAllEmployeesOfOrganization());

        if (this.searchEmployeeForm == null)
            searchEmployeeForm = new SearchEmployeeForm();
        else searchEmployeeForm.clearFields();

        searchEmployeeForm.setLocationRelativeTo(null);
        searchEmployeeForm.setVisible(true);

        /* Поиск до тех пор, пока не найдем сотрудника */
        while(employee == null) {

            /* Ожидаем нажатия кнопки */
            while (!searchEmployeeForm.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            searchEmployeeForm.setConfirmButtonClicked(false);

            if(searchEmployeeForm.getPersonnelNumberTextField().getText().equals("")) {
                this.showError("Введите табельный номер сотрудника");
                continue;
            }
            else {
                try {
                    personnelNumber = Integer.parseInt(searchEmployeeForm.getPersonnelNumberTextField().getText());
                }
                catch (Exception e) {
                    this.showError("Введен некорректный номер. Проверьте правильность введенных данных");
                    continue;
                }

                /* Если введены корректные данные, осуществляем поиск */
                employee = staff.searchEmployeeByPersonnelNumber(personnelNumber);

                if(employee == null) {
                    this.showError("Сотрудник с табельным номером " + personnelNumber + " не найден");
                    continue;
                }
            }
        }

        searchEmployeeForm.setVisible(false);

        return employee;
     //   return null;
    }

    @Override
    public User getCreatedUser(Employee employee) {
        User createdUser = null;

        String login, password, confirmPassword, role;

        if(this.createUserForm == null)
            createUserForm = new CreateUserForm();
        else createUserForm.clearFormFields();

        createUserForm.setLocationRelativeTo(null);

        while (createdUser == null) {
            createUserForm.setVisible(true);

            /* Ожидаем нажатия кнопки */
            while (!createUserForm.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            createUserForm.setConfirmButtonClicked(false);
            //   createUserForm.setVisible(false);

            try {

                login = createUserForm.getLoginTextField().getText();
                password = new String(createUserForm.getPasswordField().getPassword());
                confirmPassword = new String(createUserForm.getConfirmPasswordField().getPassword());
                role = (String) createUserForm.getRoleComboBox().getSelectedItem();

                /* Проверка корректности введенных данных */
                if(login.equals("") || password.equals("") || confirmPassword.equals("") || role.equals("")) {
                    this.showError("Ошибка. Заполните все поля");
                    continue;
                }
                else if(!password.equals(confirmPassword)) {
                    this.showError("Введенные пароли не совпадают");
                    continue;
                }
                else {
                    createdUser = new User(employee, login, password, role);
                }
            } catch (RuntimeException e) {
                this.showError("Ошибка. Проверьте корректность данных");
                createdUser = null;
            }
        }

        createUserForm.setVisible(false);

        return createdUser;
     //   return null;
    }

    @Override
    public User getUserToChange(Collection<User> users) {
        User userToChange = null;

        try {
            if (this.chooseUserForm == null)
                chooseUserForm = new ChooseUserForm(users);
            else chooseUserForm.updateUsers(users);
        }
        catch (NullPointerException e) {
            this.showError("Ошибка создания списка сотрудников. NullPointerException.");
            return null;
        }

        chooseUserForm.setLocationRelativeTo(null);

        while (userToChange == null) {
            chooseUserForm.setVisible(true);

            /* Ожидаем нажатия кнопки */
            while (!chooseUserForm.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            chooseUserForm.setConfirmButtonClicked(false);
            //   chooseUserForm.setVisible(false);

            /* Если = -1 - строка не выбрана */
            if(chooseUserForm.getUserTable().getSelectedRowCount() != 1) {
                this.showMessage("Выберите одного пользователя");
            }
            else {
                userToChange = chooseUserForm.getChooseUserTableModel().getUsers().get(chooseUserForm.getUserTable().getSelectedRow());
            }
        }

        chooseUserForm.setVisible(false);
        return userToChange;
     //   return null;
    }

    @Override
    public User getChangedUser(User userToChange) {
        User changedUser = null;

        String login, oldPassword, newPassword, confirmNewPassword, role;

        if(this.userEditorForm == null)
            userEditorForm = new UserEditorForm();
        else userEditorForm.clearFormFields();

        /* Заполняем поля формы данными сотрудника */
        userEditorForm.setFormFields(userToChange);
        userEditorForm.setLocationRelativeTo(null);

        while (changedUser == null) {
            userEditorForm.setVisible(true);

            /* Ожидаем нажатия кнопки */
            while (!userEditorForm.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            userEditorForm.setConfirmButtonClicked(false);
            //   userEditorForm.setVisible(false);

            try {
                login = userEditorForm.getLoginTextField().getText();
                oldPassword = new String(userEditorForm.getOldPasswordField().getPassword());
                newPassword = new String(userEditorForm.getNewPasswordField().getPassword());
                confirmNewPassword = new String(userEditorForm.getConfirmNewPasswordField().getPassword());
                role = (String) userEditorForm.getRoleComboBox().getSelectedItem();

                /* Проверка корректности введенных данных */
                if(login.equals("") || oldPassword.equals("") || newPassword.equals("") || confirmNewPassword.equals("") || role.equals("")) {
                    this.showError("Ошибка. Заполните все поля");
                    continue;
                }
                else if(!oldPassword.equals(userToChange.getPassword())) {
                    this.showError("Ошибка. Старый пароль пользователя не совпадает");
                    continue;
                }
                else if(!newPassword.equals(confirmNewPassword)) {
                    this.showError("Введенные пароли не совпадают");
                    continue;
                }
                else {
                    changedUser = new User(userToChange.getId(), userToChange.getPersonId(), login, newPassword, role);
                }
            } catch (RuntimeException e) {
                this.showError("Ошибка. Проверьте корректность данных");
                changedUser = null;
            }
        }

        userEditorForm.setVisible(false);
        return changedUser;

        //   return null;
    }

    @Override
    public void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
     //   System.out.println(message);
    }

    @Override
    public void showError(String message) {
        JOptionPane.showMessageDialog(null, message, "Ошибка", JOptionPane.ERROR_MESSAGE);
    }

    @Override
    public void showMainHRManagerForm() {
     //   boolean actionSelected = false;
        Collection<Position> vacantPositions;

        if(this.mainFormHRManager == null) {
            this.mainFormHRManager = new MainFormHRManager();
        }

        mainFormHRManager.setLocationRelativeTo(null);

        /* Пока не выбрано действие */
        while(true) {
            /* Загружаем актуальную информацию из АИС (обновляем organization, authorizedUser) */
            if(this.executionFlowService != null) {
                this.executionFlowService.loadOrganization();
            }
            else {
                this.showError("Ошибка при обновлении информации АИС");
                System.exit(1);
            }

            mainFormHRManager.setVisible(true);

            /* Ожидаем нажатия кнопки */
            while (!mainFormHRManager.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            /* Сбрасываем признак нажатия кнопки */
            mainFormHRManager.setConfirmButtonClicked(false);

            /* Прячем экран */
            mainFormHRManager.setVisible(false);

            /* Проверяем, какое действие выбрал пользователь */
            if (mainFormHRManager.getAddNewDataToStaffListRadioButton().isSelected()) {
                this.businessProcessService.addDataToStaffList(this.authorizedUser);
            }
            else if (mainFormHRManager.getEditDataInStaffListRadioButton().isSelected()) {
                this.businessProcessService.changeDataInStaffList(this.authorizedUser);
            }
            else if (mainFormHRManager.getGetVacantPositionListByDivisionRadioButton().isSelected()) {
                vacantPositions = this.businessProcessService.getVacantPositionListByDivision(this.authorizedUser);
                if(vacantPositions != null) {
                    if(vacantPositions.size() > 0) {
                        /* Показать список свободных должностей */
                        this.showVacantPositions(vacantPositions);
                    }
                    else this.showMessage("В подразделении нет свободных должностей");
                }
                else this.showError("Произошла ошибка при получении списка свободных должностей");
            }
            else if (mainFormHRManager.getGetVacantPositionListByOrganizationRadioButton().isSelected()) {
                vacantPositions = this.businessProcessService.getVacantPositionListByOrganization(this.authorizedUser);
                if(vacantPositions != null) {
                    if(vacantPositions.size() > 0) {
                        /* Показать список свободных должностей */
                        this.showVacantPositions(vacantPositions);
                    }
                    else this.showMessage("В организации нет свободных должностей");
                }
                else this.showError("Произошла ошибка при получении списка свободных должностей");
            }
            else this.showMessage("Не выбрано действие");
        }

     //   mainFormHRManager.setVisible(false);
    }

    @Override
    public void showMainHREmployeeForm() {
        Collection<Position> vacantPositions;

        if(this.mainFormHREmployee == null) {
            this.mainFormHREmployee = new MainFormHREmployee();
        }

        mainFormHREmployee.setLocationRelativeTo(null);

        /* Пока не выбрано действие */
        while(true) {
            /* Загружаем актуальную информацию из АИС (обновляем organization, authorizedUser) */
            if(this.executionFlowService != null) {
                this.executionFlowService.loadOrganization();
            }
            else {
                this.showError("Ошибка при обновлении информации АИС");
                System.exit(1);
            }

            mainFormHREmployee.setVisible(true);

            /* Ожидаем нажатия кнопки */
            while (!mainFormHREmployee.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            /* Сбрасываем признак нажатия кнопки */
            mainFormHREmployee.setConfirmButtonClicked(false);

            /* Прячем экран */
            mainFormHREmployee.setVisible(false);

            /* Проверяем, какое действие выбрал пользователь */
            if (mainFormHREmployee.getHireEmployeeRadioButton().isSelected()) {
                if(this.businessProcessService.hireEmployee(this.authorizedUser) != null);
             //   this.showMessage("Сотрудник успешно создан");
            }
            else if (mainFormHREmployee.getChangeEmployeeDataRadioButton().isSelected()) {
                this.businessProcessService.changeEmployeeData(this.authorizedUser);
            }
            else if (mainFormHREmployee.getGetVacantPositionListByDivisionRadioButton().isSelected()) {
                vacantPositions = this.businessProcessService.getVacantPositionListByDivision(this.authorizedUser);
                if(vacantPositions != null) {
                    if(vacantPositions.size() > 0) {
                        /* Показать список свободных должностей */
                        this.showVacantPositions(vacantPositions);
                    }
                    else this.showMessage("В подразделении нет свободных должностей");
                }
                else this.showError("Произошла ошибка при получении списка свободных должностей");
            }
            else if (mainFormHREmployee.getGetVacantPositionListByOrganizationRadioButton().isSelected()) {
                vacantPositions = this.businessProcessService.getVacantPositionListByOrganization(this.authorizedUser);
                if(vacantPositions != null) {
                    if(vacantPositions.size() > 0) {
                        /* Показать список свободных должностей */
                        this.showVacantPositions(vacantPositions);
                    }
                    else this.showMessage("В организации нет свободных должностей");
                }
                else this.showError("Произошла ошибка при получении списка свободных должностей");
            }
            else this.showMessage("Не выбрано действие");
        }
    }

    @Override
    public void showMainAdministratorForm() {
        if(this.mainFormAdministrator == null) {
            this.mainFormAdministrator = new MainFormAdministrator();
        }

        mainFormAdministrator.setLocationRelativeTo(null);

        /* Пока не выбрано действие */
        while(true) {
            /* Загружаем актуальную информацию из АИС (обновляем organization, authorizedUser) */
            if(this.executionFlowService != null) {
                this.executionFlowService.loadOrganization();
            }
            else {
                this.showError("Ошибка при обновлении информации АИС");
                System.exit(1);
            }

            mainFormAdministrator.setVisible(true);

            /* Ожидаем нажатия кнопки */
            while (!mainFormAdministrator.isConfirmButtonClicked()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }

            /* Сбрасываем признак нажатия кнопки */
            mainFormAdministrator.setConfirmButtonClicked(false);

            /* Прячем экран */
            mainFormAdministrator.setVisible(false);

            /* Проверяем, какое действие выбрал пользователь */
            if (mainFormAdministrator.getRegisterUserRadioButton().isSelected()) {
                this.businessProcessService.registerUser(this.authorizedUser);
            }
            else if (mainFormAdministrator.getSetAccessRightsRadioButton().isSelected()) {
                this.businessProcessService.setAccessRights(this.authorizedUser);
            }
            else this.showMessage("Не выбрано действие");
        }
    }

    /** Проверка введенных пользователем данных сотрудник **/
    public boolean checkEmployeeEditorFormFields(EmployeeEditorForm formToCheck) {
        boolean correct = true;

        String lastName, firstName, middleName, scientificTitle, scienceDegree, email;
        Date birthday, contractStartDate, contractExpirationDate;
        int passportSeries, passportNumber, personnelNumber;
        Position currentPosition;

        lastName = formToCheck.getLastNameTextField().getText();
        firstName = formToCheck.getFirstNameTextField().getText();
        middleName = formToCheck.getMiddleNameTextField().getText();
        birthday = formToCheck.getBirthdayDateChooser().getDate();
        currentPosition = this.authorizedUser.getEmployee().getCurrentPosition().getParentDivision().getPositionByName((String) formToCheck.getCurrentPositionComboBox().getSelectedItem());
        scientificTitle = (String) formToCheck.getScientificTitleComboBox().getSelectedItem();
        scienceDegree = (String) formToCheck.getScienceDegreeComboBox().getSelectedItem();
        email = formToCheck.getEmailTextField().getText();
        contractStartDate = formToCheck.getContractStartDateChooser().getDate();
        contractExpirationDate = formToCheck.getContractExpirationDateChooser().getDate();

        if(lastName.equals("") || firstName.equals("") || middleName.equals("") || birthday == null || scientificTitle.equals("") || scienceDegree.equals("") || email.equals("") || contractStartDate == null || contractExpirationDate == null) {
            correct = false;
            this.showError("Заполните все поля");
            return correct;
        }
        /* Проверки корректности введенных дат */
        else if(contractStartDate != null && contractExpirationDate != null) {
            if(contractStartDate.compareTo(contractExpirationDate) > 0) {
                correct = false;
                this.showError("Ошибка. Дата начала действия контракта не может быть больше даты окончания действия " +
                        "контракта");
                return correct;
            }
        }

        try {
            passportSeries = Integer.parseInt(formToCheck.getPassportSeriesTextField().getText());
            passportNumber = Integer.parseInt(formToCheck.getPassportNumberTextField().getText());
            personnelNumber = Integer.parseInt(formToCheck.getPersonnelNumberTextField().getText());
        } catch (Exception e) {
            correct = false;
            this.showError("Ошибка. Проверьте правильность введенных серии паспорта, номера паспорта " +
                    "и табельного номера");
            return correct;
        }


        return correct;
    }
}
