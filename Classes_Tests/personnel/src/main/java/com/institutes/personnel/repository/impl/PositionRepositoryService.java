package com.institutes.personnel.repository.impl;

import com.institutes.personnel.model.Division;
import com.institutes.personnel.model.Employee;
import com.institutes.personnel.model.Position;
import com.institutes.personnel.repository.DivisionRepository;
import com.institutes.personnel.repository.EmployeeRepository;
import com.institutes.personnel.repository.PositionRepository;
import com.institutes.personnel.service.DatabaseConnectionService;
import com.institutes.personnel.service.impl.DatabaseConnectionServiceBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;

public class PositionRepositoryService implements PositionRepository {

/*    private DatabaseConnectionService databaseConnectionService;

    private EmployeeRepository employeeRepository;

    private DivisionRepository divisionRepository;

    public PositionRepositoryService() {
        this.databaseConnectionService = new DatabaseConnectionServiceBean();
        this.employeeRepository = new EmployeeRepositoryService();
        this.divisionRepository = new DivisionRepositoryService();
    }*/

    @Override
    public Position savePosition(Position position) {
        return position;
    }

    @Override
    public void deletePosition(Position position) {
        return;
    }

    @Override
    public Collection<Position> findAll() {
        return null;
    }

    @Override
    public Position findPositionById(int id) {
        return null;
    }

    @Override
    public Collection<Position> findPositionsByDivisionIdForCreatingDivision(int divisionId) {
        return null;
    }

    //    @Override
//    public Position findPositionById(int id) {
//        Position position = null;
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        String query = "SELECT p.*" +
//                       "FROM position_t p" +
//                       "WHERE(p.id = ?)";
//
//        /** Получаем соединение **/
//        try {
//            connection = databaseConnectionService.getConnection();
//            if(connection == null)
//                throw new RuntimeException("Not connected to database");
//
//            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
//             *  необходим курсор, с помощью которого можно перемещаться по записям **/
//            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//            statement.setInt(1, id);
//
//            resultSet = statement.executeQuery();
//
//            /** Получаем количество строк, которое было получено после выполнения запроса **/
//            resultSet.last();
//            int numRows = resultSet.getRow();
//
//            /** Если запрос вернул несколько строк, выбрасываем исключение **/
//            if(numRows > 1)
//                throw new RuntimeException("Several Positions with id " + id + "found");
//            else if(numRows == 0)
//                return null;
//
//            resultSet.beforeFirst();
//            while(resultSet.next()) {
//                String positionName = resultSet.getString("POSITIONNAME");
//                int maxEmployees = resultSet.getInt("MAXEMPLOYEES");
//                int vacantPositions = resultSet.getInt("VACANTPOSITIONS");
//                int parentDivisionId = resultSet.getInt("DIVISIONID");
//
//                /** Получаем коллекцию всех Сотрудников, которые занимают эту Должность **/
//                Collection<Employee> employees = this.employeeRepository.findEmployeesByPositionIdForCreatingPosition(id);
//
//                /** Получаем объект родительского Подразделения **/
//                Division parentDivision = this.divisionRepository.findDivisionById(parentDivisionId);
//
//                /** Создаем объект Должности. В конструкторе Position всем employees будет присвоено currentPosition **/
//                position = new Position(positionName, maxEmployees, vacantPositions, parentDivision, employees);
//            }
//
//            //Закрываем
//            resultSet.close();
//            statement.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return position;
//    }
//
//    @Override
//    public Collection<Position> findPositionsByDivisionIdForCreatingDivision(int divisionId) {
//        Collection<Position> positions = null;
//        Connection connection = null;
//        PreparedStatement statement = null;
//        ResultSet resultSet = null;
//        String query = "SELECT pos.*" +
//                "FROM division_t div" +
//                "JOIN position_t pos ON (div.ID = pos.DIVISIONID)" +
//                "WHERE(div.ID = ?)";
//
//        try {
//            /** Получаем соединение **/
//            connection = databaseConnectionService.getConnection();
//            if(connection == null)
//                throw new RuntimeException("Not connected to database");
//
//            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
//             *  необходим курсор, с помощью которого можно перемещаться по записям **/
//            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//            statement.setInt(1, divisionId);
//
//            resultSet = statement.executeQuery();
//
//            /** Получаем количество строк, которое было получено после выполнения запроса **/
//            resultSet.last();
//            int numRows = resultSet.getRow();
//
//            /** Если не было найдено Должностей, относящихся к этому Подразделению, возвращаем коллекцию размера 0 **/
//            if(numRows < 1) {
//                positions = new LinkedHashSet<>();
//                return positions;
//            }
//            /** Если были найдены Сотрудники, создаем объекты для них и добавляем в коллекцию **/
//            else {
//                positions = new LinkedHashSet<>();
//                resultSet.beforeFirst();
//                while(resultSet.next()) {
//                    String positionName = resultSet.getString("POSITIONNAME");
//                    int maxEmployees = resultSet.getInt("MAXEMPLOYEES");
//                    int vacantPositions = resultSet.getInt("VACANTPOSITIONS");
//                    int positionId = resultSet.getInt("ID");
//
//                    /** Получаем коллекцию всех Сотрудников, которые занимают эту Должность **/
//                    Collection<Employee> employees = this.employeeRepository.findEmployeesByPositionIdForCreatingPosition(positionId);
//                    /* Если был возвращен null - произошла ошибка */
//                    if(employees == null)
//                        return null;
//
//                    /** Создаем объект Должности **/
//                    Position position = new Position(positionName, maxEmployees, vacantPositions, employees);
//
//                    /** Добавляем объект Должности в коллекцию **/
//                    positions.add(position);
//                }
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return positions;
//    }
}
