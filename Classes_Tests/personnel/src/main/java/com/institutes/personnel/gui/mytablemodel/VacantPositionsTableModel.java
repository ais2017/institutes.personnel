package com.institutes.personnel.gui.mytablemodel;

import com.institutes.personnel.model.Position;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

public class VacantPositionsTableModel implements TableModel {
    private Collection<TableModelListener> listeners = new LinkedHashSet<>();

    private List<Position> vacantPositions;

    public VacantPositionsTableModel(List<Position> vacantPositions) {
        this.vacantPositions = vacantPositions;
    }

    public List<Position> getVacantPositions() {
        return vacantPositions;
    }

    public void setVacantPositions(List<Position> vacantPositions) {
        this.vacantPositions = vacantPositions;
    }

    @Override
    public int getRowCount() {
        return vacantPositions.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Код подразделения";
            case 1:
                return "Название подразделения";
            case 2:
                return "Название должности";
            case 3:
                return "Количество вакансий";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Position vacantPosition = vacantPositions.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return vacantPosition.getParentDivision().getDivisionCode();
            case 1:
                return vacantPosition.getParentDivision().getDivisionName();
            case 2:
                return vacantPosition.getPositionName();
            case 3:
                return vacantPosition.getVacantPositions();
        }
        return "";
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }
}
