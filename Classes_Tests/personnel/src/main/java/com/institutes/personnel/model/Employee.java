package com.institutes.personnel.model;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.util.Date;

public class Employee extends Person {

    //Зависимость от ScientificTitle
    private final ScientificTitle scientificTitleInstance = new ScientificTitle();

    //Зависимость от ScienceDegree
    private final ScienceDegree scienceDegreeInstance = new ScienceDegree();

    /** Primary key в таблице person_t **/
    private int id;

    /** Foreign key к таблице position_t **/
    private int positionId;

    /** Нельзя создать с null, но можно изменить на null - тогда Сотрудник уволен **/
    @Nullable
    private Position currentPosition;

    @NotNull
    private int personnelNumber;

    @Nullable
    private String scientificTitle;

    @Nullable
    private String scienceDegree;

    @Nullable
    private String email;

    //Date в java - по сути, timestamp как в unix, количество миллисекунд с January 1, 1970 00:00:00.000 GMT (Gregorian).
    @NotNull
    private Date contractStartDate;

    //Date - deprecated. Переделать.
    /* Дата окончания может быть null - в этом случае контракт не ограничен сроком */
    @Nullable
    private Date contractExpirationDate;

    /** Конструктор для репозитория. Не использовать в интерфейсе! Отсутствует проверка на currentPosition **/
    public Employee(int id, int positionId, String lastName, String firstName, String middleName, Date birthday, int passportSeries, int passportNumber, int personnelNumber, String scientificTitle, String scienceDegree, String email, Date contractStartDate, Date contractExpirationDate) {
        super(lastName, firstName, middleName, birthday, passportSeries, passportNumber);

        /* Новый объект */
        this.id = id;
        this.positionId = positionId;

        /** Будет заполнено в соответствующем методе репозитория при инициализации Position через БД **/
        this.currentPosition = null;

        /* Табельный номер должен быть корректен */
        if(personnelNumber < 1)
            throw new RuntimeException("personnelNumber is incorrect");
        else this.personnelNumber = personnelNumber;

        /* Проверки корректности введенных дат */
        if(contractStartDate != null) {
            if(contractExpirationDate != null && contractStartDate.compareTo(contractExpirationDate) > 0)
                throw new RuntimeException("Contract Start Date is after Contract Expiration Date");
            else this.contractStartDate = contractStartDate;
        }
        else throw new RuntimeException("Contract Start Date is null");

        if(contractExpirationDate != null) {
            if(contractStartDate != null && contractStartDate.compareTo(contractExpirationDate) > 0)
                throw new RuntimeException("Contract Start Date is after Contract Expiration Date");
            else this.contractExpirationDate = contractExpirationDate;
        }
        else this.contractExpirationDate = contractExpirationDate;

        /* Проверки корректности значений scientificTitle, scienceDegree */
        if(scientificTitleInstance.checkScientificTitleExists(scientificTitle))
            this.scientificTitle = scientificTitle;
        else throw new RuntimeException("Incorrect Scientific Title");

        if(scienceDegreeInstance.checkScienceDegreeExists(scienceDegree))
            this.scienceDegree = scienceDegree;
        else throw new RuntimeException("Incorrect Science Degree");

        this.email = email;
    }


    /** Основной конструктор **/
    public Employee(String lastName, String firstName, String middleName, Date birthday, int passportSeries, int passportNumber, Position currentPosition, int personnelNumber, String scientificTitle, String scienceDegree, String email, Date contractStartDate, Date contractExpirationDate) {
        super(lastName, firstName, middleName, birthday, passportSeries, passportNumber);

        /* Нельзя создать Сотрудника без Должности */
        if(currentPosition == null)
            throw new RuntimeException("currentPosition is null");
        else {
            this.currentPosition = currentPosition;

            /* Новый объект */
            this.id = -1;
            this.positionId = currentPosition.getId();


            /**
             *
             * upd. Убрал this.currentPosition.addEmployee(this), так как будут создаваться дубли. В методе
             *      Position.addEmployee() есть связывание, нужно пользоватсья этим методом при добавлении Сотрудников.
             *
              */

            /* Добавляем сотрудника в Position.employees родительской должности */
            //this.currentPosition.addEmployee(this);

        }

        /* Табельный номер должен быть корректен */
        if(personnelNumber < 1)
            throw new RuntimeException("personnelNumber is incorrect");
        else this.personnelNumber = personnelNumber;

        /* Проверки корректности введенных дат */
        if(contractStartDate != null) {
            if(contractExpirationDate != null && contractStartDate.compareTo(contractExpirationDate) > 0)
                throw new RuntimeException("Contract Start Date is after Contract Expiration Date");
            else this.contractStartDate = contractStartDate;
        }
        else throw new RuntimeException("Contract Start Date is null");

        if(contractExpirationDate != null) {
            if(contractStartDate != null && contractStartDate.compareTo(contractExpirationDate) > 0)
                throw new RuntimeException("Contract Start Date is after Contract Expiration Date");
            else this.contractExpirationDate = contractExpirationDate;
        }
        else this.contractExpirationDate = contractExpirationDate;

        /* Проверки корректности значений scientificTitle, scienceDegree */
        if(scientificTitleInstance.checkScientificTitleExists(scientificTitle))
            this.scientificTitle = scientificTitle;
        else throw new RuntimeException("Incorrect Scientific Title");

        if(scienceDegreeInstance.checkScienceDegreeExists(scienceDegree))
            this.scienceDegree = scienceDegree;
        else throw new RuntimeException("Incorrect Science Degree");

        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public Position getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(Position currentPosition) {

     /****
      *
      * upd. Убрал закомментированные проверки на несовпадение Новой Должности со Старой Должностью
      * с последующим вызовом this.currentPosition.add/deleteEmployee, так как создавались дубли.
      * Проверки будут осуществляться на уровне бизнес-процессов; Если Новая Должность не совпадает со
      * Старой Должностью, делаем oldPosition.deleteEmployee() и newPosition.addEmployee() - этого будет достаточно.
      *
      * ****/

        /* Добавляем сотрудника в Position.employees родительской должности
         * Если Должность не совпадает с текущей, необходимо удалить этого сотрудника из текущей Должности*/
        /*if(!this.currentPosition.equals(currentPosition))
            this.currentPosition.deleteEmployee(this); */
        //Если должность null, значит уволен
        this.currentPosition = currentPosition;
        //this.currentPosition.addEmployee(this);
    }

    public int getPersonnelNumber() {
        return personnelNumber;
    }

    public void setPersonnelNumber(int personnelNumber) {
        this.personnelNumber = personnelNumber;
    }

    public String getScientificTitle() {
        return scientificTitle;
    }

    public void setScientificTitle(String scientificTitle) {
        if(scientificTitleInstance.checkScientificTitleExists(scientificTitle))
            this.scientificTitle = scientificTitle;
        else throw new RuntimeException("Incorrect Scientific Title");
    }

    public String getScienceDegree() {
        return scienceDegree;
    }

    public void setScienceDegree(String scienceDegree) {
        if(scienceDegreeInstance.checkScienceDegreeExists(scienceDegree))
            this.scienceDegree = scienceDegree;
        else throw new RuntimeException("Incorrect Science Degree");
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getContractStartDate() {
        return contractStartDate;
    }

    public void setContractStartDate(Date contractStartDate) {
        if(contractStartDate != null) {
            if(this.contractExpirationDate != null && contractStartDate.compareTo(contractExpirationDate) > 0)
                throw new RuntimeException("Contract Start Date is after Contract Expiration Date");
            else this.contractStartDate = contractStartDate;
        }
        else throw new RuntimeException("Contract Start Date is null");
    }

    public Date getContractExpirationDate() {
        return contractExpirationDate;
    }

    public void setContractExpirationDate(Date contractExpirationDate) {
        if(contractExpirationDate != null) {
            if(this.contractStartDate != null && this.contractStartDate.compareTo(contractExpirationDate) > 0)
                throw new RuntimeException("Contract Start Date is after Contract Expiration Date");
            else this.contractExpirationDate = contractExpirationDate;
        }
        else this.contractExpirationDate = contractExpirationDate;
    }
}
