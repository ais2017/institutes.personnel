package com.institutes.personnel;

import com.institutes.personnel.flow.ExecutionFlowService;
import com.institutes.personnel.flow.impl.ExecutionFlowServiceBean;
import com.institutes.personnel.service.UserInteractionService;
import com.institutes.personnel.service.impl.UserInteractionServiceBean;

import javax.swing.*;


public class Main
{

    private UserInteractionService userInteractionService;

    private ExecutionFlowService executionFlowService;

    public Main() throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        this.userInteractionService = new UserInteractionServiceBean();
        this.executionFlowService = new ExecutionFlowServiceBean();
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }

    public static void main(String[] args ) {
        ExecutionFlowService executionFlowService = null;
        try {
            executionFlowService = new ExecutionFlowServiceBean();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        executionFlowService.start();

     //   System.exit(0);
    }
}
