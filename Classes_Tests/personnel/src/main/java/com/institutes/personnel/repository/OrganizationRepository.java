package com.institutes.personnel.repository;

import com.institutes.personnel.model.Organization;

public interface OrganizationRepository {

    /** Только одна Организация **/
    public Organization findOrganization();

}
