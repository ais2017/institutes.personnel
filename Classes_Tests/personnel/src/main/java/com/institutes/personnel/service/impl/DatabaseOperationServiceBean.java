package com.institutes.personnel.service.impl;

import com.institutes.personnel.model.*;
import com.institutes.personnel.service.DatabaseConnectionService;
import com.institutes.personnel.service.DatabaseOperationService;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.*;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;

public class DatabaseOperationServiceBean implements DatabaseOperationService {

    private DatabaseConnectionService databaseConnectionService;
    private Connection connection;
    private final int connectionTimeout = 3;

    public DatabaseOperationServiceBean() {

        this.databaseConnectionService = new DatabaseConnectionServiceBean();
        try {
            connection = this.databaseConnectionService.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public DatabaseOperationServiceBean(Connection connection) {
        this.databaseConnectionService = new DatabaseConnectionServiceBean();
        this.connection = connection;
    }

    public DatabaseOperationServiceBean(Connection connection, DatabaseConnectionService databaseConnectionService) {
        this.connection = connection;
        this.databaseConnectionService = databaseConnectionService;
    }

    /** Поиск Сотрудника по id. Как работает: полностью инициализирует объект родительской Должности, и Подразделения, к которому относится эта Должность. **/
    @Override
    public Employee findEmployeeById(int id) {
        Employee employee = null;
     //   Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT p.* " +
                "FROM person_t p " +
                "WHERE(p.id = ?)";

        try {

            /** Получаем соединение **/
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
             *  необходим курсор, с помощью которого можно перемещаться по записям **/
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setInt(1, id);

            resultSet = statement.executeQuery();

            /** Получаем количество строк, которое было получено после выполнения запроса **/
            resultSet.last();
            int numRows = resultSet.getRow();

            /** Если запрос вернул несколько строк, выбрасываем исключение **/
            if(numRows > 1)
                throw new RuntimeException("Several Employees with id " + id + "found");
            else if(numRows == 1) {

                resultSet.beforeFirst();
                while (resultSet.next()) {
                    int passportSeries = resultSet.getInt("PASSPORTSERIES");
                    int passportNumber = resultSet.getInt("PASSPORTNUMBER");
                    int currentPositionId = resultSet.getInt("POSITIONID");
                    int personnelNumber = resultSet.getInt("PERSONNELNUMBER");

                    /** Получаем объект Должности, с которой связан этот Сотрудник **/
                    Position currentPosition = this.findPositionById(currentPositionId);

                    /** Если объект Сотрудника не удалось инциализировать - поиск считаем неуспешным
                     *  ведь без Сотрудника нельзя создать Пользователя **/
                    if (currentPosition != null) {
                        /** Получаем объект Сотрудника **/
                        employee = currentPosition.searchEmployeeByPersonnelNumber(personnelNumber);
                    }

                    /** Upd. Убрал блок Создания объекта Сотрудника, так как в полученном объекте Должности
                     *  нужный Сотрудник уже содержится. Для получения объекта этого Сотрудника можно воспользоваться
                     *  методами поиска Position.searchEmployeeBy*. Если создавать новый объект, его хэш будет
                     *  отличаться от хэша того же Сотрудника с точки зрения логики, который уже
                     *  есть в currentPosition.employees() - это два разных объекта **/
                    /* Создаем объект Сотрудника */
/*                employee = new Employee(lastName, firstName, middleName, birthday,
                        passportSeries, passportNumber, currentPosition, personnelNumber,
                        scientificTitle, scienceDegree, email, contractStartDate, contractExpirationDate); */
                }
            }

            //Закрываем
            resultSet.close();
            statement.close();
         //   connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }




        return employee;
    }

    @Override
    public Position findPositionById(int id) {
        Position position = null;
     //   Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT p.* " +
                "FROM position_t p " +
                "WHERE(p.id = ?)";

        /** Получаем соединение **/
        try {
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
             *  необходим курсор, с помощью которого можно перемещаться по записям **/
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setInt(1, id);

            resultSet = statement.executeQuery();

            /** Получаем количество строк, которое было получено после выполнения запроса **/
            resultSet.last();
            int numRows = resultSet.getRow();

            /** Если запрос вернул несколько строк, выбрасываем исключение **/
            if(numRows > 1)
                throw new RuntimeException("Several Positions with id " + id + "found");
            else if(numRows == 1) {

                resultSet.beforeFirst();
                while (resultSet.next()) {
                    String positionName = resultSet.getString("POSITIONNAME");
                    int parentDivisionId = resultSet.getInt("DIVISIONID");

                    /** Upd. Убрал, так как при создании parentDivision будет получен объект Division
                     *  с корректно инициализованным массивом positions. У каждой Должности в
                     *  parentDivision.Position уже будет массив position.employees **/
                    /* Получаем коллекцию всех Сотрудников, которые занимают эту Должность.
                     *  У всех employees currentPosition = null */
                    // Collection<Employee> employees = this.findEmployeesByPositionIdForCreatingPosition(id);

                    /** Получаем объект родительского Подразделения **/
                    Division parentDivision = this.findDivisionById(parentDivisionId);

                    /** Upd. Убрал блок Создания объекта Должности, так как в полученном объекте Подразделения
                     *  нужная Должность уже содержится. Для получения объекта этой Должности можно воспользоваться
                     *  методом поиска Division.getPositionByName. Если создавать новый объект, его хэш будет
                     *  отличаться от хэша той же Должности с точки зрения логики, которая уже
                     *  есть в parentDivision.positions() - это два разных объекта **/
                    /* Создаем объект Должности. В конструкторе Position всем employees будет присвоено currentPosition */
                    /* position = new Position(positionName, maxEmployees, vacantPositions, parentDivision, employees); */

                    /** Получаем объект Должности **/
                    position = parentDivision.getPositionByName(positionName);

                }
            }

            //Закрываем
            resultSet.close();
            statement.close();
         //   connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return position;
    }

    @Override
    public Division findDivisionById(int id) {
        Division division = null;
     //   Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT d.* " +
                "FROM division_t d " +
                "WHERE(d.ID = ?)";

        try {
            /** Получаем соединение **/
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
             *  необходим курсор, с помощью которого можно перемещаться по записям **/
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setInt(1, id);

            resultSet = statement.executeQuery();

            /** Получаем количество строк, которое было получено после выполнения запроса **/
            resultSet.last();
            int numRows = resultSet.getRow();

            /** Если запрос вернул несколько строк, выбрасываем исключение **/
            if(numRows > 1)
                throw new RuntimeException("Several Divisions with id " + id + "found");
            else if(numRows == 1) {

                resultSet.beforeFirst();
                while (resultSet.next()) {
                    String divisionCode = resultSet.getString("DIVISIONCODE");
                    String divisionName = resultSet.getString("DIVISIONNAME");

                    /** Получаем коллекцию Должностей, связанных с этим Подразделением.
                     *  У всех positions parentDivision = null **/
                    Collection<Position> positions = this.findPositionsByDivisionIdForCreatingDivision(id);

                    /** Создаем объект Подразделения **/
                    division = new Division(divisionCode, divisionName, positions);
                }
            }

            //Закрываем
            resultSet.close();
            statement.close();
         //   connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return division;
    }

    @Override
    public Collection<Employee> findEmployeesByPositionIdForCreatingPosition(int positionId) {
        Collection<Employee> employees = null;
     //   Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT per.* " +
                "FROM person_t per " +
                "JOIN position_t pos ON (per.POSITIONID = pos.ID) " +
                "WHERE(pos.ID = ?)";

        try {
            /** Получаем соединение **/
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
             *  необходим курсор, с помощью которого можно перемещаться по записям **/
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setInt(1, positionId);

            resultSet = statement.executeQuery();

            /** Получаем количество строк, которое было получено после выполнения запроса **/
            resultSet.last();
            int numRows = resultSet.getRow();

            /** Если не было найдено Сотрудников, занимающих эту Должность, возвращаем коллекцию размера 0 **/
            if(numRows < 1) {
                //Закрываем
                resultSet.close();
                statement.close();
                connection.close();
                employees = new LinkedHashSet<>();
                return employees;
            }
            /** Если были найдены Сотрудники, создаем объекты для них и добавляем в коллекцию **/
            else {
                employees = new LinkedHashSet<>();
                resultSet.beforeFirst();
                while(resultSet.next()) {
                    String lastName = resultSet.getString("LASTNAME");
                    String firstName = resultSet.getString("FIRSTNAME");
                    String middleName = resultSet.getString("MIDDLENAME");
                    Date birthday = resultSet.getDate("BIRTHDAY");
                    int passportSeries = resultSet.getInt("PASSPORTSERIES");
                    int passportNumber = resultSet.getInt("PASSPORTNUMBER");
                    int currentPositionId = resultSet.getInt("POSITIONID");
                    int id = resultSet.getInt("ID");
                    int personnelNumber = resultSet.getInt("PERSONNELNUMBER");
                    String scientificTitle = resultSet.getString("SCIENTIFICTITLE");
                    String scienceDegree = resultSet.getString("SCIENCEDEGREE");
                    String email = resultSet.getString("EMAIL");
                    Date contractStartDate = resultSet.getDate("CONTRACTSTARTDATE");
                    Date contractExpirationDate = resultSet.getDate("CONTRACTEXPIRATIONDATE");

                    /** Создаем объект Сотрудника c currentPosition = null **/
                    Employee employee = new Employee(id, currentPositionId, lastName, firstName, middleName, birthday,
                            passportSeries, passportNumber, personnelNumber,
                            scientificTitle, scienceDegree, email, contractStartDate, contractExpirationDate);

                    /** Добавляем объект Сотрудника в коллекцию **/
                    employees.add(employee);
                }
            }

            //Закрываем
            resultSet.close();
            statement.close();
         //   connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return employees;
    }

    @Override
    public Collection<Position> findPositionsByDivisionIdForCreatingDivision(int divisionId) {
        Collection<Position> positions = null;
     //   Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT pos.* " +
                "FROM division_t div " +
                "JOIN position_t pos ON (div.ID = pos.DIVISIONID) " +
                "WHERE(div.ID = ?)";

        try {
            /** Получаем соединение **/
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
             *  необходим курсор, с помощью которого можно перемещаться по записям **/
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setInt(1, divisionId);

            resultSet = statement.executeQuery();

            /** Получаем количество строк, которое было получено после выполнения запроса **/
            resultSet.last();
            int numRows = resultSet.getRow();

            /** Если не было найдено Должностей, относящихся к этому Подразделению, возвращаем коллекцию размера 0 **/
            if(numRows < 1) {
                //Закрываем
                resultSet.close();
                statement.close();
                connection.close();
                positions = new LinkedHashSet<>();
                return positions;
            }
            /** Если были найдены Сотрудники, создаем объекты для них и добавляем в коллекцию **/
            else {
                positions = new LinkedHashSet<>();
                resultSet.beforeFirst();
                while(resultSet.next()) {
                    String positionName = resultSet.getString("POSITIONNAME");
                    int maxEmployees = resultSet.getInt("MAXEMPLOYEES");
                    int vacantPositions = resultSet.getInt("VACANTPOSITIONS");
                    int parentDivisionId = resultSet.getInt("DIVISIONID");
                    int positionId = resultSet.getInt("ID");

                    /** Получаем коллекцию всех Сотрудников, которые занимают эту Должность **/
                    Collection<Employee> employees = this.findEmployeesByPositionIdForCreatingPosition(positionId);

                    /** Создаем объект Должности с parentDivision = null **/
                    Position position = new Position(positionId, parentDivisionId, positionName, maxEmployees, vacantPositions, employees);

                    /** Добавляем объект Должности в коллекцию **/
                    positions.add(position);
                }
            }

            //Закрываем
            resultSet.close();
            statement.close();
         //   connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return positions;
    }

    @Override
    public User findUserByLogin(String login) {
        User user = null;
     //   Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT * FROM user_t u WHERE(u.LOGIN = ?)";

        try {
            /** Получаем соединение **/
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
             *  необходим курсор, с помощью которого можно перемещаться по записям **/
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setString(1, login);

            resultSet = statement.executeQuery();

            /** Получаем количество строк, которое было получено после выполнения запроса **/
            resultSet.last();
            int numRows = resultSet.getRow();

            /** Если запрос вернул несколько строк, выбрасываем исключение **/
            if(numRows > 1)
                throw new RuntimeException("Several Users with login " + login + "found");
            else if(numRows == 1) {

                resultSet.beforeFirst();
                while (resultSet.next()) {
                    String lg = resultSet.getString("LOGIN");
                    String pswd = resultSet.getString("PASSWORD");
                    String role = resultSet.getString("ROLE");
                    int personId = resultSet.getInt("PERSONID");
                    int id = resultSet.getInt("ID");

                    /** Получаем объект Сотрудника, с которым связан этот Пользователь **/
                    Employee employee = this.findEmployeeById(personId);

                    /** Если объект Сотрудника не удалось инциализировать - поиск считаем неуспешным
                     *  ведь без Сотрудника нельзя создать Пользователя **/
                    if (employee == null)
                        return null;

                    /** Создаем объект Пользователя **/
                    user = new User(id, personId, employee, lg, pswd, role);
                }
            }

            //Закрываем
            resultSet.close();
            statement.close();
         //   connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

        return user;
    }

/***************************************************************Организация.******************************************************************************/
    @Override
    public Organization findOrganization() {
        Organization organization = null;
    //    Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT * FROM organization_t";

        try {
            /** Получаем соединение **/
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
             *  необходим курсор, с помощью которого можно перемещаться по записям **/
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

            resultSet = statement.executeQuery();

            /** Получаем количество строк, которое было получено после выполнения запроса **/
            resultSet.last();
            int numRows = resultSet.getRow();

            /** Если запрос вернул несколько строк, выбрасываем исключение **/
            if(numRows > 1)
                throw new RuntimeException("Several Organizations found");
            else if(numRows == 1) {

                resultSet.beforeFirst();
                while (resultSet.next()) {
                    int organizationId = resultSet.getInt("ID");

                    /** Получаем коллекцию всех Подразделений, входящих в Организацию **/
                    Collection<Division> divisions = this.findDivisionsByOrganizationId(organizationId);

                    /** Создаем объект Организации **/
                    organization = new Organization(divisions);
                }
            }

            //Закрываем
            resultSet.close();
            statement.close();
         //   connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return organization;
    }

    @Override
    public Collection<Division> findDivisionsByOrganizationId(int organizationId) {
        LinkedHashSet<Division> divisions = new LinkedHashSet<>();
     //   Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT * " +
                       "FROM public.division_t d " +
                       "WHERE(d.ORGANIZATIONID = ?)";

        try {
            /** Получаем соединение **/
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
             *  необходим курсор, с помощью которого можно перемещаться по записям **/
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setInt(1, organizationId);

            resultSet = statement.executeQuery();

            /** Получаем количество строк, которое было получено после выполнения запроса **/
            resultSet.last();
            int numRows = resultSet.getRow();

            /** Если не было найдено Подразделений, относящихся к этой Организации, возвращаем коллекцию размера 0 **/
            if(numRows < 1) {
                //Закрываем
                resultSet.close();
                statement.close();
                connection.close();
                return divisions;
            }
            /** Если были найдены Подразделения, создаем объекты для них и добавляем в коллекцию **/
            else {
                resultSet.beforeFirst();
                while(resultSet.next()) {
                    int id = resultSet.getInt("ID");
                    int organizationdId = resultSet.getInt("ORGANIZATIONID");
                    String divisionCode = resultSet.getString("DIVISIONCODE");
                    String divisionName = resultSet.getString("DIVISIONNAME");

                    /** Получаем коллекцию всех Должностей, которые принадлежат этому Подразделению **/
                    Collection<Position> positions = this.findPositionsByDivisionIdForCreatingDivision(id);

                    /** Создаем объект Подразделения **/
                    Division division = new Division(id, organizationId, divisionCode, divisionName, positions);

                    /** Добавляем объект Подразделения в коллекцию **/
                    divisions.add(division);
                }
            }

            //Закрываем
            resultSet.close();
            statement.close();
         //   connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return divisions;
    }

/***************************************************************Персонал. Возможно будет убран.******************************************************************************/
    @Override
    public Staff getStaff() {
        Staff staff = null;

        Organization organization = this.findOrganization();

        Collection<Employee> employees = organization.getAllEmployeesOfOrganization();

        staff = new Staff(employees);

        return staff;
    }

    /***************************************************************Методы сохранения.******************************************************************************/

    @Override
    public Employee saveEmployee(Employee employee) {
        Employee savedEmployee = employee;
        PreparedStatement statement = null;

        try {
            if(!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            String query = "";

            /** Если id = < 1 - добавляем новую запись **/
            if(employee.getId() < 1) {
                query = "INSERT INTO person_t(\n" +
                        "\tid, positionid, lastname, firstname, middlename, birthday, passportseries, passportnumber, personnelnumber, scientifictitle, sciencedegree, contractstartdate, contractexpirationdate, email, discriminator)\n" +
                        "\tVALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

                /* Подставляем параметры в sql-выражение */
                statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

                statement.setInt(1, employee.getPositionId());
                statement.setString(2, employee.getLastName());
                statement.setString(3, employee.getFirstName());
                statement.setString(4, employee.getMiddleName());
                statement.setDate(5, new java.sql.Date(employee.getBirthday().getTime()));
                statement.setInt(6, employee.getPassportSeries());
                statement.setInt(7, employee.getPassportNumber());
                statement.setInt(8, employee.getPersonnelNumber());
                statement.setString(9, employee.getScientificTitle());
                statement.setString(10, employee.getScienceDegree());
                statement.setDate(11, new java.sql.Date(employee.getContractStartDate().getTime()));
                statement.setDate(12, new java.sql.Date(employee.getContractExpirationDate().getTime()));
                statement.setString(13, employee.getEmail());
                statement.setString(14, "");
            }
            /** Обновляем существующую запись **/
            else {
                query = "UPDATE person_t\n" +
                        "\tSET positionid=?, lastname=?, firstname=?, middlename=?, birthday=?, passportseries=?, passportnumber=?, personnelnumber=?, scientifictitle=?, sciencedegree=?, contractstartdate=?, contractexpirationdate=?, email=?, discriminator=?\n" +
                        "\tWHERE (id = ?);";

                /* Подставляем параметры в sql-выражение */
                statement = connection.prepareStatement(query);

                statement.setInt(1, employee.getPositionId());
                statement.setString(2, employee.getLastName());
                statement.setString(3, employee.getFirstName());
                statement.setString(4, employee.getMiddleName());
                statement.setDate(5, new java.sql.Date(employee.getBirthday().getTime()));
                statement.setInt(6, employee.getPassportSeries());
                statement.setInt(7, employee.getPassportNumber());
                statement.setInt(8, employee.getPersonnelNumber());
                statement.setString(9, employee.getScientificTitle());
                statement.setString(10, employee.getScienceDegree());
                statement.setDate(11, new java.sql.Date(employee.getContractStartDate().getTime()));
                statement.setDate(12, new java.sql.Date(employee.getContractExpirationDate().getTime()));
                statement.setString(13, employee.getEmail());
                statement.setString(14, "");
                statement.setInt(15, employee.getId());
            }

            /* Если != 1 - запись не была вставлена */
            if(statement.executeUpdate() != 1)
                savedEmployee = null;

            ResultSet rs = statement.getGeneratedKeys();
            int generatedId = 0;
            if (rs.next()) {
                generatedId = rs.getInt(1);
                employee.setId(generatedId);
            }
            rs.close();

            /** Изменение объекта Сотрудник сопровождается изменением атрибутов maxEmployees и vacantPositions
             *  Должности, которую он занимает, поэтому необходимо также обновить Должность, которую он занимал ранее
             *  и ту Должность, которую он занимает после обновления. **/

            //Закрываем
            statement.close();
            //        connection.close();
        } catch (SQLException e) {
            savedEmployee = null;
            e.printStackTrace();
        }

        return savedEmployee;
    }

    @Override
    public Order saveOrder(Order order) {
        Order savedOrder = order;
        PreparedStatement statement = null;

        try {
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            String query = "";

            /** Если id = < 1 - добавляем новую запись **/
            if(order.getId() < 1) {
                query = "INSERT INTO order_t(\n" +
                        "\tid, type)\n" +
                        "\tVALUES (DEFAULT, ?);";

                /* Подставляем параметры в sql-выражение */
                statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

                statement.setString(1, order.getType());

                /* Если != 1 - запись не была вставлена */
                if(statement.executeUpdate() != 1) {
                    savedOrder = null;
                    statement.close();
                    return savedOrder;
                }

                /* Берем сгенерированный id order и заполняем orderLine этим id */
                ResultSet rs = statement.getGeneratedKeys();
                int generatedId = 0;
                if (rs.next()) {
                    generatedId = rs.getInt(1);

                    order.setId(generatedId);
                    for(OrderLine orderLine: order.getOrderLines())
                        orderLine.setOrderId(generatedId);
                }
                rs.close();

                /* После сохранения Приказа необходимо сохранить его строки.
                 * Если метод вернул null - произошла ошибка и строки вставлены не были. */
                if(this.saveOrderLines(order.getOrderLines()) == null)
                    savedOrder = null;
            }
            /** Обновляем существующую запись **/
            else {
                query = "UPDATE order_t\n" +
                        "\tSET type=?\n" +
                        "\tWHERE (id = ?);";

                /* Подставляем параметры в sql-выражение */
                statement = connection.prepareStatement(query);

                statement.setString(1, order.getType());
                statement.setInt(2, order.getId());

                /* Если != 1 - запись не была вставлена */
                if(statement.executeUpdate() != 1)
                    savedOrder = null;
            }

            //Закрываем
            statement.close();
            //        connection.close();
        }
        catch (SQLException e) {
            savedOrder = null;
            e.printStackTrace();
        }

        return savedOrder;
    }

    @Override
    public Collection<OrderLine> saveOrderLines(Collection<OrderLine> orderLines) {
        Collection<OrderLine> savedOrderLines = orderLines;
        PreparedStatement statement = null;

        try {
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            String query = "INSERT INTO orderline_t(\n" +
                    "\tid, orderid, divisionid, positionid, personid, startdate, enddate)\n" +
                    "\tVALUES (DEFAULT, ?, ?, ?, ?, ?, ?);";

            for(OrderLine orderLine: orderLines) {
                /* Подставляем параметры в sql-выражение */
                statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

                statement.setInt(1, orderLine.getOrderId());
                statement.setInt(2, orderLine.getDivisionId());
                statement.setInt(3, orderLine.getPositionId());
                statement.setInt(4, orderLine.getPersonId());
                statement.setDate(5, new java.sql.Date(orderLine.getStartDate().getTime()));
                statement.setDate(6, new java.sql.Date(orderLine.getEndDate().getTime()));

                /* Если != 1 - запись не была вставлена */
                if(statement.executeUpdate() != 1) {
                    savedOrderLines = null;
                    break;
                }

                /* Обновить объект сгенерированным идентификатором */
                ResultSet rs = statement.getGeneratedKeys();
                int generatedId = 0;
                if (rs.next()) {
                    generatedId = rs.getInt(1);

                    orderLine.setId(generatedId);
                }
                rs.close();
                statement.close();
            }

            //Закрываем
            statement.close();
            //        connection.close();
        } catch (SQLException e) {
            savedOrderLines = null;
            e.printStackTrace();
        }

        return savedOrderLines;
    }

    @Override
    public User saveUser(User user) {
        User savedUser = user;
        PreparedStatement statement = null;

        try {
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            String query = "";

            /** Если id = < 1 - добавляем новую запись **/
            if(user.getId() < 1) {
                query = "INSERT INTO user_t(\n" +
                        "\tid, personid, login, password, role)\n" +
                        "\tVALUES (DEFAULT, ?, ?, ?, ?);";

                /* Подставляем параметры в sql-выражение */
                statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

                statement.setInt(1, user.getPersonId());
                statement.setString(2, user.getLogin());
                statement.setString(3, user.getPassword());
                statement.setString(4, user.getRole());
            }
            /** Обновляем существующую запись **/
            else {
                query = "UPDATE user_t\n" +
                        "\tSET personid=?, login=?, password=?, role=?\n" +
                        "\tWHERE (id = ?);";

                /* Подставляем параметры в sql-выражение */
                statement = connection.prepareStatement(query);

                statement.setInt(1, user.getPersonId());
                statement.setString(2, user.getLogin());
                statement.setString(3, user.getPassword());
                statement.setString(4, user.getRole());
                statement.setInt(5, user.getId());
            }

            /* Если != 1 - запись не была вставлена */
            if(statement.executeUpdate() != 1)
                savedUser = null;

            /* Обновить объект сгенерированным идентификатором в случае успешного INSERT */
            ResultSet rs = statement.getGeneratedKeys();
            int generatedId = 0;
            if (rs.next()) {
                generatedId = rs.getInt(1);
                user.setId(generatedId);
            }
            rs.close();

            //Закрываем
            statement.close();
            //        connection.close();
        } catch (SQLException e) {
            savedUser = null;
            e.printStackTrace();
        }

        return savedUser;
    }

    @Override
    public Position savePosition(Position position) {
        Position savedPosition = position;
        PreparedStatement statement = null;


        try {
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            String query = "";

            /** Если id = < 1 - добавляем новую запись **/
            if(position.getId() < 1) {
                query = "INSERT INTO position_t(\n" +
                        "\tid, divisionid, positionname, maxemployees, vacantpositions)\n" +
                        "\tVALUES (DEFAULT, ?, ?, ?, ?);";

                /* Подставляем параметры в sql-выражение */
                statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

                statement.setInt(1, position.getDivisionId());
                statement.setString(2, position.getPositionName());
                statement.setInt(3, position.getMaxEmployees());
                statement.setInt(4, position.getVacantPositions());
            }
            /** Обновляем существующую запись **/
            else {
                query = "UPDATE public.position_t\n" +
                        "\tSET divisionid=?, positionname=?, maxemployees=?, vacantpositions=?\n" +
                        "\tWHERE (id = ?);";

                /* Подставляем параметры в sql-выражение */
                statement = connection.prepareStatement(query);

                statement.setInt(1, position.getDivisionId());
                statement.setString(2, position.getPositionName());
                statement.setInt(3, position.getMaxEmployees());
                statement.setInt(4, position.getVacantPositions());
                statement.setInt(5, position.getId());
            }

            /* Если != 1 - запись не была вставлена */
            if(statement.executeUpdate() != 1)
                savedPosition = null;

            /* Обновить объект сгенерированным идентификатором в случае успешного INSERT */
            ResultSet rs = statement.getGeneratedKeys();
            int generatedId = 0;
            if (rs.next()) {
                generatedId = rs.getInt(1);
                position.setId(generatedId);
            }
            rs.close();

            //Закрываем
            statement.close();
            //       connection.close();
        } catch (SQLException e) {
            savedPosition = null;
            e.printStackTrace();
        }

        return savedPosition;
    }

    @Override
    public Division saveDivision(Division division) {
        throw new NotImplementedException();
    }

    @Override
    public void deletePosition(Position position) {
        if(position.getId() < 1)
            throw new RuntimeException("Cannot delete Position with id < 1");

        PreparedStatement statement = null;

        try {
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            String query = "DELETE FROM position_t\n" +
                    "\tWHERE (id = ?);";

            /* Подставляем параметры в sql-выражение */
            statement = connection.prepareStatement(query);

            statement.setInt(1, position.getId());

            /* Если != 1 - запись не была удалена */
            if(statement.executeUpdate() != 1)
                throw new RuntimeException("Cannot delete Position");

            //Закрываем
            statement.close();
            //       connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Невозможно удалить Должность " + position.getPositionName());
        }

        return;
    }

    @Override
    public void deleteUser(User user) {
        if(user.getId() < 1)
            throw new RuntimeException("Cannot delete User with id < 1");

        PreparedStatement statement = null;

        try {
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            String query = "DELETE FROM user_t\n" +
                    "\tWHERE (id = ?);";

            /* Подставляем параметры в sql-выражение */
            statement = connection.prepareStatement(query);

            statement.setInt(1, user.getId());

            /* Если != 1 - запись не была удалена */
            if(statement.executeUpdate() != 1)
                throw new RuntimeException("Cannot delete User");

            //Закрываем
            statement.close();
            //       connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Collection<Employee> saveEmployees(Collection<Employee> employees) {
        throw new NotImplementedException();
    }

    @Override
    public boolean checkCredentials(String login, String password) {
        boolean isCorrect = false;
     //   Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT * FROM user_t u WHERE(u.LOGIN = ?)";

        try {
            /** Получаем соединение **/
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
             *  необходим курсор, с помощью которого можно перемещаться по записям **/
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setString(1, login);

            resultSet = statement.executeQuery();

            /** Получаем количество строк, которое было получено после выполнения запроса **/
            resultSet.last();
            int numRows = resultSet.getRow();

            /** Если запрос вернул несколько строк, выбрасываем исключение **/
            if(numRows > 1)
                throw new RuntimeException("Several Users with login " + login + "found");
            else if(numRows == 1) {

                resultSet.beforeFirst();
                while (resultSet.next()) {
                    String lg = resultSet.getString("LOGIN");
                    String pswd = resultSet.getString("PASSWORD");

                    /** Проверяем совпадение логина и пароля **/
                    if (login.equals(lg) && password.equals(pswd))
                        isCorrect = true;
                    else isCorrect = false;
                }
            }

            //Закрываем
            resultSet.close();
            statement.close();
         //   connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return isCorrect;
    }

    @Override
    public Collection<User> findUsers() {
        Collection<User> users = null;
        //   Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT *\n" +
                       "FROM user_t;";

        try {
            /** Получаем соединение **/
            if (!this.connection.isValid(this.connectionTimeout)) {
                this.connection = this.databaseConnectionService.getConnection();
            }

            /** Настраиваем курсор. Так как у ResultSet нет метода получения количества строк,
             *  необходим курсор, с помощью которого можно перемещаться по записям **/
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

            resultSet = statement.executeQuery();

            /** Получаем количество строк, которое было получено после выполнения запроса **/
            resultSet.last();
            int numRows = resultSet.getRow();

            /** Если не было найдено Пользователей, возвращаем коллекцию размера 0 **/
            if(numRows < 1) {
                //Закрываем
                resultSet.close();
                statement.close();
                connection.close();
                users = new LinkedHashSet<>();
                return users;
            }
            /** Если были найдены Пользователи, создаем объекты для них и добавляем в коллекцию **/
            else {
                users = new LinkedHashSet<>();
                resultSet.beforeFirst();
                while(resultSet.next()) {
                    int id = resultSet.getInt("ID");
                    int personId = resultSet.getInt("PERSONID");
                    String login = resultSet.getString("LOGIN");
                    String password = resultSet.getString("PASSWORD");
                    String role = resultSet.getString("ROLE");

                    /** Создаем объект Пользователя c employee = null **/
                    User user = new User(id, personId, login, password, role);

                    /** Добавляем объект Пользователя в коллекцию **/
                    users.add(user);
                }
            }

            //Закрываем
            resultSet.close();
            statement.close();
            //   connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;

     //   return null;
    }
}
