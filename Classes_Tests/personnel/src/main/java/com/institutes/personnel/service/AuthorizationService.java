package com.institutes.personnel.service;

import com.institutes.personnel.service.classes.Credentials;

public interface AuthorizationService {

    public boolean checkCredentials(String login, String password);

}
