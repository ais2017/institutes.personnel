package com.institutes.personnel.repository.impl;

import com.institutes.personnel.model.Order;
import com.institutes.personnel.repository.OrderRepository;

import java.util.Collection;

public class OrderRepositoryService implements OrderRepository {

    @Override
    public Order saveOrder(Order order) {
        return order;
    }

    @Override
    public Collection<Order> findAll() {
        return null;
    }

    @Override
    public Collection<Order> saveAll(Collection<Order> orders) {
        return null;
    }
}
