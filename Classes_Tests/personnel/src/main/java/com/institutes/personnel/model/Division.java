package com.institutes.personnel.model;

import java.util.Collection;
import java.util.LinkedHashSet;

public class Division {

    /** Primary key в таблице division_t **/
    private int id;

    /** Foreign key к таблице organization_t **/
    private int organizationId;

    private String divisionCode;

    private String divisionName;

    private Collection<Position> positions;

    /* Основной конструктор */
    public Division(String divisionCode, String divisionName, Collection<Position> positions) {

        if(divisionCode == null || divisionName == null || positions == null || divisionCode.equals("") || divisionName.equals(""))
            throw new RuntimeException("Cannot create Object Division with specified parameters");

        this.id = -1;
        this.organizationId = -1;

        this.divisionCode = divisionCode;
        this.divisionName = divisionName;
        this.positions = positions;

        //set parentDivision for position in positions
        for(Position position: this.positions)
                position.setParentDivision(this);
    }

    /* Конструктор репозитория */
    public Division(int id, int organizationId, String divisionCode, String divisionName, Collection<Position> positions) {

        if(divisionCode == null || divisionName == null || positions == null || divisionCode.equals("") || divisionName.equals(""))
            throw new RuntimeException("Cannot create Object Division with specified parameters");

        this.id = id;
        this.organizationId = organizationId;

        this.divisionCode = divisionCode;
        this.divisionName = divisionName;
        this.positions = positions;

        //set parentDivision for position in positions
        for(Position position: this.positions)
            position.setParentDivision(this);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public String getDivisionCode() {
        return divisionCode;
    }

    public void setDivisionCode(String divisionCode) {
        this.divisionCode = divisionCode;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public void addPosition(Position position) {
        if(position == null)
            throw new RuntimeException("Cannot add null position");
        else if(this.positions.add(position))
            position.setParentDivision(this);
    }

    public void deletePosition(Position position) {
        this.positions.remove(position);
    }

    public Collection<Position> getPositions() {
        return positions;
    }

    public void setPositions(Collection<Position> positions) {
        this.positions = positions;
    }

    //В случае успеха возвращает объект типа Position
    //В случае неудачи возвращает null
    public Position getPositionByName(String positionName) {
        if(this.positions.size() < 1) {
            return null;
        }

        //Проходим в цикле все элементы сета positionsInStaffList, для каждого сета сравниваем positionName
        for(Position position: this.positions) {
            if(position.getPositionName().equals(positionName))
                return position;
        }

        //Возвращаем null, в случае, если предыдущий цикл не нашел элемент
        return null;

    }

    public Collection<Employee> getAllEmployeesOfDivision() {
        Collection<Employee> employees = new LinkedHashSet<>();

        for(Position position: this.getPositions()) {
            for(Employee employee: position.getEmployees()) {
                employees.add(employee);
            }
        }

        if(employees.size() > 0)
            return employees;
        else return null;
    }
}
