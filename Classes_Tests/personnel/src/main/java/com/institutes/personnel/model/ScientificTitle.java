package com.institutes.personnel.model;

import com.institutes.personnel.service.mock.ScientificTitleService;

import java.util.LinkedHashSet;

//Список предопределенных значений
public class ScientificTitle {

    private LinkedHashSet<String> scientificTitles;

    public ScientificTitle() {
        //this.scientificTitles = new LinkedHashSet<String>();
        this.scientificTitles = new LinkedHashSet<>(ScientificTitleService.findAll());
    }

    public ScientificTitle(LinkedHashSet<String> scientificTitles) {
        this.scientificTitles = scientificTitles;
    }

    public LinkedHashSet<String> getScientificTitles() {
        return scientificTitles;
    }

    public void setScientificTitles(LinkedHashSet<String> scientificTitles) {
        this.scientificTitles = scientificTitles;
    }

    public void addScientificTitle(String scientificTitle) {
        for (String item: this.scientificTitles) {
            if(item.equals(scientificTitle)) {
                return;
            }
        }

        this.scientificTitles.add(scientificTitle);
    }

    public boolean deleteScientificTitle(String scientificTitle) {
        boolean setContainedElement = this.scientificTitles.remove(scientificTitle);

        //true if the set contained the specified element
        return setContainedElement;
    }

    //Проверяет, есть ли строка scientificTitle в коллекции scientificTitles
    public boolean checkScientificTitleExists(String scientificTitle) {
        //Returns true if this set contains the specified element.
        //More formally, returns true if and only if this set contains an element e such that (o==null ? e==null : o.equals(e))
        if(this.scientificTitles.contains(scientificTitle))
            return true;
        else return false;
    }
}
