package com.institutes.personnel.gui;

import com.institutes.personnel.gui.mytablemodel.ChooseDivisionTableModel;
import com.institutes.personnel.model.Division;
import com.institutes.personnel.model.Organization;
import com.institutes.personnel.model.User;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class ChooseDivisionForm extends JFrame {
    private JPanel rootPanel;
    private JScrollPane scrollPane;
    private JTable divisionTable;
    private JButton confirmButton;
    private JLabel divisionTableLabel;

    private boolean confirmButtonClicked;
    private ChooseDivisionTableModel chooseDivisionTableModel;
    private Organization organization;

    public ChooseDivisionForm(Organization activeOrganization) {
        add(rootPanel);
        setTitle("Список подразделений");
        setSize(700, 500);
        rootPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        confirmButtonClicked = false;
        organization = activeOrganization;

        List<Division> divisionsList = new ArrayList<>(organization.getDivisions());
        chooseDivisionTableModel = new ChooseDivisionTableModel(divisionsList);
        divisionTable.setModel(chooseDivisionTableModel);
        divisionTable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        confirmButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                confirmButtonClicked = true;
            }
        });

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    public JTable getDivisionTable() {
        return divisionTable;
    }

    public void setDivisionTable(JTable divisionTable) {
        this.divisionTable = divisionTable;
    }

    public boolean isConfirmButtonClicked() {
        return confirmButtonClicked;
    }

    public void setConfirmButtonClicked(boolean confirmButtonClicked) {
        this.confirmButtonClicked = confirmButtonClicked;
    }

    public ChooseDivisionTableModel getChooseDivisionTableModel() {
        return chooseDivisionTableModel;
    }

    public void setChooseDivisionTableModel(ChooseDivisionTableModel chooseDivisionTableModel) {
        this.chooseDivisionTableModel = chooseDivisionTableModel;
    }

    public void updateDivisions(Organization activeOrganization) {
        organization = activeOrganization;
        List<Division> divisions = new ArrayList<>(activeOrganization.getDivisions());
     //   chooseDivisionTableModel.setDivisions(divisions);

        chooseDivisionTableModel = new ChooseDivisionTableModel(divisions);
        divisionTable.setModel(chooseDivisionTableModel);
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        rootPanel = new JPanel();
        rootPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(4, 1, new Insets(0, 0, 0, 0), -1, -1));
        scrollPane = new JScrollPane();
        rootPanel.add(scrollPane, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        divisionTable = new JTable();
        scrollPane.setViewportView(divisionTable);
        final com.intellij.uiDesigner.core.Spacer spacer1 = new com.intellij.uiDesigner.core.Spacer();
        rootPanel.add(spacer1, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        confirmButton = new JButton();
        confirmButton.setText("Подтвердить");
        rootPanel.add(confirmButton, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        divisionTableLabel = new JLabel();
        Font divisionTableLabelFont = this.$$$getFont$$$(null, Font.BOLD, 16, divisionTableLabel.getFont());
        if (divisionTableLabelFont != null) divisionTableLabel.setFont(divisionTableLabelFont);
        divisionTableLabel.setText("Список подразделений");
        rootPanel.add(divisionTableLabel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return rootPanel;
    }
}
