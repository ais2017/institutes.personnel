package com.institutes.personnel.gui.mytablemodel;

import com.institutes.personnel.model.Position;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

public class ChoosePositionTableModel implements TableModel {
    private Collection<TableModelListener> listeners = new LinkedHashSet<>();

    private List<Position> positions;

    public ChoosePositionTableModel(List<Position> positions) {
        this.positions = positions;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    @Override
    public int getRowCount() {
        return positions.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Код подразделения";
            case 1:
                return "Название подразделения";
            case 2:
                return "Название должности";
            case 3:
                return "Максимальное количество сотрудников";
            case 4:
                return "Количество свободных должностей";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Position position = positions.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return position.getParentDivision().getDivisionCode();
            case 1:
                return position.getParentDivision().getDivisionName();
            case 2:
                return position.getPositionName();
            case 3:
                return position.getMaxEmployees();
            case 4:
                return position.getVacantPositions();
        }
        return "";
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }
}
