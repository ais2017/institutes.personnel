package com.institutes.personnel.repository;

import com.institutes.personnel.model.Position;

import java.util.Collection;

public interface PositionRepository {

    public Position savePosition(Position position);

    public void deletePosition(Position position);

    public Collection<Position> findAll();

    /** Поиск Должности по идентификатору **/
    public Position findPositionById(int id);

    /** Поиск всех Должностей, принадлежащих определенному Подразделению.
     *  Этот метод возвращает коллекцию Должностей, у которых parentDivision = null!
     *  Предполагаемое применение: в методе DivisionRepository.findDivisionById
     *  для корректной инициализации Подразделения. Каждой Должности
     *  будет присвоен parentDivision в конструкторе Division **/
    public Collection<Position> findPositionsByDivisionIdForCreatingDivision(int divisionId);

}
