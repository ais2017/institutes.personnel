package com.institutes.personnel.gui.mytablemodel;

import com.institutes.personnel.model.Division;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

public class ChooseDivisionTableModel implements TableModel {
    private Collection<TableModelListener> listeners = new LinkedHashSet<>();

    private List<Division> divisions;

    public ChooseDivisionTableModel(List<Division> divisions) {
        this.divisions = divisions;
    }

    public List<Division> getDivisions() {
        return divisions;
    }

    public void setDivisions(List<Division> divisions) {
        this.divisions = divisions;
    }

    @Override
    public int getRowCount() {
        return divisions.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Код подразделения";
            case 1:
                return "Название подразделения";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Division division = divisions.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return division.getDivisionCode();
            case 1:
                return division.getDivisionName();
        }
        return "";
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }
}
