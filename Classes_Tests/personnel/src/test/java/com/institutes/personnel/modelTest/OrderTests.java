package com.institutes.personnel.modelTest;

import com.institutes.personnel.model.*;
import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;

//When using this mode, a new test instance will be created for each test method or test factory method.
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class OrderTests {

    public Order order;
    public Division division1;
    public Employee employee1;
    public Employee employee2;

    @BeforeEach
    void init() {

        /* Создаем несколько экземпляров OrderLine */

        //Даты
        Date startDate1 = new GregorianCalendar(2018, 8, 1).getTime();
        Date expirationDate1 = new GregorianCalendar(2020, 8, 1).getTime();
        Date startDate2 = new GregorianCalendar(2018, 8, 1).getTime();
        Date expirationDate2 = new GregorianCalendar(2019, 8, 1).getTime();

        //Подразделение
     //   LinkedHashSet<Position> positions1 = new LinkedHashSet<Position>();
     //   Division division1 = new Division("0101", "First biological institute experimental division", positions1);

        //Должности
     //   Position position1 = new Position("Преподаватель", 50, 30, division1, new LinkedHashSet<Employee>());
     //   Position position2 = new Position("Секретарь", 10, 7, division1, new LinkedHashSet<Employee>());

        //Массив должностей
        LinkedHashSet<Position> positions1 = new LinkedHashSet<Position>();

        //Подразделение
        division1 = new Division("0101", "First division", positions1);

        //Должности
        Position position1 = new Position("Преподаватель", 50, 30, division1, new LinkedHashSet<Employee>());
        Position position2 = new Position("Директор", 5, 1, division1, new LinkedHashSet<Employee>());

        division1.addPosition(position1);
        division1.addPosition(position2);


        //Сотрудники
        employee1 = new Employee("Федоров", "Сергей", "Витальевич", new GregorianCalendar(1956, 7, 9).getTime(),
                4050, 404505, position1, 111, "Доцент", "Кандидат наук", "test@domain.com",
                startDate1, expirationDate1);
        employee2 = new Employee("Сидоров", "Валентин", "Михайлович", new GregorianCalendar(1958, 8, 12).getTime(),
                4060, 505606, position2, 114, "Отсутствует", "Отсутствует", "test@domain.com", startDate2, expirationDate2);

        //Приказ
        this.order = new Order(new LinkedHashSet<OrderLine>(Arrays.asList(new OrderLine(employee1, position1, division1, startDate1, expirationDate1),
                                                                          new OrderLine(employee2, position2, division1, startDate2, expirationDate2))), "Прием");

    }

    @Test
    @DisplayName("Тест: после создания объекта Order в блоке init() количество входящих" +
            "в него OrderLine должно быть равно двум")
    public void TestThatOrganizationWasCorrectlyInitializedWithTwoDivisions() {

        //arrange и act - в блоке init()

        //assert
        Assertions.assertEquals(2, this.order.getOrderLines().size());

    }

    @Test
    @DisplayName("Тест: проверка корректности работы методов addOrderLine и deleteOrderLine" +
            "После добавления новой Строки приказа в Приказ с двумя Строками приказа " +
            "количество Строк приказа должно равняться трем. " +
            "После удаления третей Строки приказа количество Строк приказа должно равняться двум")
    public void TestThatAddingThirdOrderLineToOrderWithTwoOrderLinesReturnsSizeThree_AndAfterRemovingThirdOrderLineReturnsSizeTwo() {
        //Проверяем, что в начале теста количество подразделений равно двум
        Assertions.assertEquals(2, this.order.getOrderLines().size());

        //добавляем строку приказа
        Date startDate3 = new GregorianCalendar(2018, 8, 1).getTime();
        Date expirationDate3 = new GregorianCalendar(2021, 8, 1).getTime();
        Position position3 = new Position("Директор", 5, 5, division1, new LinkedHashSet<Employee>());
        Employee employee3 = new Employee("Петров", "Николай", "Игоревич", new GregorianCalendar(1956, 10, 9).getTime(),
                4070, 606707, position3, 115, "Профессор", "Доктор наук", "test@domain.com",
                startDate3, expirationDate3);

        OrderLine orderLine3 = new OrderLine(employee3, position3, division1, startDate3, expirationDate3);

        //Добавляем строку приказа в приказ
        this.order.addOrderLine(orderLine3);

        //Проверяем, что количество строк приказа стало равно трем
        Assertions.assertEquals(3, this.order.getOrderLines().size());

        //Удаляем строку приказа из приказа
        this.order.deleteOrderLine(orderLine3);

        //Проверяем, что количество строк приказа стало равно двум
        Assertions.assertEquals(2, this.order.getOrderLines().size());

    }

    /* Тесты методов поиска */
    @Test
    @DisplayName("Тест: проверка работоспособности метода searchOrderLineByEmployee. " +
            "При поиске searchOrderLineByEmployee(employee1) должен вернуть " +
            "Строку приказа.")
    public void TestSearchOrderLineByEmployee_ByExistentEmployee_ReturnsCorrectOrderLine() {
        //arrange
        OrderLine orderLine;

        //act
        orderLine = this.order.searchOrderLineByEmployee(this.employee1);

        //assert
        Assertions.assertEquals("Преподаватель", orderLine.getPosition().getPositionName());
    }

    /* Тесты методов поиска */
    @Test
    @DisplayName("Тест: проверка работоспособности метода searchOrderLineByEmployee. " +
            "При поиске с несуществующим сотрудником должен вернуть null.")
    public void TestSearchOrderLineByEmployee_ByNonExistentEmployee_ReturnsNull() {
        //arrange
        Date startDate3 = new GregorianCalendar(2018, 8, 1).getTime();
        Date expirationDate3 = new GregorianCalendar(2021, 8, 1).getTime();
        Position position3 = new Position("Секретарь", 3, 3, division1, new LinkedHashSet<Employee>());
        Employee employee3 = new Employee("Петров", "Николай", "Игоревич", new GregorianCalendar(1956, 10, 9).getTime(),
                4070, 606707, position3, 115, "Профессор", "Доктор наук", "test@domain.com",
                startDate3, expirationDate3);

        OrderLine orderLine;

        //act
        orderLine = this.order.searchOrderLineByEmployee(employee3);

        //assert
        Assertions.assertNull(orderLine);
    }

}
