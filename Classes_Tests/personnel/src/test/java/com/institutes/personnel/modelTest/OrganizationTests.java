package com.institutes.personnel.modelTest;

import com.institutes.personnel.model.*;
import org.junit.jupiter.api.*;

import java.util.LinkedHashSet;

//When using this mode, a new test instance will be created for each test method or test factory method.
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class OrganizationTests {

    public Organization organization;

    @BeforeEach
    void init() {

        //Подготовка данных для теста Organization
        //Состоит из двух Division, со своими сетами positions

        //Для Division1
        LinkedHashSet<Position> positions1 = new LinkedHashSet<Position>();
        Division division1 = new Division("0101", "First biological institute experimental division", positions1);
        division1.addPosition(new Position("Professor", 50, 30, division1, new LinkedHashSet<Employee>()));
        division1.addPosition(new Position("Dean", 5, 1, division1, new LinkedHashSet<Employee>()));
        division1.addPosition(new Position("Secretary", 10, 7, division1, new LinkedHashSet<Employee>()));

        //Для Division2
        LinkedHashSet<Position> positions2 = new LinkedHashSet<Position>();
        Division division2 = new Division("0102", "First biological institute practical division", positions2);
        division2.addPosition(new Position("Professor", 40, 30, division2, new LinkedHashSet<Employee>()));
        division2.addPosition(new Position("Dean", 3, 1, division2, new LinkedHashSet<Employee>()));
        division2.addPosition(new Position("Admin", 7, 7, division2, new LinkedHashSet<Employee>()));

        //Объединение дивизий
        LinkedHashSet<Division> divisions = new LinkedHashSet<Division>();
        divisions.add(division1);
        divisions.add(division2);

        //Организация
        this.organization = new Organization(divisions);

    }

    @Test
    @DisplayName("Тест: после создания объекта organization в блоке initAll() количество входящих" +
            "в него подразделений должно быть равно двум")
    public void TestThatOrganizationWasCorrectlyInitializedWithTwoDivisions() {

        //arrange и act - в блоке init()

        //assert
        Assertions.assertEquals(2, this.organization.getDivisions().size());

    }

    @Test
    @DisplayName("Тест: проверка корректности работы методов addDivision и deleteDivision" +
            "После добавления нового подразделения в организацию с двумя подразделениями " +
            "количество подразделений должно равняться трем. " +
            "После удаления третьего подразделения количество подразделений должно равняться двум")
    public void TestThatAddingThirdDivisionToOrganizationWithTwoDivisionsReturnsSizeThree_AndAfterRemovingThirdDivisionReturnsSizeTwo() {

        //Проверяем, что в начале теста количество подразделений равно двум
        Assertions.assertEquals(2, this.organization.getDivisions().size());

        //добавляем подразделение
        LinkedHashSet<Position> positions3 = new LinkedHashSet<Position>();
        Division division3 = new Division("0102", "First biological institute practical division", positions3);
        division3.addPosition(new Position("Senior", 15, 4, division3, new LinkedHashSet<Employee>()));
        division3.addPosition(new Position("Mistor", 10, 3, division3, new LinkedHashSet<Employee>()));
        division3.addPosition(new Position("Herr", 5, 1, division3, new LinkedHashSet<Employee>()));

        //Добавляем новое подразделение в организацию с помощью метода addDivision
        this.organization.addDivision(division3);

        //Проверяем, что количество подразделений стало равно трем
        Assertions.assertEquals(3, this.organization.getDivisions().size());

        //Удаляем третье подразделение из организации с помощью метода deleteDivision
        this.organization.deleteDivision(division3);

        //Проверяем, что количество подразделений стало равно двум
        Assertions.assertEquals(2, this.organization.getDivisions().size());

    }

    @Test
    @DisplayName("Тест: проверка работоспособности метода getDivisionByCode. " +
            "При поиске getDivisionByCode('0101') должен вернуть подразделение " +
            "с divisionName 'First biological institute experimental division'.")
    public void TestGetDivisionByExistentCodeReturnsCorrectDivision() {

        //Поиск по коду '0101'
        Division division = this.organization.getDivisionByCode("0101");

        //assert
        Assertions.assertEquals("0101", division.getDivisionCode());
        Assertions.assertEquals("First biological institute experimental division", division.getDivisionName());

    }

    @Test
    @DisplayName("Тест: проверка работоспособности метода getDivisionByCode. " +
            "При поиске getDivisionByCode('0103') вернет null.")
    public void TestGetDivisionByNonExistentCodeReturnsNull() {

        //Поиск по коду '0103'
        Division division = this.organization.getDivisionByCode("0103");

        //assert
        Assertions.assertNull(division);

    }

    @Test
    @DisplayName("Тест: проверка работоспособности метода getDivisionByCode. " +
            "При поиске по Организации, не содержащей Подразделений, вернет null.")
    public void TestGetDivisionByCode_IfOrganizationHasNoDivisions_ReturnsNull() {

        //Удаляем все подразделения
        this.organization.getDivisions().clear();

        //Поиск по коду '0101'
        Division division = this.organization.getDivisionByCode("0101");

        //assert
        Assertions.assertNull(division);

    }

    @Test
    @DisplayName("Тест: проверка работоспособности метода getDivisionByName. " +
            "При поиске getDivisionByName('First biological institute experimental division') должен вернуть " +
            "подразделение с divisionCode '0101'.")
    public void TestGetDivisionByExistentNameReturnsCorrectDivision() {

        //Поиск по имени 'First biological institute experimental division'
        Division division = this.organization.getDivisionByName("First biological institute experimental division");

        //assert
        Assertions.assertEquals("0101", division.getDivisionCode());
        Assertions.assertEquals("First biological institute experimental division", division.getDivisionName());

    }

    @Test
    @DisplayName("Тест: проверка работоспособности метода getDivisionByName. " +
            "При поиске getDivisionByName('NonExistentName') вернет null.")
    public void TestGetDivisionByNonExistentNameReturnsNull() {

        //Поиск по имени 'NonExistentName'
        Division division = this.organization.getDivisionByName("NonExistentName");

        //assert
        Assertions.assertNull(division);

    }

    @Test
    @DisplayName("Тест: проверка работоспособности метода getDivisionByName. " +
            "При поиске по Организации, не содержащей Подразделений, вернет null.")
    public void TestGetDivisionByName_IfOrganizationHasNoDivisions_ReturnsNull() {

        //Удаляем все подразделения
        this.organization.getDivisions().clear();

        //Поиск по имени 'First biological institute experimental division'
        Division division = this.organization.getDivisionByName("First biological institute experimental division");

        //assert
        Assertions.assertNull(division);

    }
    /****/
    @Test
    @DisplayName("Тест: проверка работоспособности метода getDivisionByCodeAndName. " +
            "При поиске getDivisionByCodeAndName('0101', 'First biological institute experimental division') должен вернуть " +
            "подразделение с divisionCode '0101' и divisionName 'First biological institute experimental division'.")
    public void TestGetDivisionByExistentCodeAndNameReturnsCorrectDivision() {

        //Поиск по коду '0101' и имени 'First biological institute experimental division'
        Division division = this.organization.getDivisionByCodeAndName("0101","First biological institute experimental division");

        //assert
        Assertions.assertEquals("0101", division.getDivisionCode());
        Assertions.assertEquals("First biological institute experimental division", division.getDivisionName());

    }

    @Test
    @DisplayName("Тест: проверка работоспособности метода getDivisionByCodeAndName. " +
            "При поиске getDivisionByName('0103', 'First biological institute experimental division') вернет null, " +
            "так как нет подразделения с кодом '0103'")
    public void TestGetDivisionByNonExistentCodeOrNameReturnsNull() {

        //Поиск по несуществующему коду '0103' и имени 'First biological institute experimental division'
        Division division = this.organization.getDivisionByCodeAndName("0103", "First biological institute experimental division");

        //assert
        Assertions.assertNull(division);

    }

    @Test
    @DisplayName("Тест: проверка работоспособности метода getDivisionByCodeAndName. " +
            "При поиске по Организации, не содержащей Подразделений, вернет null.")
    public void TestGetDivisionByCodeAndName_IfOrganizationHasNoDivisions_ReturnsNull() {

        //Удаляем все подразделения
        this.organization.getDivisions().clear();

        //Поиск по коду '0101' и имени 'First biological institute experimental division'
        Division division = this.organization.getDivisionByCodeAndName("0101","First biological institute experimental division");

        //assert
        Assertions.assertNull(division);

    }

    @AfterEach
    void tearDown() {
        for (Division d:  this.organization.getDivisions()) {
            for(Position p: d.getPositions())
                p.getEmployees().clear();
            d.getPositions().clear();
        }
        this.organization.getDivisions().clear();
    }

}
