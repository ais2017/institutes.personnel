package com.institutes.personnel.interfacesTest;

import com.institutes.personnel.model.*;
import com.institutes.personnel.repository.UserRepository;
import com.institutes.personnel.repository.impl.UserRepositoryService;
import com.institutes.personnel.service.DatabaseOperationService;
import com.institutes.personnel.service.impl.DatabaseOperationServiceBean;
import org.junit.jupiter.api.*;

import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;

//When using this mode, a new test instance will be created for each test method or test factory method.
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class DatabaseInterfaceTests {

    private final DatabaseOperationService databaseOperationService = new DatabaseOperationServiceBean();
    private Organization organization;

    @BeforeEach
    public void init() {
        this.organization = databaseOperationService.findOrganization();
    }

    @Test
    @DisplayName("Проверка работоспособности DatabaseOperationService.finUserByLogin")
    public void TestThatDatabaseOperationServiceFindUserByLoginWorks() {

        //arrange
        String login = "NKOLESNIKOVA";

        //act
        User user = this.databaseOperationService.findUserByLogin(login);

        //assert
        Assertions.assertEquals("NKOLESNIKOVA", user.getLogin());
        Assertions.assertEquals("NKOLESNIKOVA", user.getPassword());
        Assertions.assertEquals("Руководитель ОК", user.getRole());
        Assertions.assertNotNull(user.getEmployee());
        Assertions.assertEquals("Колесникова", user.getEmployee().getLastName());

    }

    @Test
    @DisplayName("Проверка работоспособности DatabaseOperationService.findOrganization")
    public void TestThatDatabaseOperationServiceFindOrganizationWorks() {

        //act
        Organization organization;

        //act
        organization = this.databaseOperationService.findOrganization();

        //assert
        Assertions.assertNotNull(organization);
        Assertions.assertEquals(3, organization.getDivisions().size());
    }

    /*********************************************************** Тесты методов сохранения ************************/

    /*1. saveEmployee */
    @Test
    @DisplayName("Проверка работоспособности DatabaseOperationService.saveEmployee")
    public void TestThatDatabaseOperationServiceSaveEmployeeCreatesNewDBRecord() {

        //arrange
        Staff staff = new Staff(this.organization.getAllEmployeesOfOrganization());
        Employee employee = staff.searchEmployeeByPersonnelNumber(101);
        Employee createdEmployee = new Employee("Test", "Test", "Test", new GregorianCalendar(1959,10,1).getTime(),
                4190, 401190, employee.getCurrentPosition().getParentDivision().getPositionByName("Преподаватель"),
                190, "Доцент", "Кандидат наук", "test@domain.com",
                new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2018, 11, 1).getTime());

        //act
        Employee savedEmployee = this.databaseOperationService.saveEmployee(createdEmployee);

        //assert
        Assertions.assertNotNull(savedEmployee);
        Assertions.assertTrue(savedEmployee.getId() > 0);
    }

    @Test
    @DisplayName("Проверка работоспособности DatabaseOperationService.saveEmployee")
    public void TestThatDatabaseOperationServiceSaveEmployeeUpdatesExistingDBRecord() {
        //arrange
        Staff staff = new Staff(this.organization.getAllEmployeesOfOrganization());
        Employee employee = staff.searchEmployeeByPersonnelNumber(101);
        employee.setEmail("test@domain.com");

        //act
        Employee savedEmployee = this.databaseOperationService.saveEmployee(employee);

        //assert
        Assertions.assertNotNull(savedEmployee);
    }

    /*2. savePosition */
    @Test
    @DisplayName("Проверка работоспособности DatabaseOperationService.savePosition")
    public void TestThatDatabaseOperationServiceSavePositionCreatesNewDBRecord() {
        //arrange
        Division division = this.organization.getDivisionByCode("0101");
        Position createdPosition = new Position("Test", 10, 10, division, new LinkedHashSet<>());

        //act
        Position savedPosition = this.databaseOperationService.savePosition(createdPosition);

        //assert
        Assertions.assertNotNull(savedPosition);
        Assertions.assertTrue(savedPosition.getId() > 0);
    }

    @Test
    @DisplayName("Проверка работоспособности DatabaseOperationService.savePosition")
    public void TestThatDatabaseOperationServiceSavePositionUpdatesExistingDBRecord() {
        //arrange
        Division division = this.organization.getDivisionByCode("0101");
        Position position = division.getPositionByName("Преподаватель");
        position.setMaxEmployees(18);

        //act
        Position savedPosition = this.databaseOperationService.savePosition(position);

        //assert
        Assertions.assertNotNull(savedPosition);
    }

    /*3. saveOrder */
    @Test
    @DisplayName("Проверка работоспособности DatabaseOperationService.saveOrder")
    public void TestThatDatabaseOperationServiceSaveOrderCreatesNewDBRecord() {

        //arrange
        Staff staff = new Staff(this.organization.getAllEmployeesOfOrganization());
        Employee employee = staff.searchEmployeeByPersonnelNumber(101);

        Collection<OrderLine> orderLines = new LinkedHashSet<>();

        orderLines.add(new OrderLine(employee, employee.getCurrentPosition(), employee.getCurrentPosition().getParentDivision(), employee.getContractStartDate(), employee.getContractExpirationDate()));
        orderLines.add(new OrderLine(employee, employee.getCurrentPosition(), employee.getCurrentPosition().getParentDivision(), employee.getContractStartDate(), employee.getContractExpirationDate()));
        Order createdOrder = new Order(orderLines, "Прием на работу");


        //act
        Order savedOrder = this.databaseOperationService.saveOrder(createdOrder);
//        Collection<OrderLine> savedOrderLines = this.databaseOperationService.saveOrderLines(savedOrder.getOrderLines());

        //assert
        Assertions.assertNotNull(savedOrder);
        Assertions.assertTrue(savedOrder.getId() > 0);
        Assertions.assertNotNull(savedOrder.getOrderLines());
        for(OrderLine orderLine: savedOrder.getOrderLines())
            Assertions.assertTrue(orderLine.getId() > 0);
    }

    /*4. saveUser */
    @Test
    @DisplayName("Проверка работоспособности DatabaseOperationService.saveUser")
    public void TestThatDatabaseOperationServiceSaveUserCreatesNewDBRecord() {

        //arrange
        Staff staff = new Staff(this.organization.getAllEmployeesOfOrganization());
        Employee employee = staff.searchEmployeeByPersonnelNumber(207);

        User createdUser = new User(employee, "KABOIMOVA", "KABOIMOVA", "Сотрудник ОК");

        //act
        User savedUser = this.databaseOperationService.saveUser(createdUser);

        //assert
        Assertions.assertNotNull(savedUser);
        Assertions.assertTrue(savedUser.getId() > 0);
    }

    @Test
    @DisplayName("Проверка работоспособности DatabaseOperationService.saveUser")
    public void TestThatDatabaseOperationServiceSaveUserUpdatesExistingDBRecord() {

        //arrange
        String newPassword = "TEST";
        User user = this.databaseOperationService.findUserByLogin("NKOLESNIKOVA");
        Assertions.assertNotNull(user);
        user.setPassword(newPassword);

        //act
        User savedUser = this.databaseOperationService.saveUser(user);

        //assert
        Assertions.assertNotNull(savedUser);
        Assertions.assertTrue(newPassword.equals(savedUser.getPassword()));
    }

    /*********************************************************** Тесты методов удаления ************************/

    /*1. deletePosition */
    @Test
    @DisplayName("Проверка работоспособности DatabaseOperationService.deletePosition. Нельзя удалить Должности, на которые ссылаются Сотрудники")
    public void TestThatDatabaseOperationServiceDeletePositionCannotDeletePositionIfThereAreReferencesToIt() {
        //arrange
        Position positionToDelete = this.organization.getDivisionByCode("0101").getPositionByName("Преподаватель");

        //act
        Assertions.assertThrows(RuntimeException.class, ()-> {
            this.databaseOperationService.deletePosition(positionToDelete);
        });

        //assert
     //   Assertions.assertNotNull(savedUser);
     //   Assertions.assertTrue(newPassword.equals(savedUser.getPassword()));
    }

    @Test
    @DisplayName("Проверка работоспособности DatabaseOperationService.deletePosition. Можно удалить Должности, на которые не ссылаются Сотрудники")
    public void TestThatDatabaseOperationServiceDeletePositionDeletesRecord() {
        //arrange
        Division division = this.organization.getDivisionByCode("0101");
        Position createdPosition = new Position("Test", 10, 10, division, new LinkedHashSet<>());
        Position savedPosition = this.databaseOperationService.savePosition(createdPosition);
        Assertions.assertNotNull(savedPosition);
        Assertions.assertTrue(savedPosition.getId() > 0);

        //act
        this.databaseOperationService.deletePosition(savedPosition);
    }

    /*1. deleteUser */
    @Test
    @DisplayName("Проверка работоспособности DatabaseOperationService.deleteUser.")
    public void TestThatDatabaseOperationServiceDeleteUserDeletesRecord() {
        //arrange
        User userToDelete = this.databaseOperationService.findUserByLogin("NKOLESNIKOVA");

        //act
        this.databaseOperationService.deleteUser(userToDelete);
    }
}
