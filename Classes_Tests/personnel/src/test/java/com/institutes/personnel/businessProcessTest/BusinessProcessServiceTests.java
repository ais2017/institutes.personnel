package com.institutes.personnel.businessProcessTest;

import com.institutes.personnel.model.*;
import com.institutes.personnel.repository.*;
import com.institutes.personnel.repository.impl.*;
import com.institutes.personnel.service.AuthorizationService;
import com.institutes.personnel.service.BusinessProcessService;
import com.institutes.personnel.service.DatabaseOperationService;
import com.institutes.personnel.service.UserInteractionService;
import com.institutes.personnel.service.classes.Credentials;
import com.institutes.personnel.service.impl.BusinessProcessServiceBean;
import com.institutes.personnel.service.impl.DatabaseOperationServiceBean;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.*;

import static org.mockito.Mockito.*;

//When using this mode, a new test instance will be created for each test method or test factory method.
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class BusinessProcessServiceTests {

    /* Заглушки */
/*    private AuthorizationService authorizationService;
    private DivisionRepository divisionRepository;
    private EmployeeRepository employeeRepository;
    private OrderRepository orderRepository;
    private OrganizationRepository organizationRepository;
    private PositionRepository positionRepository;
    private StaffRepository staffRepository;
    private UserRepository userRepository;*/
    private UserInteractionService userInteractionService;
    private DatabaseOperationService databaseOperationService;

    private BusinessProcessService businessProcessService;

    /* Этих классов должно быть достаточно для инициализации всех объектов системы */
    private Organization organization;
    private Staff staff;
    private ArrayList<Order> orders = new ArrayList<Order>();
    private ArrayList<User> users = new ArrayList<User>();

    /* Перед тестом каждого бизнес-процесса необходимо инициализировать систему, заполнить тестовыми данными
       с которыми и будем работать*/
    @BeforeEach
    void init() {

        /* Персонал */
//            this.staff = new Staff();

        /* Создадим организацию из трех дивизий */
            //Для Division1
            Collection<Position> positions1 = new ArrayList<Position>();
            Division division1 = new Division("0101", "Первое подразделение", positions1);

            //!Должно сопровождаться приказом!
            Collection<Employee> employees1_1 = new ArrayList<Employee>();  //Сотрудники для первой Должности первого Подразделения
            Collection<Employee> employees1_2 = new ArrayList<Employee>();  //Сотрудники для второй Должности первого Подразделения
            Collection<Employee> employees1_3 = new ArrayList<Employee>();  //Сотрудники для третьей Должности первого Подразделения
            Collection<Employee> employees1_4 = new ArrayList<Employee>();  //Сотрудники для четвертой Должности первого Подразделения
            Collection<Employee> employees1_5 = new ArrayList<Employee>();  //Сотрудники для пятой Должности первого Подразделения

            division1.addPosition(new Position("Преподаватель", 10, 10, division1, employees1_1));
            division1.addPosition(new Position("Декан", 1, 1, division1, employees1_2));
            division1.addPosition(new Position("Секретарь", 5, 5, division1, employees1_3));
            division1.addPosition(new Position("Руководитель ОК", 1, 1, division1, employees1_4));
            division1.addPosition(new Position("Сотрудник ОК", 1, 1, division1, employees1_5));

                //Сотрудники Должности "Преподаватель" первого подразделения
                division1.getPositionByName("Преподаватель").addEmployee(new Employee("Сергеев", "Владислав", "Сергеевич", new GregorianCalendar(1959,10,1).getTime(),
                    4101, 401101, division1.getPositionByName("Преподаватель"),
                    101, "Доцент", "Кандидат наук", "vsergeev@domain.com",
                    new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2018, 11, 1).getTime()));
                division1.getPositionByName("Преподаватель").addEmployee(new Employee("Иванов", "Иван", "Иванович", new GregorianCalendar(1969,10,1).getTime(),
                    4102, 401102, division1.getPositionByName("Преподаватель"),
                    102, "Доцент", "Кандидат наук", "iivanov@domain.com",
                    new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2018, 11, 20).getTime()));
                division1.getPositionByName("Преподаватель").addEmployee(new Employee("Сидоров", "Николай", "Григорьевич", new GregorianCalendar(1971,10,1).getTime(),
                    4103, 401103, division1.getPositionByName("Преподаватель"),
                    103, "Доцент", "Кандидат наук", "nsidorov@domain.com",
                    new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2031, 11, 20).getTime()));

                //Сотрудники Должности "Декан" первого подразделения
                division1.getPositionByName("Декан").addEmployee(new Employee("Соколова", "Алена", "Геннадьевна", new GregorianCalendar(1959,10,1).getTime(),
                    4104, 401104, division1.getPositionByName("Декан"),
                    104, "Доцент", "Доктор наук", "asokolova@domain.com",
                        new GregorianCalendar(2007,7,1).getTime(), new GregorianCalendar(2031, 11, 1).getTime()));

                //Сотрудники Должности "Секретарь" первого подразделения
                division1.getPositionByName("Секретарь").addEmployee(new Employee("Фролова", "Татьяна", "Валерьевна", new GregorianCalendar(1959,10,1).getTime(),
                    4105, 401105, division1.getPositionByName("Секретарь"),
                    105, "Отсутствует", "Отсутствует", "tfrolova@domain.com",
                    new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2031, 11, 1).getTime()));
                division1.getPositionByName("Секретарь").addEmployee(new Employee("Степанова", "Богдана", "Валерьевна", new GregorianCalendar(1959,10,1).getTime(),
                    4106, 401106, division1.getPositionByName("Секретарь"),
                    106, "Отсутствует", "Отсутствует", "tfrolova@domain.com",
                    new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2018, 11, 9).getTime()));
                division1.getPositionByName("Секретарь").addEmployee(new Employee("Аникеева", "Валерия", "Валерьевна", new GregorianCalendar(1959,10,1).getTime(),
                    4107, 401107, division1.getPositionByName("Секретарь"),
                    107, "Отсутствует", "Отсутствует", "vanikeeva@domain.com",
                    new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2018, 11, 19).getTime()));

                //Сотрудники Должности "Руководитель ОК" первого подразделения
                division1.getPositionByName("Руководитель ОК").addEmployee(new Employee("Колесникова", "Ника", "Семеновна", new GregorianCalendar(1959,10,1).getTime(),
                        4108, 401108, division1.getPositionByName("Руководитель ОК"),
                        108, "Отсутствует", "Отсутствует", "nkolesnikova@domain.com",
                        new GregorianCalendar(2007,7,1).getTime(), new GregorianCalendar(2031, 11, 1).getTime()));

                //Пользователь Руководитель ОК первого подразделения - со старым наследованием от сотрудника
                /*User user1_1 = new User(((ArrayList<Employee>) employees1_4).get(0).getLastName(), ((ArrayList<Employee>) employees1_4).get(0).getFirstName(), ((ArrayList<Employee>) employees1_4).get(0).getMiddleName(),
                        ((ArrayList<Employee>) employees1_4).get(0).getBirthday(), ((ArrayList<Employee>) employees1_4).get(0).getPassportSeries(), ((ArrayList<Employee>) employees1_4).get(0).getPassportNumber(),
                        ((ArrayList<Employee>) employees1_4).get(0).getCurrentPosition(), ((ArrayList<Employee>) employees1_4).get(0).getPersonnelNumber(), ((ArrayList<Employee>) employees1_4).get(0).getScientificTitle(),
                        ((ArrayList<Employee>) employees1_4).get(0).getScienceDegree(), ((ArrayList<Employee>) employees1_4).get(0).getEmail(), ((ArrayList<Employee>) employees1_4).get(0).getContractStartDate(),
                        ((ArrayList<Employee>) employees1_4).get(0).getContractExpirationDate(), "NKOLESNIKOVA", "NKOLESNIKOVA", "Руководитель ОК"); */

                //Пользователь Руководитель ОК первого подразделения
                User user1_1 = new User(((ArrayList<Employee>) employees1_4).get(0), "NKOLESNIKOVA", "NKOLESNIKOVA", "Руководитель ОК");

                users.add(user1_1);

                //Сотрудники Должности "Сотрудник ОК" первого подразделения
                division1.getPositionByName("Сотрудник ОК").addEmployee(new Employee("Филимонов", "Лука", "Геннадьевич", new GregorianCalendar(1959,10,1).getTime(),
                        4109, 401109, division1.getPositionByName("Сотрудник ОК"),
                        109, "Отсутствует", "Отсутствует", "lfilimonov@domain.com",
                        new GregorianCalendar(2007,7,1).getTime(), new GregorianCalendar(2031, 11, 1).getTime()));

                //Пользователь Сотрудник ОК первого подразделения - со старым наследованием от сотрудника
                /*User user1_2 = new User(((ArrayList<Employee>) employees1_5).get(0).getLastName(), ((ArrayList<Employee>) employees1_5).get(0).getFirstName(), ((ArrayList<Employee>) employees1_5).get(0).getMiddleName(),
                        ((ArrayList<Employee>) employees1_5).get(0).getBirthday(), ((ArrayList<Employee>) employees1_5).get(0).getPassportSeries(), ((ArrayList<Employee>) employees1_5).get(0).getPassportNumber(),
                        ((ArrayList<Employee>) employees1_5).get(0).getCurrentPosition(), ((ArrayList<Employee>) employees1_5).get(0).getPersonnelNumber(), ((ArrayList<Employee>) employees1_5).get(0).getScientificTitle(),
                        ((ArrayList<Employee>) employees1_5).get(0).getScienceDegree(), ((ArrayList<Employee>) employees1_5).get(0).getEmail(), ((ArrayList<Employee>) employees1_5).get(0).getContractStartDate(),
                        ((ArrayList<Employee>) employees1_5).get(0).getContractExpirationDate(), "LFILIMONOV", "LFILIMONOV", "Сотрудник ОК"); */

                //Пользователь Сотрудник ОК первого подразделения
                User user1_2 = new User(((ArrayList<Employee>) employees1_5).get(0), "LFILIMONOV", "LFILIMONOV", "Сотрудник ОК");

                users.add(user1_2);

                //Приказ о приеме на работу 1
                Collection<OrderLine> orderLines1 = new LinkedHashSet<OrderLine>();
                for(Position position: division1.getPositions()) {
                    for(Employee employee: position.getEmployees()) {
                        orderLines1.add(new OrderLine(employee, position, division1, employee.getContractStartDate(), employee.getContractExpirationDate()));
                    }
                }

                Order order1 = new Order(orderLines1, "Прием на работу");

                this.orders.add(order1);

            //Для Division2
            Collection<Position> positions2 = new ArrayList<Position>();
            Division division2 = new Division("0102", "Второе подразделение", positions2);

            //!Должно сопровождаться приказом!
            Collection<Employee> employees2_1 = new ArrayList<Employee>();  //Сотрудники для первой Должности второго Подразделения
            Collection<Employee> employees2_2 = new ArrayList<Employee>();  //Сотрудники для второй Должности второго Подразделения

            division2.addPosition(new Position("Преподаватель", 10, 10, division2, employees2_1));
            division2.addPosition(new Position("Сотрудник ОК", 3, 3, division2, employees2_2));

                //Сотрудники Должности "Преподаватель" второго подразделения
                division2.getPositionByName("Преподаватель").addEmployee(new Employee("Владиславов", "Владислав", "Сергеевич", new GregorianCalendar(1959,10,1).getTime(),
                        4201, 402101, division2.getPositionByName("Преподаватель"),
                        201, "Доцент", "Кандидат наук", "vvladislavov@domain.com",
                        new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2018, 11, 1).getTime()));
                division2.getPositionByName("Преподаватель").addEmployee(new Employee("Березкин", "Иван", "Иванович", new GregorianCalendar(1969,10,1).getTime(),
                        4202, 402102, division2.getPositionByName("Преподаватель"),
                        202, "Доцент", "Кандидат наук", "iberezkin@domain.com",
                        new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2018, 11, 20).getTime()));
                division2.getPositionByName("Преподаватель").addEmployee(new Employee("Липов", "Николай", "Григорьевич", new GregorianCalendar(1971,10,1).getTime(),
                        4203, 402103, division2.getPositionByName("Преподаватель"),
                        203, "Доцент", "Кандидат наук", "nlipov@domain.com",
                        new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2031, 11, 20).getTime()));
                division2.getPositionByName("Преподаватель").addEmployee(new Employee("Пихтов", "Антон", "Олегович", new GregorianCalendar(1969,10,1).getTime(),
                        4204, 402104, division2.getPositionByName("Преподаватель"),
                        204, "Доцент", "Кандидат наук", "apihtov@domain.com",
                        new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2018, 11, 20).getTime()));
                division2.getPositionByName("Преподаватель").addEmployee(new Employee("Облепихин", "Николай", "Григорьевич", new GregorianCalendar(1971,10,1).getTime(),
                        4205, 402105, division2.getPositionByName("Преподаватель"),
                        205, "Доцент", "Кандидат наук", "noblepihin@domain.com",
                        new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2031, 11, 20).getTime()));

                //Сотрудники Должности "Сотрудник ОК" второго подразделения
                division2.getPositionByName("Сотрудник ОК").addEmployee(new Employee("Ибрагимова", "Агафья", "Всеволодовна", new GregorianCalendar(1959,10,1).getTime(),
                        4206, 402106, division2.getPositionByName("Сотрудник ОК"),
                        206, "Отсутствует", "Отсутствует", "aibragimova@domain.com",
                        new GregorianCalendar(2007,7,1).getTime(), new GregorianCalendar(2031, 11, 1).getTime()));

                division2.getPositionByName("Сотрудник ОК").addEmployee(new Employee("Абоймова", "Кира", "Александровна", new GregorianCalendar(1959,10,1).getTime(),
                        4207, 402107, division2.getPositionByName("Сотрудник ОК"),
                        207, "Отсутствует", "Отсутствует", "kaboimova@domain.com",
                        new GregorianCalendar(2007,7,1).getTime(), new GregorianCalendar(2031, 11, 1).getTime()));

                //Пользователь Сотрудник ОК второго подразделения - со старым наследованием от сотрудника
                /*User user2_1 = new User(((ArrayList<Employee>) employees2_2).get(0).getLastName(), ((ArrayList<Employee>) employees2_2).get(0).getFirstName(), ((ArrayList<Employee>) employees2_2).get(0).getMiddleName(),
                        ((ArrayList<Employee>) employees2_2).get(0).getBirthday(), ((ArrayList<Employee>) employees2_2).get(0).getPassportSeries(), ((ArrayList<Employee>) employees2_2).get(0).getPassportNumber(),
                        ((ArrayList<Employee>) employees2_2).get(0).getCurrentPosition(), ((ArrayList<Employee>) employees2_2).get(0).getPersonnelNumber(), ((ArrayList<Employee>) employees2_2).get(0).getScientificTitle(),
                        ((ArrayList<Employee>) employees2_2).get(0).getScienceDegree(), ((ArrayList<Employee>) employees2_2).get(0).getEmail(), ((ArrayList<Employee>) employees2_2).get(0).getContractStartDate(),
                        ((ArrayList<Employee>) employees2_2).get(0).getContractExpirationDate(), "AIBRAGIMOVA", "AIBRAGIMOVA", "Сотрудник ОК"); */

                //Пользователь Сотрудник ОК второго подразделения
                User user2_1 = new User(((ArrayList<Employee>) employees2_2).get(0), "AIBRAGIMOVA", "AIBRAGIMOVA", "Сотрудник ОК");

                users.add(user2_1);

                //Приказ о приеме на работу 2
                Collection<OrderLine> orderLines2 = new LinkedHashSet<OrderLine>();
                for(Position position: division2.getPositions()) {
                    for(Employee employee: position.getEmployees()) {
                        orderLines2.add(new OrderLine(employee, position, division2, employee.getContractStartDate(), employee.getContractExpirationDate()));
                    }
                }

                Order order2 = new Order(orderLines2, "Прием на работу");

                this.orders.add(order2);

            //Для Division3
            Collection<Position> positions3 = new ArrayList<Position>();
            Division division3 = new Division("0103", "Третье подразделение", positions3);

            //!Должно сопровождаться приказом!
            Collection<Employee> employees3_1 = new ArrayList<Employee>();  //Сотрудники для первой Должности третьего Подразделения
            Collection<Employee> employees3_2 = new ArrayList<Employee>();  //Сотрудники для второй Должности третьего Подразделения
            Collection<Employee> employees3_3 = new ArrayList<Employee>();  //Сотрудники для третьей Должности третьего Подразделения

            division3.addPosition(new Position("Преподаватель", 15, 15, division3, employees3_1));
            division3.addPosition(new Position("Администратор", 3, 3, division3, employees3_2));
            division3.addPosition(new Position("Сотрудник ОК", 1, 1, division3, employees3_3));

                //Сотрудники Должности "Преподаватель" третьего подразделения
                division3.getPositionByName("Преподаватель").addEmployee(new Employee("Новичкова", "Ольга", "Никитевна", new GregorianCalendar(1959,10,1).getTime(),
                        4301, 403101, division3.getPositionByName("Преподаватель"),
                        301, "Доцент", "Кандидат наук", "onovichkova@domain.com",
                        new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2018, 11, 1).getTime()));
                division3.getPositionByName("Преподаватель").addEmployee(new Employee("Лобов", "Август", "Геннадьевич", new GregorianCalendar(1969,10,1).getTime(),
                        4302, 403102, division3.getPositionByName("Преподаватель"),
                        302, "Доцент", "Кандидат наук", "alobov@domain.com",
                        new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2018, 11, 20).getTime()));

                //Сотрудники Должности "Администратор" третьего подразделения
                division3.getPositionByName("Администратор").addEmployee(new Employee("Черных", "Гавриил", "Макарович", new GregorianCalendar(1959,10,1).getTime(),
                        4303, 403103, division3.getPositionByName("Администратор"),
                        303, "Отсутствует", "Отсутствует", "gchernykh@domain.com",
                        new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2038, 11, 1).getTime()));

                //Сотрудники Должности "Сотрудник ОК" третьего подразделения
                division3.getPositionByName("Сотрудник ОК").addEmployee(new Employee("Кураев", "Ярослав", "Давыдович", new GregorianCalendar(1959,10,1).getTime(),
                        4304, 403104, division3.getPositionByName("Сотрудник ОК"),
                        304, "Отсутствует", "Отсутствует", "ykuraev@domain.com",
                        new GregorianCalendar(2007,7,1).getTime(), new GregorianCalendar(2031, 11, 1).getTime()));

                //Пользователь Сотрудник ОК третьего подразделения - со старым наследованием от сотрудника
                /*User user3_1 = new User(((ArrayList<Employee>) employees3_3).get(0).getLastName(), ((ArrayList<Employee>) employees3_3).get(0).getFirstName(), ((ArrayList<Employee>) employees3_3).get(0).getMiddleName(),
                        ((ArrayList<Employee>) employees3_3).get(0).getBirthday(), ((ArrayList<Employee>) employees3_3).get(0).getPassportSeries(), ((ArrayList<Employee>) employees3_3).get(0).getPassportNumber(),
                        ((ArrayList<Employee>) employees3_3).get(0).getCurrentPosition(), ((ArrayList<Employee>) employees3_3).get(0).getPersonnelNumber(), ((ArrayList<Employee>) employees3_3).get(0).getScientificTitle(),
                        ((ArrayList<Employee>) employees3_3).get(0).getScienceDegree(), ((ArrayList<Employee>) employees3_3).get(0).getEmail(), ((ArrayList<Employee>) employees3_3).get(0).getContractStartDate(),
                        ((ArrayList<Employee>) employees3_3).get(0).getContractExpirationDate(), "YKURAEV", "YKURAEV", "Сотрудник ОК"); */

                //Пользователь Сотрудник ОК третьего подразделения
                User user3_1 = new User(((ArrayList<Employee>) employees3_3).get(0), "YKURAEV", "YKURAEV", "Сотрудник ОК");

                users.add(user3_1);

                //Пользователь Администратор третьего подразделения
                User user3_2 = new User(((ArrayList<Employee>) employees3_2).get(0), "GCHERNYKH", "GCHERNYKH", "Администратор");

                users.add(user3_2);

                //Приказ о приеме на работу 3
                Collection<OrderLine> orderLines3 = new LinkedHashSet<OrderLine>();
                for(Position position: division3.getPositions()) {
                    for(Employee employee: position.getEmployees()) {
                        orderLines3.add(new OrderLine(employee, position, division3, employee.getContractStartDate(), employee.getContractExpirationDate()));
                    }
                }

                Order order3 = new Order(orderLines3, "Прием на работу");

                this.orders.add(order3);

            //Объединение дивизий
            LinkedHashSet<Division> divisions = new LinkedHashSet<Division>();
            divisions.add(division1);
            divisions.add(division2);
            divisions.add(division3);

        /*  */

        //Организация
        this.organization = new Organization(divisions);

        //Персонал - все сотрудники всех подразделений
/*        for(Division division: this.organization.getDivisions()) {
            for(Position position: division.getPositions()) {
                for(Employee employee: position.getEmployees()) {
                    this.staff.addEmployee(employee);
                }
            }
        } */
        this.staff = new Staff(this.organization.getAllEmployeesOfOrganization());

        /* Настраиваем заглушки */
     //   authorizationService = mock(AuthorizationService.class);
        databaseOperationService = mock(DatabaseOperationService.class);
        //userInteractionService = spy(UserInteractionService.class);
        //businessProcessService = spy(BusinessProcessService.class);

        //Проверка паролей
        when(databaseOperationService.checkCredentials("NKOLESNIKOVA", "NKOLESNIKOVA")).thenReturn(true);
        when(databaseOperationService.checkCredentials("LFILIMONOV", "LFILIMONOV")).thenReturn(true);
        when(databaseOperationService.checkCredentials("AIBRAGIMOVA", "AIBRAGIMOVA")).thenReturn(true);
        when(databaseOperationService.checkCredentials("YKURAEV", "YKURAEV")).thenReturn(true);
        when(databaseOperationService.checkCredentials("GCHERNYKH", "GCHERNYKH")).thenReturn(true);

        //Поиск пользователя по логину
     //   userRepository = spy(UserRepository.class);
        when(databaseOperationService.findUserByLogin("NKOLESNIKOVA")).thenReturn(users.get(0));
        when(databaseOperationService.findUserByLogin("LFILIMONOV")).thenReturn(users.get(1));
        when(databaseOperationService.findUserByLogin("AIBRAGIMOVA")).thenReturn(users.get(2));
        when(databaseOperationService.findUserByLogin("YKURAEV")).thenReturn(users.get(3));
        when(databaseOperationService.findUserByLogin("GCHERNYKH")).thenReturn(users.get(4));

/*        //Хранилище сотрудников
        employeeRepository = new EmployeeRepositoryService();

        //Хранилище приказов
        orderRepository = new OrderRepositoryService();

        divisionRepository = new DivisionRepositoryService();
        organizationRepository = new OrganizationRepositoryService();
        positionRepository = new PositionRepositoryService();
        staffRepository = new StaffRepositoryService();*/

        //Авторизация
        /*when(businessProcessService.authorization("NKOLESNIKOVA", "NKOLESNIKOVA")).thenReturn(user1_1);
        when(businessProcessService.authorization("LFILIMONOV", "LFILIMONOV")).thenReturn(user1_2);
        when(businessProcessService.authorization("AIBRAGIMOVA", "AIBRAGIMOVA")).thenReturn(user2_1);
        when(businessProcessService.authorization("YKURAEV", "YKURAEV")).thenReturn(user3_1); */

    }

    @Test
    @DisplayName("Тест: Проверка корректности работы заглушек(проверка логина/пароля)")
    public void TestCredentialsMock() {
        Assertions.assertTrue(databaseOperationService.checkCredentials("NKOLESNIKOVA", "NKOLESNIKOVA"));
        Assertions.assertTrue(databaseOperationService.checkCredentials("LFILIMONOV", "LFILIMONOV"));
        Assertions.assertTrue(databaseOperationService.checkCredentials("AIBRAGIMOVA", "AIBRAGIMOVA"));
        Assertions.assertTrue(databaseOperationService.checkCredentials("YKURAEV", "YKURAEV"));
        Assertions.assertTrue(databaseOperationService.checkCredentials("GCHERNYKH", "GCHERNYKH"));
        Assertions.assertFalse(databaseOperationService.checkCredentials("NKOLESNIKOVA1", "NKOLESNIKOVA1"));
    }

    @Test
    @DisplayName("Тест: Проверка корректности работы заглушек(поиск пользователя по логину)")
    public void TestUserRepositoryMock() {
        Assertions.assertEquals(users.get(0), databaseOperationService.findUserByLogin("NKOLESNIKOVA"));
        Assertions.assertEquals(users.get(1), databaseOperationService.findUserByLogin("LFILIMONOV"));
        Assertions.assertEquals(users.get(2), databaseOperationService.findUserByLogin("AIBRAGIMOVA"));
        Assertions.assertEquals(users.get(3), databaseOperationService.findUserByLogin("YKURAEV"));
        Assertions.assertEquals(users.get(4), databaseOperationService.findUserByLogin("GCHERNYKH"));
        Assertions.assertNull(databaseOperationService.findUserByLogin("AAA"));
    }

    /*************************************************************************************** Тесты авторизации *****************************************************************************/
    @Test
    @DisplayName("Тест: Бизнес-процесс Авторизация пользователя в системе. " +
            "При авторизации пользователя NKOLESNIKOVA/NKOLESNIKOVA должны успешно авторизоваться.")
    public void TestAuthorizationBusinessProcessNKOLESNIKOVA() {

        User user = null;
        String login = "NKOLESNIKOVA";
        String password = "NKOLESNIKOVA";

        /* Настройка заглушек */
        userInteractionService = mock(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));

        /* Создание экземпляра сервиса Бизнес-процессов */
        /* businessProcessService = new BusinessProcessServiceBean(this.organization, this.staff, this.orders, this.users, this.authorizationService, this.userInteractionService,
                this.userRepository, this.employeeRepository, this.orderRepository); */
        /* businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.authorizationService,
                this.userRepository, this.employeeRepository, this.orderRepository, this.positionRepository,
                this.divisionRepository, this.organizationRepository, this.staffRepository); */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        //act
        user = businessProcessService.authorization();

        //assert
        Assertions.assertNotNull(user);
        Assertions.assertEquals(4108, user.getEmployee().getPassportSeries());
        Assertions.assertEquals(401108, user.getEmployee().getPassportNumber());
        Assertions.assertEquals(108, user.getEmployee().getPersonnelNumber());
        Assertions.assertEquals(user, users.get(0));
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Авторизация пользователя в системе. " +
            "При авторизации пользователя AAA/AAA авторизация неуспешна.")
    public void TestAuthorizationBusinessProcessAAAReturnsUserNull() {
        User user = null;
        String login = "AAA";
        String password = "AAA";

        /* Настройка заглушек */
        userInteractionService = mock(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));

        /* Создание экземпляра сервиса Бизнес-процессов */
        /* businessProcessService = new BusinessProcessServiceBean(this.organization, this.staff, this.orders, this.users, this.authorizationService, this.userInteractionService,
                this.userRepository, this.employeeRepository, this.orderRepository); */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        //act
        user = businessProcessService.authorization();

        //assert
        Assertions.assertNull(user);
    }

    /*************************************************************************************** Тесты Изменение данных о существующем сотруднике ********************************************************/
    @Test
    @DisplayName("Тест: Бизнес-процесс Изменение данных о существующих сотрудниках. " +
            "Попытка вызвать метод с пользовательской ролью, отличной от Сотрудник ОК, вернет null.")
    public void TestChangeEmployeeData_WithIncorrectUserRole_ReturnsNull() {
        //Только пользователь с ролью Сотрудник ОК может изменять данные о сотруднике

        //NKOLESNIKOVA - Руководитель ОК Первого Подразделения. Пользовательская роль Руководитель ОК.
        //Будем менять преподавателя первого подразделения "Сидоров", "Николай", "Григорьевич"
        User user = null;
        String login = "NKOLESNIKOVA";
        String password = "NKOLESNIKOVA";
        user = databaseOperationService.findUserByLogin(login);
        Employee employeeToChange = staff.searchEmployeeByPassportData(4103, 401103); //Сидоров Николай Григорьевич, преподаватель первого подразделения
        Employee changedEmployee = new Employee(employeeToChange.getLastName(), employeeToChange.getFirstName(), employeeToChange.getMiddleName(), employeeToChange.getBirthday(),
                employeeToChange.getPassportSeries(), employeeToChange.getPassportNumber(), employeeToChange.getCurrentPosition(), 150,
                employeeToChange.getScientificTitle(), employeeToChange.getScienceDegree(), employeeToChange.getEmail(),
                employeeToChange.getContractStartDate(), employeeToChange.getContractExpirationDate());   //Изменены только персональные данные Сидорова (табельный номер)

        //Запоминаем текущую Должность
        Position oldPosition = employeeToChange.getCurrentPosition();

        //Проверка, что найденный сотрудник - тот, которого рассчитывали вытащить.
        Assertions.assertNotNull(employeeToChange);
        Assertions.assertEquals("Сидоров", employeeToChange.getLastName());
        Assertions.assertEquals("Николай", employeeToChange.getFirstName());
        Assertions.assertEquals("Григорьевич", employeeToChange.getMiddleName());
        Assertions.assertEquals(103, employeeToChange.getPersonnelNumber());

        /* Настройка заглушек */

        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getEmployeeToChange()).thenReturn(employeeToChange);                        //Сидоров Николай Григорьевич, преподаватель первого подразделения
        when(userInteractionService.getChangedEmployee(employeeToChange)).thenReturn(changedEmployee);          //Сидоров Николай Григорьевич, преподаватель первого подразделения, изменен только табельный номер

        /* Создание экземпляра сервиса Бизнес-процессов */
        /* businessProcessService = new BusinessProcessServiceBean(this.organization, this.staff, this.orders, this.users, this.authorizationService, this.userInteractionService,
                this.userRepository, this.employeeRepository, this.orderRepository); */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        Employee employee = businessProcessService.changeEmployeeData(user);            //Должно вернуть Сидорова с измененным табельным номером

        /** assert **/
        Assertions.assertNull(employee);
        Assertions.assertEquals(3, this.orders.size());
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Изменение данных о существующих сотрудниках. " +
            "Изменение данных о сотруднике с подразделением, отличным от подразделения" +
            "пользователя с ролью Сотрудник ОК, вернет null.")
    public void TestChangeEmployeeData_WithAnotherDivision_ReturnsNull() {
        //Только пользователь с ролью Сотрудник ОК может изменять данные о сотруднике

        //AIBRAGIMOVA - Сотрудник ОК Второго Подразделения.
        //Будем менять преподавателя первого подразделения "Сидоров", "Николай", "Григорьевич" - получим null
     //   User user = null;
        String login = "AIBRAGIMOVA";
        String password = "AIBRAGIMOVA";
        final User user = databaseOperationService.findUserByLogin(login);
        Employee employeeToChange = staff.searchEmployeeByPassportData(4103, 401103); //Сидоров Николай Григорьевич, преподаватель первого подразделения
        Employee changedEmployee = new Employee(employeeToChange.getLastName(), employeeToChange.getFirstName(), employeeToChange.getMiddleName(), employeeToChange.getBirthday(),
                employeeToChange.getPassportSeries(), employeeToChange.getPassportNumber(), employeeToChange.getCurrentPosition(), 150,
                employeeToChange.getScientificTitle(), employeeToChange.getScienceDegree(), employeeToChange.getEmail(),
                employeeToChange.getContractStartDate(), employeeToChange.getContractExpirationDate());   //Изменены только персональные данные Сидорова (табельный номер)

        //Запоминаем текущую Должность
        Position oldPosition = employeeToChange.getCurrentPosition();

        //Проверка, что найденный сотрудник - тот, которого рассчитывали вытащить.
        Assertions.assertNotNull(employeeToChange);
        Assertions.assertEquals("Сидоров", employeeToChange.getLastName());
        Assertions.assertEquals("Николай", employeeToChange.getFirstName());
        Assertions.assertEquals("Григорьевич", employeeToChange.getMiddleName());
        Assertions.assertEquals(103, employeeToChange.getPersonnelNumber());

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getEmployeeToChange()).thenReturn(employeeToChange);                        //Сидоров Николай Григорьевич, преподаватель первого подразделения
        when(userInteractionService.getChangedEmployee(employeeToChange)).thenReturn(changedEmployee);          //Сидоров Николай Григорьевич, преподаватель первого подразделения, изменен только табельный номер

        /* Создание экземпляра сервиса Бизнес-процессов */
        /* businessProcessService = new BusinessProcessServiceBean(this.organization, this.staff, this.orders, this.users, this.authorizationService, this.userInteractionService,
                this.userRepository, this.employeeRepository, this.orderRepository); */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Employee employee = businessProcessService.changeEmployeeData(user);
        });
    }

    @Disabled
    @Test
    @DisplayName("Тест: Бизнес-процесс Изменение данных о существующих сотрудниках. " +
            "Изменение данных о сотруднике с тем же подразделением, что и у пользователя с ролью Сотрудник ОК. " +
            "Изменения только личных данных, подразделение не меняется. " +
            "" +
            "Пользователь, под которым будет выполняться изменение данных - Сотрудник ОК Первого подразделения " +
            "LFILIMONOV. " +
            "Сотрудник, данные которого будут изменены - преподаватель Первого подразделения " +
            "Сидоров Николай Григорьевич, паспорт 4103 401103. " +
            "В ходе теста меняется только табельный номер. ")
    public void TestChangeEmployeeData_FromUserHREmployee_WithSameDivision_ChangeOnlyPersonnelNumber_SuccessfulCompletion_WithoutNewOrders() {
        //Только пользователь с ролью Сотрудник ОК может изменять данные о сотруднике

        //LFILIMONOV - Сотрудник ОК Первого Подразделения.
        //Будем менять преподавателя первого подразделения "Сидоров", "Николай", "Григорьевич"
        User user = null;
        String login = "LFILIMONOV";
        String password = "LFILIMONOV";
        user = databaseOperationService.findUserByLogin(login);
        Employee employeeToChange = staff.searchEmployeeByPassportData(4103, 401103); //Сидоров Николай Григорьевич, преподаватель первого подразделения
        Employee changedEmployee = new Employee(employeeToChange.getLastName(), employeeToChange.getFirstName(), employeeToChange.getMiddleName(), employeeToChange.getBirthday(),
                employeeToChange.getPassportSeries(), employeeToChange.getPassportNumber(), employeeToChange.getCurrentPosition(), 150,
                employeeToChange.getScientificTitle(), employeeToChange.getScienceDegree(), employeeToChange.getEmail(),
                employeeToChange.getContractStartDate(), employeeToChange.getContractExpirationDate());   //Изменены только персональные данные Сидорова (табельный номер)

        //Запоминаем текущую Должность
        Position oldPosition = employeeToChange.getCurrentPosition();

        //Проверка, что найденный сотрудник - тот, которого рассчитывали вытащить.
        Assertions.assertNotNull(employeeToChange);
        Assertions.assertEquals("Сидоров", employeeToChange.getLastName());
        Assertions.assertEquals("Николай", employeeToChange.getFirstName());
        Assertions.assertEquals("Григорьевич", employeeToChange.getMiddleName());
        Assertions.assertEquals(103, employeeToChange.getPersonnelNumber());

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getEmployeeToChange()).thenReturn(employeeToChange);                        //Сидоров Николай Григорьевич, преподаватель первого подразделения
        when(userInteractionService.getChangedEmployee(employeeToChange)).thenReturn(changedEmployee);          //Сидоров Николай Григорьевич, преподаватель первого подразделения, изменен только табельный номер

        /* Создание экземпляра сервиса Бизнес-процессов */
        /* businessProcessService = new BusinessProcessServiceBean(this.organization, this.staff, this.orders, this.users, this.authorizationService, this.userInteractionService,
                this.userRepository, this.employeeRepository, this.orderRepository); */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        Employee employee = businessProcessService.changeEmployeeData(user);            //Должно вернуть Сидорова с измененным табельным номером

        /** assert **/
        Assertions.assertNotNull(employee);
        Assertions.assertEquals("Сидоров", employee.getLastName());
        Assertions.assertEquals("Николай", employee.getFirstName());
        Assertions.assertEquals("Григорьевич", employee.getMiddleName());
        Assertions.assertEquals(4103, employee.getPassportSeries());
        Assertions.assertEquals(401103, employee.getPassportNumber());
        Assertions.assertEquals(150, employee.getPersonnelNumber());

        //Проверяем, что в рамках теста не было создано приказов - меняли только табельный номер
        Assertions.assertEquals(3, this.orders.size());

        //Проверяем, что старый экземпляр Сотрудника не содержится в Должности
        Assertions.assertFalse(oldPosition.getEmployees().contains(employeeToChange));

        //Проверяем, что новый экземпляр Сотрудника содержится в Должности
        Assertions.assertTrue(oldPosition.getEmployees().contains(employee));
    }

    @Disabled
    @Test
    @DisplayName("Тест: Бизнес-процесс Изменение данных о существующих сотрудниках. " +
            "Если в результате изменения данных меняется Должность сотрудника на null, необходимо выполнить ряд действий: " +
            "Убрать старый экземпляр сотрудника из прежней Должности, создать Приказ Увольнение.")
    public void TestChangeEmployeeData_ChangeEmployeesPositionToNull_LeadsToOrderCreation() {
        //Только пользователь с ролью Сотрудник ОК может изменять данные о сотруднике

        //Будем менять преподавателя первого подразделения "Сидоров", "Николай", "Григорьевич"
        //Изменим его Должность; С Преподавателя Первого подразделения на null
        User user = null;
        String login = "LFILIMONOV";                    //LFILIMONOV - Сотрудник ОК Первого Подразделения.
        String password = "LFILIMONOV";
        user = databaseOperationService.findUserByLogin(login);

        Employee employeeToChange = staff.searchEmployeeByPassportData(4103, 401103); //Сидоров Николай Григорьевич, преподаватель первого подразделения
        Employee changedEmployee = new Employee(employeeToChange.getLastName(), employeeToChange.getFirstName(), employeeToChange.getMiddleName(), employeeToChange.getBirthday(),
                employeeToChange.getPassportSeries(), employeeToChange.getPassportNumber(), employeeToChange.getCurrentPosition(), employeeToChange.getPersonnelNumber(),
                employeeToChange.getScientificTitle(), employeeToChange.getScienceDegree(), employeeToChange.getEmail(),
                employeeToChange.getContractStartDate(), employeeToChange.getContractExpirationDate());
        changedEmployee.setCurrentPosition(null);   //Делаем Должность null

        //Запоминаем текущую Должность
        Position oldPosition = employeeToChange.getCurrentPosition();

        //Проверка, что найденный сотрудник - тот, которого рассчитывали вытащить.
        Assertions.assertNotNull(employeeToChange);
        Assertions.assertEquals("Сидоров", employeeToChange.getLastName());
        Assertions.assertEquals("Николай", employeeToChange.getFirstName());
        Assertions.assertEquals("Григорьевич", employeeToChange.getMiddleName());
        Assertions.assertEquals(103, employeeToChange.getPersonnelNumber());
        Assertions.assertEquals("0101", employeeToChange.getCurrentPosition().getParentDivision().getDivisionCode());

        /* Настройка заглушек */
        // employeeRepository = mock(EmployeeRepository.class);
        when(databaseOperationService.saveEmployee(changedEmployee)).thenReturn(changedEmployee);

        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getEmployeeToChange()).thenReturn(employeeToChange);                        //Сидоров Николай Григорьевич, преподаватель первого подразделения
        when(userInteractionService.getChangedEmployee(employeeToChange)).thenReturn(changedEmployee);          //Сидоров Николай Григорьевич, преподаватель второго подразделения

        /* Создание экземпляра сервиса Бизнес-процессов */
        /* businessProcessService = new BusinessProcessServiceBean(this.organization, this.staff, this.orders, this.users, this.authorizationService, this.userInteractionService,
                this.userRepository, this.employeeRepository, this.orderRepository); */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        Employee employee = businessProcessService.changeEmployeeData(user);            //Должно вернуть Сидорова с измененным Подразделением

        /** assert **/
        Assertions.assertNotNull(employee);
        Assertions.assertEquals("Сидоров", employee.getLastName());
        Assertions.assertEquals("Николай", employee.getFirstName());
        Assertions.assertEquals("Григорьевич", employee.getMiddleName());
        Assertions.assertEquals(4103, employee.getPassportSeries());
        Assertions.assertEquals(401103, employee.getPassportNumber());
        Assertions.assertEquals(103, employee.getPersonnelNumber());
        Assertions.assertNull(employee.getCurrentPosition());

        //Проверяем, что в рамках теста был создано приказ - Увольнение
     //   Assertions.assertEquals(4, this.orders.size());
     //   Assertions.assertEquals("Увольнение", this.orders.get(3).getType());

        //Проверяем, что бизнес-процесс сохранял Сотрудника, которого вернул Пользователь
        verify(databaseOperationService, times(1)).saveEmployee(changedEmployee);

        //Проверяем, что старый экземпляр Сотрудника не содержится в oldPosition
        Assertions.assertFalse(oldPosition.getEmployees().contains(employeeToChange));

        //Проверяем, что новый экземпляр Сотрудника не содержится в oldPosition
        Assertions.assertFalse(oldPosition.getEmployees().contains(employee));
    }

    @Disabled
    @Test
    @DisplayName("Тест: Бизнес-процесс Изменение данных о существующих сотрудниках. " +
            "Если в результате изменения данных меняется Должность сотрудника, необходимо выполнить ряд действий: " +
            "Убрать старый экземпляр сотрудника из прежней Должности, добавить новый экземпляр сотрудника в новую Должность, " +
            "создать Приказ о переводе.")
    public void TestChangeEmployeeData_ChangeEmployeesPositionToAnotherPosition_LeadsToOrderCreation() {
        //Только пользователь с ролью Сотрудник ОК может изменять данные о сотруднике

        //Будем менять преподавателя первого подразделения "Сидоров", "Николай", "Григорьевич"
        //Изменим его Должность; С Преподавателя Первого подразделения на Преподаватель Второго подразделения.
        User user = null;
        String login = "LFILIMONOV";                    //LFILIMONOV - Сотрудник ОК Первого Подразделения.
        String password = "LFILIMONOV";
        user = databaseOperationService.findUserByLogin(login);

        Division newDivision = organization.getDivisionByCode("0102"); //Будем переводить во второе подразделение
        Position newPosition = newDivision.getPositionByName("Преподаватель");

        Employee employeeToChange = staff.searchEmployeeByPassportData(4103, 401103); //Сидоров Николай Григорьевич, преподаватель первого подразделения
        Employee changedEmployee = new Employee(employeeToChange.getLastName(), employeeToChange.getFirstName(), employeeToChange.getMiddleName(), employeeToChange.getBirthday(),
                employeeToChange.getPassportSeries(), employeeToChange.getPassportNumber(), newPosition, employeeToChange.getPersonnelNumber(),
                employeeToChange.getScientificTitle(), employeeToChange.getScienceDegree(), employeeToChange.getEmail(),
                employeeToChange.getContractStartDate(), employeeToChange.getContractExpirationDate());   //Изменена только Должность

        //Запоминаем текущую Должность
        Position oldPosition = employeeToChange.getCurrentPosition();

        //Проверка, что найденный сотрудник - тот, которого рассчитывали вытащить.
        Assertions.assertNotNull(employeeToChange);
        Assertions.assertEquals("Сидоров", employeeToChange.getLastName());
        Assertions.assertEquals("Николай", employeeToChange.getFirstName());
        Assertions.assertEquals("Григорьевич", employeeToChange.getMiddleName());
        Assertions.assertEquals(103, employeeToChange.getPersonnelNumber());
        Assertions.assertEquals("0101", employeeToChange.getCurrentPosition().getParentDivision().getDivisionCode());

        /* Настройка заглушек */
        // employeeRepository = mock(EmployeeRepository.class);
        when(databaseOperationService.saveEmployee(changedEmployee)).thenReturn(changedEmployee);

        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getEmployeeToChange()).thenReturn(employeeToChange);                        //Сидоров Николай Григорьевич, преподаватель первого подразделения
        when(userInteractionService.getChangedEmployee(employeeToChange)).thenReturn(changedEmployee);          //Сидоров Николай Григорьевич, преподаватель второго подразделения

        /* Создание экземпляра сервиса Бизнес-процессов */
        /* businessProcessService = new BusinessProcessServiceBean(this.organization, this.staff, this.orders, this.users, this.authorizationService, this.userInteractionService,
                this.userRepository, this.employeeRepository, this.orderRepository); */

        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        Employee employee = businessProcessService.changeEmployeeData(user);            //Должно вернуть Сидорова с измененным Подразделением

        /** assert **/
        Assertions.assertNotNull(employee);
        Assertions.assertEquals("Сидоров", employee.getLastName());
        Assertions.assertEquals("Николай", employee.getFirstName());
        Assertions.assertEquals("Григорьевич", employee.getMiddleName());
        Assertions.assertEquals(4103, employee.getPassportSeries());
        Assertions.assertEquals(401103, employee.getPassportNumber());
        Assertions.assertEquals(103, employee.getPersonnelNumber());
        Assertions.assertNotNull(employee.getCurrentPosition());
        Assertions.assertEquals("0102", employee.getCurrentPosition().getParentDivision().getDivisionCode());

        //Проверяем, что в рамках теста был создано приказ - Перевод на новую Должность
     //   Assertions.assertEquals(4, this.orders.size());
     //   Assertions.assertEquals("Перевод на другую Должность", this.orders.get(3).getType());

        //Проверяем, что бизнес-процесс сохранил сотрудника
        verify(databaseOperationService, times(1)).saveEmployee(changedEmployee);

        //Проверяем, что старый экземпляр Сотрудника не содержится в oldPosition
        Assertions.assertFalse(oldPosition.getEmployees().contains(employeeToChange));

        //Проверяем, что новый экземпляр Сотрудника содержится в Должности
        Assertions.assertTrue(newPosition.getEmployees().contains(employee));
    }

    /*************************************************************************************** Тесты Внесение данных о новых сотрудниках ********************************************************/
    @Disabled
    @Test
    @DisplayName("Тест: Бизнес-процесс Внесение данных о новых сотрудниках. " +
            "1) Авторизуемся как Сотрудник отдела кадров Первого подразделения LFILIMONOV;" +
            "2) Создаем нового Сотрудника на Должность Преподаватель Первого подразделения;" +
            "3) ??? PROFIT, так как прав достаточно, и есть свободные Должности.")
    public void TestHireEmployee_ByUserWithValidRole_AndSameDivisionAsEmployeeCreated_CompletesSuccessfully() {
        User user = null;
        String login = "LFILIMONOV";                    //LFILIMONOV - Сотрудник ОК Первого Подразделения.
        String password = "LFILIMONOV";
        user = databaseOperationService.findUserByLogin(login);

        Position position = this.organization.getDivisionByCode("0101").getPositionByName("Преподаватель");
        Employee employeeToCreate = new Employee("Крайнева", "Ариадна", "Афанасьевна", new GregorianCalendar(1971,10,1).getTime(),
                4150, 401150, position,
                150, "Доцент", "Кандидат наук", "akrayneva@domain.com",
                new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2031, 11, 20).getTime());

        //Проверка, что Должность не содержит этого Сотрудника
        Assertions.assertFalse(position.getEmployees().contains(employeeToCreate));

        /* Настройка заглушек */
        // employeeRepository = spy(EmployeeRepository.class);
        when(databaseOperationService.saveEmployee(employeeToCreate)).thenReturn(employeeToCreate);

        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getEmployeeToCreate()).thenReturn(employeeToCreate);                        //Новый Сотрудник Крайнева Ариадна Афанасьевна

        /* Создание экземпляра сервиса Бизнес-процессов */
        /* businessProcessService = new BusinessProcessServiceBean(this.organization, this.staff, this.orders, this.users, this.authorizationService, this.userInteractionService,
                this.userRepository, this.employeeRepository, this.orderRepository); */

        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        Employee employee = businessProcessService.hireEmployee(user);

        /** assert **/
        Assertions.assertNotNull(employee);
        Assertions.assertEquals(4150, employee.getPassportSeries());
        Assertions.assertEquals(401150, employee.getPassportNumber());
        Assertions.assertTrue(position.getEmployees().contains(employee));    //После создания Должность должна содержать этого Сотрудника

        //Проверяем, что процесс сохранял Сотрудника
        verify(databaseOperationService, times(1)).saveEmployee(employeeToCreate);
     //   Assertions.assertEquals(4, this.orders.size());    //После создания Сотрудника должен появиться приказ Прием на работу
     //   Assertions.assertEquals("Прием на работу", this.orders.get(3).getType());    //После создания Сотрудника должен появиться приказ Прием на работу
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Внесение данных о новых сотрудниках. " +
            "1) Авторизуемся как Сотрудник отдела кадров Второго подразделения AIBRAGIMOVA;" +
            "2) Создаем нового Сотрудника на Должность Преподаватель Первого подразделения;" +
            "3) ??? RuntimeException, так как прав недостаточно")
    public void TestHireEmployee_ByUserWithValidRole_ButDifferentDivisionFromEmployeeCreated_ThrowsException() {
     //   User user = null;
        String login = "AIBRAGIMOVA";                    //AIBRAGIMOVA - Сотрудник ОК Второго Подразделения.
        String password = "AIBRAGIMOVA";
        final User user = databaseOperationService.findUserByLogin(login);

        Position position = this.organization.getDivisionByCode("0101").getPositionByName("Преподаватель");
        Employee employeeToCreate = new Employee("Крайнева", "Ариадна", "Афанасьевна", new GregorianCalendar(1971, 10, 1).getTime(),
                4150, 401150, position,
                150, "Доцент", "Кандидат наук", "akrayneva@domain.com",
                new GregorianCalendar(2005, 7, 1).getTime(), new GregorianCalendar(2031, 11, 20).getTime());

        //Проверка, что Должность не содержит этого Сотрудника
        Assertions.assertFalse(position.getEmployees().contains(employeeToCreate));

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getEmployeeToCreate()).thenReturn(employeeToCreate);                        //Новый Сотрудник Крайнева Ариадна Афанасьевна

        /* Создание экземпляра сервиса Бизнес-процессов */
        /* businessProcessService = new BusinessProcessServiceBean(this.organization, this.staff, this.orders, this.users, this.authorizationService, this.userInteractionService,
                this.userRepository, this.employeeRepository, this.orderRepository); */

        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()->{
            Employee employee = businessProcessService.hireEmployee(user);
        });
    }

    /*************************************************************************************** Тесты Выдача списка свободных Должностей по всем Подразделениям ************************/
    @Test
    @DisplayName("Тест: Бизнес-процесс Выдача списка свободных Должностей по всем Подразделениям. " +
            "Успешный сценарий: Пользователь с ролью Сотрудник ОК может запросить " +
            "список свободных Должностей")
    public void TestThatHREmployeeCanSuccessfullyGetVacantPositionsForOrganization() {
        /** arrange **/
        User user = null;
        String login = "LFILIMONOV";                    //LFILIMONOV - Сотрудник ОК Первого Подразделения.
        String password = "LFILIMONOV";
        user = databaseOperationService.findUserByLogin(login);

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));

        // organizationRepository = mock(OrganizationRepository.class);
        when(databaseOperationService.findOrganization()).thenReturn(this.organization);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        Collection<Position> positions = businessProcessService.getVacantPositionListByOrganization(user);

        /** assert **/
        Assertions.assertNotNull(positions);
        Assertions.assertEquals(6, positions.size());
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Выдача списка свободных Должностей по всем Подразделениям. " +
            "Успешный сценарий: Пользователь с ролью Руководитель ОК может запросить " +
            "список свободных Должностей")
    public void TestThatHRManagerCanSuccessfullyGetVacantPositionsForOrganization() {
        /** arrange **/
        User user = null;
        String login = "NKOLESNIKOVA";                    //NKOLESNIKOVA - Руководитель ОК Первого Подразделения.
        String password = "NKOLESNIKOVA";
        user = databaseOperationService.findUserByLogin(login);

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));

        // organizationRepository = mock(OrganizationRepository.class);
        when(databaseOperationService.findOrganization()).thenReturn(this.organization);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        Collection<Position> positions = businessProcessService.getVacantPositionListByOrganization(user);

        /** assert **/
        Assertions.assertNotNull(positions);
        Assertions.assertEquals(6, positions.size());
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Выдача списка свободных Должностей по всем Подразделениям. " +
            "Попытка выполнить бизнес-процесс с ролью, отличной от Сотрудник ОК/Руководитель ОК вернет RuntimeException")
    public void TestThatAdministratorTryingToGetVacantPositionsForOrganizationGetsRuntimeException () {
        /** arrange **/
     //   User user = null;
        String login = "GCHERNYKH";                    //GCHERNYKH - Администратор Третьего Подразделения.
        String password = "GCHERNYKH";
        final User user = databaseOperationService.findUserByLogin(login);

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));

        // organizationRepository = mock(OrganizationRepository.class);
        when(databaseOperationService.findOrganization()).thenReturn(this.organization);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Collection<Position> positions = businessProcessService.getVacantPositionListByOrganization(user);
        });
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Выдача списка свободных Должностей по всем Подразделениям. " +
            "Попытка выполнить бизнес-процесс по Организации без Подразделений вернет RuntimeException")
    public void TestThatTryingToGetVacantPositionsForOrganizationWithoutDivisionsGetsRuntimeException() {
        /** arrange **/
     //   User user = null;
        String login = "LFILIMONOV";                    //LFILIMONOV - Сотрудник ОК Первого Подразделения.
        String password = "LFILIMONOV";
        final User user = databaseOperationService.findUserByLogin(login);

        Organization emptyOrganization = new Organization(new LinkedHashSet<Division>());

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));

        // organizationRepository = mock(OrganizationRepository.class);
        when(databaseOperationService.findOrganization()).thenReturn(emptyOrganization);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Collection<Position> positions = businessProcessService.getVacantPositionListByOrganization(user);
        });
    }

    /*************************************************************************************** Тесты Выдача списка свободных Должностей по Подразделению ************************/
    @Test
    @DisplayName("Тест: Бизнес-процесс Выдача списка свободных Должностей по Подразделению. " +
            "Успешный сценарий: Пользователь с ролью Сотрудник ОК может запросить " +
            "список свободных Должностей")
    public void TestThatHREmployeeCanSuccessfullyGetVacantPositionsByDivision() {
        /** arrange **/
        User user = null;
        String login = "LFILIMONOV";                    //LFILIMONOV - Сотрудник ОК Первого Подразделения.
        String password = "LFILIMONOV";
        user = databaseOperationService.findUserByLogin(login);

        Division divisionToSearch = this.organization.getDivisionByCode("0101"); //Первое Подразделение

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getDivisionToSearchVacantPositions()).thenReturn(divisionToSearch);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        Collection<Position> positions = businessProcessService.getVacantPositionListByDivision(user);

        /** assert **/
        Assertions.assertNotNull(positions);
        Assertions.assertEquals(2, positions.size());
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Выдача списка свободных Должностей по Подразделению. " +
            "Успешный сценарий: Пользователь с ролью Руководитель ОК может запросить " +
            "список свободных Должностей")
    public void TestThatHRManagerCanSuccessfullyGetVacantPositionsByDivision() {
        /** arrange **/
        User user = null;
        String login = "NKOLESNIKOVA";                    //NKOLESNIKOVA - Руководитель ОК Первого Подразделения.
        String password = "NKOLESNIKOVA";
        user = databaseOperationService.findUserByLogin(login);

        Division divisionToSearch = this.organization.getDivisionByCode("0101"); //Первое Подразделение

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getDivisionToSearchVacantPositions()).thenReturn(divisionToSearch);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        Collection<Position> positions = businessProcessService.getVacantPositionListByDivision(user);

        /** assert **/
        Assertions.assertNotNull(positions);
        Assertions.assertEquals(2, positions.size());
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Выдача списка свободных Должностей по Подразделению. " +
            "Попытка выполнить бизнес-процесс с ролью, отличной от Сотрудник ОК/Руководитель ОК вернет RuntimeException")
    public void TestThatAdministratorTryingToGetVacantPositionsForDivisionGetsRuntimeException() {
        /** arrange **/
     //   User user = null;
        String login = "GCHERNYKH";                    //GCHERNYKH - Администратор Третьего Подразделения.
        String password = "GCHERNYKH";
        final User user = databaseOperationService.findUserByLogin(login);

        Division divisionToSearch = this.organization.getDivisionByCode("0101"); //Первое Подразделение

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getDivisionToSearchVacantPositions()).thenReturn(divisionToSearch);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Collection<Position> positions = businessProcessService.getVacantPositionListByDivision(user);
        });
    }

    /*************************************************************************************** Тесты Внесение новых данных в штатное расписание ************************/
    @Test
    @DisplayName("Тест: Бизнес-процесс Внесение новых данных в штатное расписание. " +
            "Попытка выполнить бизнес-процесс с ролью, отличной от Руководитель ОК вернет RuntimeException")
    public void TestThatExecutingAddDataToStaffListWithRoleOtherThanHRManagerThrowsRuntimeException() {
        /** arrange **/
     //   User user = null;
        String login = "LFILIMONOV";                    //LFILIMONOV - Сотрудник ОК Первого Подразделения.
        String password = "LFILIMONOV";
        final User user = databaseOperationService.findUserByLogin(login);

        Division targetDivisionForStaffListPosition = this.organization.getDivisionByCode("0101");  //Будем добавлять в Первое Подразделение
        Position newPositionForStaffList = new Position("Заведующий кафедры", 1, 1, targetDivisionForStaffListPosition, new LinkedHashSet<Employee>());

        //Перед началом теста проверяем, что такой Должности еще нет в Первом Подразделении
        Assertions.assertNull(targetDivisionForStaffListPosition.getPositionByName(newPositionForStaffList.getPositionName()));

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getTargetDivisionForStaffListPosition()).thenReturn(targetDivisionForStaffListPosition);
        when(userInteractionService.getNewPositionForStaffListDivision(targetDivisionForStaffListPosition)).thenReturn(newPositionForStaffList);

        // positionRepository = mock(PositionRepository.class);
        when(databaseOperationService.savePosition(newPositionForStaffList)).thenReturn(newPositionForStaffList);

        // divisionRepository = mock(DivisionRepository.class);
        when(databaseOperationService.saveDivision(targetDivisionForStaffListPosition)).thenReturn(targetDivisionForStaffListPosition);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Position position = businessProcessService.addDataToStaffList(user);
        });
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Внесение новых данных в штатное расписание. " +
            "Попытка внести Должность с Сотрудниками, ссылающимися " +
            "на эту Должность, вызовет RuntimeException")
    public void TestThatTryingToAddPositionWithEmployeesToStaffListThrowsRuntimeException() {
        /** arrange **/
     //   User user = null;
        String login = "NKOLESNIKOVA";                    //NKOLESNIKOVA - Руководитель ОК Первого Подразделения.
        String password = "NKOLESNIKOVA";
        final User user = databaseOperationService.findUserByLogin(login);

        Division targetDivisionForStaffListPosition = this.organization.getDivisionByCode("0101");  //Будем добавлять в Первое Подразделение
        Position newPositionForStaffList = new Position("Заведующий кафедры", 1, 1, targetDivisionForStaffListPosition, new LinkedHashSet<Employee>());

        //Добавляем Сотрудника в создаваемую Должность. Попытка создать запись в штатном расписании теперь вызовет
        //RuntimeException
        newPositionForStaffList.addEmployee(new Employee("Сутулин", "Артем", "Миронович", new GregorianCalendar(1959,10,1).getTime(),
                4150, 401150, newPositionForStaffList,
                150, "Отсутствует", "Отсутствует", "tfrolova@domain.com",
                new GregorianCalendar(2005,7,1).getTime(), new GregorianCalendar(2031, 11, 1).getTime()));

        //Перед началом теста проверяем, что такой Должности еще нет в Первом Подразделении
        Assertions.assertNull(targetDivisionForStaffListPosition.getPositionByName(newPositionForStaffList.getPositionName()));

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getTargetDivisionForStaffListPosition()).thenReturn(targetDivisionForStaffListPosition);
        when(userInteractionService.getNewPositionForStaffListDivision(targetDivisionForStaffListPosition)).thenReturn(newPositionForStaffList);

        // positionRepository = mock(PositionRepository.class);
        when(databaseOperationService.savePosition(newPositionForStaffList)).thenReturn(newPositionForStaffList);

        // divisionRepository = mock(DivisionRepository.class);
        when(databaseOperationService.saveDivision(targetDivisionForStaffListPosition)).thenReturn(targetDivisionForStaffListPosition);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Position position = businessProcessService.addDataToStaffList(user);
        });
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Внесение новых данных в штатное расписание. " +
            "Успешный сценарий: выполнение сервиса от лица Пользователя с ролью " +
            "Руководитель ОК и передачей корректной Должности приведет к успешному созданию и сохранению записи")
    public void TestThatTryingToAddDataToStaffListWithHRManagerRoleAndCorrectPositionCompletesSuccessfully() {
        /** arrange **/
        User user = null;
        String login = "NKOLESNIKOVA";                    //NKOLESNIKOVA - Руководитель ОК Первого Подразделения.
        String password = "NKOLESNIKOVA";
        user = databaseOperationService.findUserByLogin(login);

        Division targetDivisionForStaffListPosition = this.organization.getDivisionByCode("0101");  //Будем добавлять в Первое Подразделение
        Position newPositionForStaffList = new Position("Заведующий кафедры", 1, 1, targetDivisionForStaffListPosition, new LinkedHashSet<Employee>());

        //Перед началом теста проверяем, что такой Должности еще нет в Первом Подразделении
        Assertions.assertNull(targetDivisionForStaffListPosition.getPositionByName(newPositionForStaffList.getPositionName()));

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getTargetDivisionForStaffListPosition()).thenReturn(targetDivisionForStaffListPosition);
        when(userInteractionService.getNewPositionForStaffListDivision(targetDivisionForStaffListPosition)).thenReturn(newPositionForStaffList);

        // positionRepository = mock(PositionRepository.class);
        when(databaseOperationService.savePosition(newPositionForStaffList)).thenReturn(newPositionForStaffList);

        // divisionRepository = mock(DivisionRepository.class);
        when(databaseOperationService.saveDivision(targetDivisionForStaffListPosition)).thenReturn(targetDivisionForStaffListPosition);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        Position createdPosition = businessProcessService.addDataToStaffList(user);

        /** assert **/
        Assertions.assertNotNull(createdPosition);
        Assertions.assertEquals("Заведующий кафедры", createdPosition.getPositionName());

        //Проверим, что Должность была успешно добавлена в Подразделение
        Assertions.assertTrue(targetDivisionForStaffListPosition.getPositions().contains(createdPosition));

        //Проверим, что сервисы сохранения Подразделения и Должности вызывались
        verify(databaseOperationService, times(1)).savePosition(newPositionForStaffList);
     //   verify(databaseOperationService, times(1)).saveDivision(targetDivisionForStaffListPosition);
    }

    /*************************************************************************************** Тесты Изменение данных в штатном расписании ************************/
    @Test
    @DisplayName("Тест: Бизнес-процесс Изменение данных в штатном расписании. " +
            "Попытка удалить Должность, которую занимают Сотрудники, " +
            "ведет к RuntimeException")
    public void TestThatTryingToRemovePositionWhichIsHasEmployeesThrowsRuntimeException() {
        /** arrange **/
     //   User user = null;
        String login = "NKOLESNIKOVA";                    //NKOLESNIKOVA - Руководитель ОК Первого Подразделения.
        String password = "NKOLESNIKOVA";
        final User user = databaseOperationService.findUserByLogin(login);

        Position positionToChange = this.organization.getDivisionByCode("0101").getPositionByName("Преподаватель");  //В Первом Подразделении есть Сотрудники, которые занимают Должность Преподаватель
        Division oldDivision = positionToChange.getParentDivision();
        Position positionAfterChange = null;

        //Перед началом теста убедимся, что Должность найдена и есть Сотрудники, занимающие эту Должность
        Assertions.assertNotNull(positionToChange);
        Assertions.assertTrue(positionToChange.getEmployees().size() > 0);

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getPositionInStaffListToChange(positionToChange.getParentDivision())).thenReturn(positionToChange);
        when(userInteractionService.getPositionInStaffListAfterChange(positionToChange)).thenReturn(positionAfterChange);

        // positionRepository = mock(PositionRepository.class);
        when(databaseOperationService.savePosition(positionAfterChange)).thenReturn(positionAfterChange);

        // divisionRepository = mock(DivisionRepository.class);
        when(databaseOperationService.saveDivision(oldDivision)).thenReturn(oldDivision);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Position position = businessProcessService.changeDataInStaffList(user);
        });
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Изменение данных в штатном расписании. " +
            "Попытка изменить Подразделение, к которому привязана Должность, " +
            "ведет к RuntimeException")
    public void TestThatTryingToChangeDivisionOfPositionThrowsRuntimeException() {
        /** arrange **/
     //   User user = null;
        String login = "NKOLESNIKOVA";                    //NKOLESNIKOVA - Руководитель ОК Первого Подразделения.
        String password = "NKOLESNIKOVA";
        final User user = databaseOperationService.findUserByLogin(login);

        Position positionToChange = this.organization.getDivisionByCode("0101").getPositionByName("Преподаватель");  //В Первом Подразделении есть Сотрудники, которые занимают Должность Преподаватель
        Division oldDivision = positionToChange.getParentDivision();

        //Создали копию positionToChange, изменили Подразделение с Первого на Второе
        Position positionAfterChange = new Position(positionToChange.getPositionName(),
                positionToChange.getMaxEmployees(), positionToChange.getVacantPositions(),
                this.organization.getDivisionByCode("0102"), new LinkedHashSet<Employee>(positionToChange.getEmployees()));

        //Перед началом теста убедимся, что Должность найдена и есть Сотрудники, занимающие эту Должность
        Assertions.assertNotNull(positionToChange);
        Assertions.assertTrue(positionToChange.getEmployees().size() > 0);

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getPositionInStaffListToChange(positionToChange.getParentDivision())).thenReturn(positionToChange);
        when(userInteractionService.getPositionInStaffListAfterChange(positionToChange)).thenReturn(positionAfterChange);

        // positionRepository = mock(PositionRepository.class);
        when(databaseOperationService.savePosition(positionAfterChange)).thenReturn(positionAfterChange);

        // divisionRepository = mock(DivisionRepository.class);
        when(databaseOperationService.saveDivision(oldDivision)).thenReturn(oldDivision);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Position position = businessProcessService.changeDataInStaffList(user);
        });
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Изменение данных в штатном расписании. " +
            "Попытка изменить Сотрудников, занимающих Должность, " +
            "ведет к RuntimeException")
    public void TestThatTryingToChangeEmployeesOfPositionToChangeThrowsException() {
        /** arrange **/
     //   User user = null;
        String login = "NKOLESNIKOVA";                    //NKOLESNIKOVA - Руководитель ОК Первого Подразделения.
        String password = "NKOLESNIKOVA";
        final User user = databaseOperationService.findUserByLogin(login);

        Position positionToChange = this.organization.getDivisionByCode("0101").getPositionByName("Преподаватель");  //В Первом Подразделении есть Сотрудники, которые занимают Должность Преподаватель
        Division oldDivision = positionToChange.getParentDivision();

        //Создали копию positionToChange
        Position positionAfterChange = new Position(positionToChange.getPositionName(),
                positionToChange.getMaxEmployees(), positionToChange.getVacantPositions(),
                positionToChange.getParentDivision(), new LinkedHashSet<Employee>(positionToChange.getEmployees()));
        //Удалили всех Сотрудников
        positionAfterChange.getEmployees().clear();

        //Перед началом теста убедимся, что Должность найдена и есть Сотрудники, занимающие эту Должность
        Assertions.assertNotNull(positionToChange);
        Assertions.assertTrue(positionToChange.getEmployees().size() > 0);

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getPositionInStaffListToChange(positionToChange.getParentDivision())).thenReturn(positionToChange);
        when(userInteractionService.getPositionInStaffListAfterChange(positionToChange)).thenReturn(positionAfterChange);

        // positionRepository = mock(PositionRepository.class);
        when(databaseOperationService.savePosition(positionAfterChange)).thenReturn(positionAfterChange);

        // divisionRepository = mock(DivisionRepository.class);
        when(databaseOperationService.saveDivision(oldDivision)).thenReturn(oldDivision);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Position position = businessProcessService.changeDataInStaffList(user);
        });
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Изменение данных в штатном расписании. " +
            "Несоблюдение правила: Количество свободных Должностей + Количество Сотрудников, " +
            "занимающих Должность = Максимальное Количество Сотрудников, занимающих Должность " +
            "ведет к RuntimeException")
    public void TestThatBreakingNumVacantPositionsBalanceRuleThrowsRuntimeException() {
        /** arrange **/
     //   User user = null;
        String login = "NKOLESNIKOVA";                    //NKOLESNIKOVA - Руководитель ОК Первого Подразделения.
        String password = "NKOLESNIKOVA";
        final User user = databaseOperationService.findUserByLogin(login);

        Position positionToChange = this.organization.getDivisionByCode("0101").getPositionByName("Преподаватель");  //В Первом Подразделении есть Сотрудники, которые занимают Должность Преподаватель
        Division oldDivision = positionToChange.getParentDivision();

        //Создали копию positionToChange
        Position positionAfterChange = new Position(positionToChange.getPositionName(),
                positionToChange.getMaxEmployees(), positionToChange.getVacantPositions(),
                positionToChange.getParentDivision(), positionToChange.getEmployees());
        //Увеличиваем vacantPositions, не увеличивая employees. Это вызовет нарушение правила.
        positionAfterChange.setVacantPositions(positionToChange.getVacantPositions() + 1);

        //Перед началом теста убедимся, что Должность найдена и есть Сотрудники, занимающие эту Должность
        Assertions.assertNotNull(positionToChange);
        Assertions.assertTrue(positionToChange.getEmployees().size() > 0);

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getPositionInStaffListToChange(positionToChange.getParentDivision())).thenReturn(positionToChange);
        when(userInteractionService.getPositionInStaffListAfterChange(positionToChange)).thenReturn(positionAfterChange);

        // positionRepository = mock(PositionRepository.class);
        when(databaseOperationService.savePosition(positionAfterChange)).thenReturn(positionAfterChange);

        // divisionRepository = mock(DivisionRepository.class);
        when(databaseOperationService.saveDivision(oldDivision)).thenReturn(oldDivision);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Position position = businessProcessService.changeDataInStaffList(user);
        });
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Изменение данных в штатном расписании. " +
            "Попытка изменить Название Должности на не уникальное " +
            "в пределах Подразделения" +
            "ведет к RuntimeException")
    public void TestThatTryingToChangePositionNameToNonUniqueThrowsRuntimeException() {
        /** arrange **/
     //   User user = null;
        String login = "NKOLESNIKOVA";                    //NKOLESNIKOVA - Руководитель ОК Первого Подразделения.
        String password = "NKOLESNIKOVA";
        final User user = databaseOperationService.findUserByLogin(login);

        Position positionToChange = this.organization.getDivisionByCode("0101").getPositionByName("Преподаватель");  //В Первом Подразделении есть Сотрудники, которые занимают Должность Преподаватель
        Division oldDivision = positionToChange.getParentDivision();

        //Создали копию positionToChange
        Position positionAfterChange = new Position(positionToChange.getPositionName(),
                positionToChange.getMaxEmployees(), positionToChange.getVacantPositions(),
                positionToChange.getParentDivision(), positionToChange.getEmployees());
        //Изменяем название на Декан. Так как в Подразделении уже есть Декан, вернет RuntimeException
        positionAfterChange.setPositionName("Декан");

        //Перед началом теста убедимся, что Должность найдена и есть Сотрудники, занимающие эту Должность
        Assertions.assertNotNull(positionToChange);
        Assertions.assertTrue(positionToChange.getEmployees().size() > 0);

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getPositionInStaffListToChange(positionToChange.getParentDivision())).thenReturn(positionToChange);
        when(userInteractionService.getPositionInStaffListAfterChange(positionToChange)).thenReturn(positionAfterChange);

        // positionRepository = mock(PositionRepository.class);
        when(databaseOperationService.savePosition(positionAfterChange)).thenReturn(positionAfterChange);

        // divisionRepository = mock(DivisionRepository.class);
        when(databaseOperationService.saveDivision(oldDivision)).thenReturn(oldDivision);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Position position = businessProcessService.changeDataInStaffList(user);
        });
    }

    @Test
    @DisplayName("Тест: Бизнес-процесс Изменение данных в штатном расписании. " +
            "Успешный сценарий: были совершены только допустимые изменения - " +
            "Название Должности без нарушения уникальности в пределах Подразделения, Максимальное количество сотрудников, " +
            "Количество свободных Должностей без нарушения правила.")
    public void TestThatChangingDataInStaffListCompletesSuccessfullyWhenConditionsAreMet() {
        /** arrange **/
        User user = null;
        String login = "NKOLESNIKOVA";                    //NKOLESNIKOVA - Руководитель ОК Первого Подразделения.
        String password = "NKOLESNIKOVA";
        user = databaseOperationService.findUserByLogin(login);

        Position positionToChange = this.organization.getDivisionByCode("0101").getPositionByName("Преподаватель");  //В Первом Подразделении есть Сотрудники, которые занимают Должность Преподаватель
        Division oldDivision = positionToChange.getParentDivision();

        //Создали копию positionToChange
        Position positionAfterChange = new Position(positionToChange.getPositionName(),
                positionToChange.getMaxEmployees(), positionToChange.getVacantPositions(),
                positionToChange.getParentDivision(), positionToChange.getEmployees());
        //Изменяем название на уникальное в пределах Подразделения. Изменяем maxEmployees на ту же величину, что и vacantPositions
        positionAfterChange.setPositionName("Пирог");
        positionAfterChange.setMaxEmployees(positionToChange.getMaxEmployees() + 1);
        positionAfterChange.setVacantPositions(positionToChange.getVacantPositions() + 1);

        //Перед началом теста убедимся, что Должность найдена и есть Сотрудники, занимающие эту Должность
        Assertions.assertNotNull(positionToChange);
        Assertions.assertTrue(positionToChange.getEmployees().size() > 0);

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getPositionInStaffListToChange(positionToChange.getParentDivision())).thenReturn(positionToChange);
        when(userInteractionService.getPositionInStaffListAfterChange(positionToChange)).thenReturn(positionAfterChange);

        // positionRepository = mock(PositionRepository.class);
        when(databaseOperationService.savePosition(positionAfterChange)).thenReturn(positionAfterChange);

        // divisionRepository = mock(DivisionRepository.class);
        when(databaseOperationService.saveDivision(oldDivision)).thenReturn(oldDivision);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        Position returnedPosition = businessProcessService.changeDataInStaffList(user);

        /** assert **/
        Assertions.assertNotNull(returnedPosition);
        Assertions.assertEquals(positionAfterChange, returnedPosition);

        //Проверяем, что Должность была сохранена
        verify(databaseOperationService, times(1)).savePosition(positionAfterChange);
    }

    /*************************************************************************************** Тесты Регистрация в системе Пользователя ************************/
    @Test
    @DisplayName("Тест: Регистрация в системе Пользователя. " +
            "Попытка вызвать сервис не от лица Администратор ведет к RuntimeException")
    public void TestThatTryingToRegisterUserWithoutAdministratorRoleThrowsRuntimeException() {
        /** arrange **/
     //   User user = null;
        String login = "NKOLESNIKOVA";                    //NKOLESNIKOVA - Руководитель ОК Первого Подразделения. Не является Администратором, поэтому вызовет RuntimeException
        String password = "NKOLESNIKOVA";
        final User user = databaseOperationService.findUserByLogin(login);

        Employee targetEmployeeForCreatingUser = this.organization.getDivisionByCode("0102").getPositionByName("Сотрудник ОК").searchEmployeeByPersonnelNumber(207); //Во втором Подразделении Сотрудник ОК имеет 1 свободную Должность из 3, а пользователь есть только для 1 из 2 занятых
        User registeredUser = new User(targetEmployeeForCreatingUser, "KABOIMOVA", "KABOIMOVA", "Сотрудник ОК");

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getTargetEmployeeForCreatingUser()).thenReturn(targetEmployeeForCreatingUser);
        when(userInteractionService.getCreatedUser(targetEmployeeForCreatingUser)).thenReturn(registeredUser);

        when(databaseOperationService.saveUser(registeredUser)).thenReturn(registeredUser);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            User returnedUser = businessProcessService.registerUser(user);
        });
    }

    @Test
    @DisplayName("Тест: Регистрация в системе Пользователя. " +
            "Попытка создать Пользователя для Сотрудника без Должности ведет к RuntimeException")
    public void TestThatTryingToCreateUserForEmployeeWithoutPositionThrowsRuntimeException() {
        /** arrange **/
     //   User user = null;
        String login = "GCHERNYKH";                    //GCHERNYKH - Администратор Третьего Подразделения.
        String password = "GCHERNYKH";
        final User user = databaseOperationService.findUserByLogin(login);

        Employee targetEmployeeForCreatingUser = this.organization.getDivisionByCode("0102").getPositionByName("Сотрудник ОК").searchEmployeeByPersonnelNumber(207); //Во втором Подразделении Сотрудник ОК имеет 1 свободную Должность из 3, а пользователь есть только для 1 из 2 занятых
        //Изменяем Должность на null. Попытка создания Пользователя для Сотрудника без Должности вызовет RuntimeException.
        targetEmployeeForCreatingUser.setCurrentPosition(null);

        User registeredUser = new User(targetEmployeeForCreatingUser, "KABOIMOVA", "KABOIMOVA", "Сотрудник ОК");

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getTargetEmployeeForCreatingUser()).thenReturn(targetEmployeeForCreatingUser);
        when(userInteractionService.getCreatedUser(targetEmployeeForCreatingUser)).thenReturn(registeredUser);

        when(databaseOperationService.saveUser(registeredUser)).thenReturn(registeredUser);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            User returnedUser = businessProcessService.registerUser(user);
        });
    }

    @Test
    @DisplayName("Тест: Регистрация в системе Пользователя. " +
            "Успешный сценарий: Пользователь создан")
    public void TestThatCreatingUserFromAdministratorForCorrectEmployeeCompletesSuccessfully() {
        /** arrange **/
        User user = null;
        String login = "GCHERNYKH";                    //GCHERNYKH - Администратор Третьего Подразделения.
        String password = "GCHERNYKH";
        user = databaseOperationService.findUserByLogin(login);

        Employee targetEmployeeForCreatingUser = this.organization.getDivisionByCode("0102").getPositionByName("Сотрудник ОК").searchEmployeeByPersonnelNumber(207); //Во втором Подразделении Сотрудник ОК имеет 1 свободную Должность из 3, а пользователь есть только для 1 из 2 занятых

        User registeredUser = new User(targetEmployeeForCreatingUser, "KABOIMOVA", "KABOIMOVA", "Сотрудник ОК");

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getTargetEmployeeForCreatingUser()).thenReturn(targetEmployeeForCreatingUser);
        when(userInteractionService.getCreatedUser(targetEmployeeForCreatingUser)).thenReturn(registeredUser);

        when(databaseOperationService.saveUser(registeredUser)).thenReturn(registeredUser);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        User returnedUser = businessProcessService.registerUser(user);

        /** assert **/
        Assertions.assertNotNull(returnedUser);
        Assertions.assertEquals("KABOIMOVA", returnedUser.getLogin());
        Assertions.assertEquals("KABOIMOVA", returnedUser.getPassword());

        //Проверяем, что вызывался сервис сохранения Пользователей
        verify(databaseOperationService, times(1)).saveUser(registeredUser);
    }

    /*************************************************************************************** Тесты Настройка прав доступа, изменение прав доступа ************************/
    @Test
    @DisplayName("Тест: Настройка прав доступа, изменение прав доступа. " +
            "Попытка изменить права пользователя, не обладая правами Администратора, ведет к RuntimeException")
    public void TestThatTryingToSetAccessRightsWithoutAdministratorRoleThrowsRuntimeException() {
        /** arrange **/
     //   User user = null;
        String login = "NKOLESNIKOVA";                    //NKOLESNIKOVA - Руководитель ОК Первого Подразделения. Не является Администратором, поэтому вызовет RuntimeException
        String password = "NKOLESNIKOVA";
        final User user = databaseOperationService.findUserByLogin(login);

        User userToChange = this.users.get(0);         //NKOLESNIKOVA, Руководитель ОК
        User userAfterChange = new User(userToChange.getEmployee(), userToChange.getLogin(), userToChange.getPassword(), userToChange.getRole());

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getUserToChange(this.users)).thenReturn(userToChange);
        when(userInteractionService.getChangedUser(userToChange)).thenReturn(userAfterChange);

        when(databaseOperationService.saveUser(userAfterChange)).thenReturn(userAfterChange);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            User returnedUser = businessProcessService.setAccessRights(user);
        });
    }

    @Test
    @DisplayName("Тест: Настройка прав доступа, изменение прав доступа. " +
            "Попытка изменить Сотрудника, закрепленного за Пользователем, ведет к RuntimeException")
    public void TestThatTryingToChangeEmployeeOfUserThrowsRuntimeException() {
        /** arrange **/
     //   User user = null;
        String login = "GCHERNYKH";                    //GCHERNYKH - Администратор Третьего Подразделения.
        String password = "GCHERNYKH";
        final User user = databaseOperationService.findUserByLogin(login);

        User userToChange = this.users.get(0);         //NKOLESNIKOVA, Руководитель ОК
        User userAfterChange = new User(userToChange.getEmployee(), userToChange.getLogin(), userToChange.getPassword(), userToChange.getRole());
        //Изменим Сотрудника, соответствующего Пользователю. Это изменение недопустимо и вызовет RuntimeException
        userAfterChange.setEmployee(this.staff.searchEmployeeByPersonnelNumber(103));   //Сидоров Николай Григорьевич, Первое Подразделение

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getUserToChange(this.users)).thenReturn(userToChange);
        when(userInteractionService.getChangedUser(userToChange)).thenReturn(userAfterChange);

        when(databaseOperationService.saveUser(userAfterChange)).thenReturn(userAfterChange);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            User returnedUser = businessProcessService.setAccessRights(user);
        });
    }

    @Test
    @DisplayName("Тест: Настройка прав доступа, изменение прав доступа. " +
            "Попытка удалить Пользователя, который не был уволен (Должность которого не null), " +
            "ведет к RuntimeException")
    public void TestThatTryingToRemoveUserWhichIsAssociatedWithPositionThrowsRuntimeException() {
        /** arrange **/
     //   User user = null;
        String login = "GCHERNYKH";                    //GCHERNYKH - Администратор Третьего Подразделения.
        String password = "GCHERNYKH";
        final User user = databaseOperationService.findUserByLogin(login);

        User userToChange = this.users.get(0);         //NKOLESNIKOVA, Руководитель ОК
        User userAfterChange = null;                   //После изменения Пользователь null, но до изменения у него
                                                       //была Должность

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getUserToChange(this.users)).thenReturn(userToChange);
        when(userInteractionService.getChangedUser(userToChange)).thenReturn(userAfterChange);

        when(databaseOperationService.saveUser(userAfterChange)).thenReturn(userAfterChange);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act, assert **/
        Assertions.assertThrows(RuntimeException.class, ()-> {
            User returnedUser = businessProcessService.setAccessRights(user);
        });
    }

    @Test
    @DisplayName("Тест: Настройка прав доступа, изменение прав доступа. " +
            "Успешный сценарий: удаление Пользователя, с которым не ассоциирована Должность")
    public void TestThatDeletingUserWithoutAssociatedPositionCompletesSuccessfully() {
        /** arrange **/
        User user = null;
        String login = "GCHERNYKH";                    //GCHERNYKH - Администратор Третьего Подразделения.
        String password = "GCHERNYKH";
        user = databaseOperationService.findUserByLogin(login);

        User userToChange = this.users.get(0);         //NKOLESNIKOVA, Руководитель ОК
        User userAfterChange = null;                   //После изменения Пользователь null
        //Убираем Должность у Сотрудника, ассоциированного с Пользователем
        userToChange.getEmployee().setCurrentPosition(null);

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getUserToChange(this.users)).thenReturn(userToChange);
        when(userInteractionService.getChangedUser(userToChange)).thenReturn(userAfterChange);

        when(databaseOperationService.saveUser(userAfterChange)).thenReturn(userAfterChange);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        User returnedUser = businessProcessService.setAccessRights(user);

        /** assert **/
        Assertions.assertNull(returnedUser);

        //Проверяем, что был вызван сервис удаления Пользователя
        verify(databaseOperationService, times(1)).deleteUser(userToChange);
    }

    @Test
    @DisplayName("Тест: Настройка прав доступа, изменение прав доступа. " +
            "Успешный сценарий: изменение данных Пользователя")
    public void TestThatChangingUserWithCorrectDataCompletesSuccessfully() {
        /** arrange **/
        User user = null;
        String login = "GCHERNYKH";                    //GCHERNYKH - Администратор Третьего Подразделения.
        String password = "GCHERNYKH";
        user = databaseOperationService.findUserByLogin(login);

        User userToChange = this.users.get(0);         //NKOLESNIKOVA, Руководитель ОК
        User userAfterChange = new User(userToChange.getEmployee(), userToChange.getLogin(), userToChange.getPassword(), userToChange.getRole());
        userAfterChange.setLogin("NKOLESNIKOVA1");
        userAfterChange.setPassword("NKOLESNIKOVA1");
        userAfterChange.setRole("Сотрудник ОК");

        /* Настройка заглушек */
        userInteractionService = spy(UserInteractionService.class);
        when(userInteractionService.getCredentials()).thenReturn(new Credentials(login, password));
        when(userInteractionService.getUserToChange(this.users)).thenReturn(userToChange);
        when(userInteractionService.getChangedUser(userToChange)).thenReturn(userAfterChange);

        when(databaseOperationService.saveUser(userAfterChange)).thenReturn(userAfterChange);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        User returnedUser = businessProcessService.setAccessRights(user);

        /** assert **/
        Assertions.assertNotNull(returnedUser);
        Assertions.assertEquals("NKOLESNIKOVA1", returnedUser.getLogin());
        Assertions.assertEquals("NKOLESNIKOVA1", returnedUser.getPassword());
        Assertions.assertEquals("Сотрудник ОК", returnedUser.getRole());

        //Проверяем, что был вызван сервис сохранения Пользователя
        verify(databaseOperationService, times(1)).saveUser(userAfterChange);
    }

    /*************************************************************************************** Тесты Оповещение Сотрудников, чьи контракты в скором времени заканчиваются ************************/
    @Disabled
    @Test
    @DisplayName("Тест: Оповещение Сотрудников, чьи контракты в скором времени заканчиваются. " +
            "Успешный сценарий.")
    public void Test1() {
        /** arrange **/
        Collection<Employee> employeesWithExpiringContracts = new LinkedHashSet<>();

        /* Настройка заглушек */
        // staffRepository = mock(StaffRepository.class);
        //when(databaseOperationService.getStaff()).thenReturn(this.staff);
        when(databaseOperationService.findOrganization()).thenReturn(this.organization);

        /* Создание экземпляра сервиса Бизнес-процессов */
        businessProcessService = new BusinessProcessServiceBean(this.userInteractionService, this.databaseOperationService);

        /** act **/
        employeesWithExpiringContracts = businessProcessService.getEmployeesWithExpiringContracts();

        /** assert **/
        Assertions.assertNotNull(employeesWithExpiringContracts);
        Assertions.assertEquals(9, employeesWithExpiringContracts.size());
    }

    @AfterEach
    public void tearDown() {
        for(Division division: this.organization.getDivisions()) {
            for(Position position: division.getPositions()) {
                position.getEmployees().clear();
            }
            division.getPositions().clear();
        }
        this.organization.getDivisions().clear();
        for(Order order: this.orders) {
            order.getOrderLines().clear();
        }
        this.staff.getEmployees().clear();
    }

}
