package com.institutes.personnel.interfacesTest;

import com.institutes.personnel.service.UserInteractionService;
import com.institutes.personnel.service.impl.UserInteractionServiceBean;
import javafx.stage.Stage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

//When using this mode, a new test instance will be created for each test method or test factory method.
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class UserInteractionTests {

    private UserInteractionServiceBean userInteractionService;

    @BeforeEach
    public void init() {
        userInteractionService = new UserInteractionServiceBean();
    }

    @Test
    @DisplayName("Проверка работоспособности start")
    public void TestThatStartWorksCorrectly() {
/*        Stage stage = new Stage();
        try {
            userInteractionService.start(stage);
        } catch (Exception e) {
            e.printStackTrace();
        } */
    }

    @Test
    @DisplayName("Проверка работоспособности test")
    public void TestThatTestWorksCorrectly() {
/*        Stage stage = new Stage();
        try {
            userInteractionService.test();
        } catch (Exception e) {
            e.printStackTrace();
        } */
    }

}
