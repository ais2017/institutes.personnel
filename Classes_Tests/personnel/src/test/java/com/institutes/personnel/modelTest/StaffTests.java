package com.institutes.personnel.modelTest;

import com.institutes.personnel.model.Division;
import com.institutes.personnel.model.Employee;
import com.institutes.personnel.model.Position;
import com.institutes.personnel.model.Staff;
import org.junit.jupiter.api.*;

import java.util.*;

//When using this mode, a new test instance will be created for each test method or test factory method.
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class StaffTests {

    public Staff staff;

    public Division division1;

    @BeforeEach
    void init() {

        //Создание объекта Персонал. Сначала добавляем трех сотрудников.
        division1 = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());
        Position position1 = new Position("Преподаватель", 3, 3, division1, new LinkedHashSet<Employee>());

        division1.getPositions().add(position1);

    //    Position position1 = new Position("Преподаватель");
        Date birthday1 = new GregorianCalendar(1959,10,1).getTime();
        Date startDate1 = new GregorianCalendar(2005,7,1).getTime();
        Date endDate1 = new GregorianCalendar(2007, 8, 1).getTime();
        Employee employee1 = new Employee("Сергеев", "Владислав", "Сергеевич", birthday1,
                4014, 219517, position1,
                111, "Доцент", "Кандидат наук", "test@domain.com",
                startDate1, endDate1);

        Position position2 = new Position("Секретарь", 3, 3, division1, new LinkedHashSet<Employee>());
        division1.addPosition(position2);

        Date birthday2 = new GregorianCalendar(1964,8,1).getTime();
        Date startDate2 = new GregorianCalendar(1996,7,1).getTime();
        Date endDate2 = new GregorianCalendar(1997, 8, 1).getTime();
        Employee employee2 = new Employee("Сергеев", "Владислав", "Сергеевич", birthday2,
                4058, 417812, position2,
                114, "Профессор", "Доктор наук", "test@domain.com",
                startDate2, endDate2);

        Position position3 = new Position("Директор", 5, 5, division1, new LinkedHashSet<Employee>());
        division1.addPosition(position3);

        Date birthday3 = new GregorianCalendar(1989,10,1).getTime();
        Date startDate3 = new GregorianCalendar(1996,7,1).getTime();
        Date endDate3 = new GregorianCalendar(2007, 8, 1).getTime();
        Employee employee3 = new Employee("Максимов", "Игорь", "Сергеевич", birthday3,
                5588, 880055, position3,
                115, "Доцент", "Кандидат наук", "test@domain.com",
                startDate3, endDate3);

        //Инициализация персонала
        staff = new Staff(new LinkedHashSet<Employee>(Arrays.asList(employee1, employee2, employee3)));

    }

    @Test
    @DisplayName("Тест: после создания объекта staff в блоке initAll() количество входящих" +
            "в него сотрудников должно быть равно трем")
    public void TestThatStaffWasCorrectlyInitializedWithThreeEmployees() {

        //arrange и act - в блоке init()

        //assert
        Assertions.assertEquals(3, this.staff.getEmployees().size());

    }

    @Test
    @DisplayName("Тест: проверка корректности работы методов addEmployee и deleteEmployee. " +
            "После добавления нового Сотрудника в Штат с тремя Сотрудниками " +
            "количество Сотрудников должно равняться четырем. " +
            "После удаления четвертого Сотрудника количество Сотрудников должно равняться трем")
    public void TestThatAddingFourthEmployee_ToStaffWithThreeEmployeesReturnsSizeFour_AndAfterRemovingFourthEmployeeReturnsSizeThree() {

        //Проверяем, что в начале теста количество сотрудников равно трем
        Assertions.assertEquals(3, this.staff.getEmployees().size());

        //добавляем сотрудника
        Position position4 = new Position("Повар", 3, 3, division1, new LinkedHashSet<Employee>());

        division1.addPosition(position4);

        Date birthday4 = new GregorianCalendar(1989,10,1).getTime();
        Date startDate4 = new GregorianCalendar(1996,7,1).getTime();
        Date endDate4 = new GregorianCalendar(2007, 8, 1).getTime();
        Employee employee4 = new Employee("Максимов", "Игорь", "Сергеевич", birthday4,
                4014, 712945, position4,
                111, "Доцент", "Кандидат наук", "test@domain.com",
                startDate4, endDate4);

        //Добавляем нового сотрудника в штат с помощью метода addEmployee
        this.staff.addEmployee(employee4);

        //Проверяем, что количество сотрудников стало равно четырем
        Assertions.assertEquals(4, this.staff.getEmployees().size());

        //Удаляем четвертого сотрудника из штата с помощью метода deleteEmployee
        this.staff.deleteEmployee(employee4);

        //Проверяем, что количество сотрудников стало равно трем
        Assertions.assertEquals(3, this.staff.getEmployees().size());

    }

    /* Тесты метода поиска searchEmployeesByFullName */
    @Test
    @DisplayName("Тест: поиск сотрудника с именем Сергеев Владислав Сергеевич должен вернуть сет из двух сотрудников")
    public void TestThat_SearchEmployeesByFullName_ReturnsSeveralEmployees_IfTheyHaveEqualName() {
        //arrange
        String lastName = "Сергеев";
        String firstName = "Владислав";
        String middleName = "Сергеевич";

        //act
        Collection<Employee> employees = this.staff.searchEmployeesByFullName(lastName, firstName, middleName);

        //assert
        Assertions.assertEquals(2, employees.size());
    }

    @Test
    @DisplayName("Тест: поиск сотрудника с именем Максимов Игорь Сергеевич должен вернуть сет из одного сотрудника")
    public void TestThat_SearchEmployeesByFullName_ReturnsOneEmployee_IfHeHasUniqueNameInStaff() {
        //arrange
        String lastName = "Максимов";
        String firstName = "Игорь";
        String middleName = "Сергеевич";

        //act
        Collection<Employee> employees = this.staff.searchEmployeesByFullName(lastName, firstName, middleName);

        //assert
        Assertions.assertEquals(1, employees.size());
    }

    @Test
    @DisplayName("Тест: поиск сотрудника по имени, которого нет в штате, вернет null")
    public void TestThat_SearchEmployeesByFullName_ReturnsNull_IfStaffContainsNoEmployeesWithSuchName() {
        //arrange
        String lastName = "Максимов";
        String firstName = "Николай";
        String middleName = "Сергеевич";

        //act
        Collection<Employee> employees = this.staff.searchEmployeesByFullName(lastName, firstName, middleName);

        //assert
        Assertions.assertNull(employees);
    }

    /* Тесты метода поиска searchEmployeeByPersonnelNumber */
    @Test
    @DisplayName("Тест: поиск сотрудника с табельным номером 114 должен вернуть найденного сотрудника")
    public void TestThat_SearchEmployeeByPersonnelNumber114_ReturnsCorrectEmployee() {
        //arrange
        int personnelNumber = 114;

        //act
        Employee employee = this.staff.searchEmployeeByPersonnelNumber(personnelNumber);

        //assert
        Assertions.assertEquals(114, employee.getPersonnelNumber());
        Assertions.assertEquals(4058, employee.getPassportSeries());
        Assertions.assertEquals(417812, employee.getPassportNumber());
    }

    @Test
    @DisplayName("Тест: поиск сотрудника с табельным номером, которого нет ни у одного сотрудник в штате, вернет null")
    public void TestThat_SearchEmployeeByPersonnelNumber_ReturnsNull_IfNoEmployeeWithSuchNumber() {
        //arrange
        int personnelNumber = 119;

        //act
        Employee employee = this.staff.searchEmployeeByPersonnelNumber(personnelNumber);

        //assert
        Assertions.assertNull(employee);
    }

    /* Тесты метода поиска searchEmployeeByPassportData */
    @Test
    @DisplayName("Тест: поиск сотрудника с паспортными данными 4058 417812 должен вернуть найденного сотрудника")
    public void TestThat_SearchEmployeeByPassportData_ReturnsCorrectEmployee() {
        //arrange
        int passportSeries = 4058;
        int passportNumber = 417812;

        //act
        Employee employee = this.staff.searchEmployeeByPassportData(passportSeries, passportNumber);

        //assert
        Assertions.assertEquals(114, employee.getPersonnelNumber());
        Assertions.assertEquals(4058, employee.getPassportSeries());
        Assertions.assertEquals(417812, employee.getPassportNumber());
    }

    @Test
    @DisplayName("Тест: поиск сотрудника с паспортными данными 4060 417812 должен вернуть null")
    public void TestThat_SearchEmployeeByPassportData_ReturnsNull_IfNoEmployeeWithSuchPassportData() {
        //arrange
        int passportSeries = 4060;
        int passportNumber = 417812;

        //act
        Employee employee = this.staff.searchEmployeeByPassportData(passportSeries, passportNumber);

        //assert
        Assertions.assertNull(employee);
    }

    @AfterEach
    void tearDown() {
        staff.getEmployees().clear();
    }

}
