package com.institutes.personnel.modelTest;

import com.institutes.personnel.model.Division;
import com.institutes.personnel.model.Employee;
import com.institutes.personnel.model.Position;
import org.junit.jupiter.api.*;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;

//When using this mode, a new test instance will be created for each test method or test factory method.
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class EmployeeTests {

    public Employee employee;

    /* Тесты на проверку дат */
    @Test
    @DisplayName("Тест: попытка создания сотрудника с contractStartDate == null вызовет RuntimeException")
    public void CreatingEmployee_With_ContractStartDateNull_ThrowsException() {

        //arrange
        /* Методы класса Date являются deprecated. Для работы с Date используется GregorianCalendar
        *  метод getTime() возвращает значение в миллисекундах от January 1, 1970, 0:00:00 GMT.*/

        /* Конструктор GregorianCalendar принимает год, месяц (от 0 до 11, 0 - январь, 11 - декабрь), день месяца, час, минута, секунда */
        //Date startDate = new GregorianCalendar(2013,0,25,13,24,56).getTime();
        Date startDate = null;
        Date endDate = new GregorianCalendar(2018,0,25).getTime();
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        Division division1 = new Division("0101", "First division", new LinkedHashSet<Position>());
        Position currentPosition = new Position("Преподаватель", 50, 30, division1, new LinkedHashSet<Employee>());

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            employee = new Employee("Сергеев", "Владислав", "Сергеевич", birthday, 4014, 712945, currentPosition,
                111, "Доцент", "Кандидат наук", "test@domain.com", startDate, endDate);
        });

    }

    @Test
    @DisplayName("Тест: попытка создания сотрудника с contractStartDate > contractExpirationDate вызовет RuntimeException")
    public void CreatingEmployee_With_ContractStartDateMoreThanContractExpirationDate_ThrowsException() {

        //arrange
        /* Конструктор GregorianCalendar принимает год, месяц (от 0 до 11, 0 - январь, 11 - декабрь), день месяца, час, минута, секунда */
        Date startDate = new GregorianCalendar(2018,1,19).getTime();
        Date endDate = new GregorianCalendar(2018,0,25).getTime();
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        Division division1 = new Division("0101", "First division", new LinkedHashSet<Position>());
        Position currentPosition = new Position("Преподаватель", 50, 30, division1, new LinkedHashSet<Employee>());

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            employee = new Employee("Сергеев", "Владислав", "Сергеевич", birthday, 4014, 712945, currentPosition,
                    111, "Доцент", "Кандидат наук", "test@domain.com", startDate, endDate);
        });

    }

    @Test
    @DisplayName("Тест: попытка создания сотрудника с contractStartDate != null и contractExpirationDate == null" +
            "приведет к успешному созданию сотрудника")
    public void CreatingEmployee_With_ContractStartDateNotNull_AndContractExpirationDateNull_CompletesSuccessfully() {

        //arrange
        /* Конструктор GregorianCalendar принимает год, месяц (от 0 до 11, 0 - январь, 11 - декабрь), день месяца, час, минута, секунда */
        Date startDate = new GregorianCalendar(2018,1,19).getTime();
        Date endDate = null;
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        Division division1 = new Division("0101", "First division", new LinkedHashSet<Position>());
        Position currentPosition = new Position("Преподаватель", 50, 30, division1, new LinkedHashSet<Employee>());

        //act
        employee = new Employee("Сергеев", "Владислав", "Сергеевич", birthday, 4014, 712945, currentPosition,
                111, "Доцент", "Кандидат наук", "test@domain.com", startDate, endDate);

        //assert
        Assertions.assertEquals(4014, employee.getPassportSeries());
        Assertions.assertEquals(712945, employee.getPassportNumber());
        Assertions.assertEquals(111, employee.getPersonnelNumber());

    }

    @Test
    @DisplayName("Тест: попытка создания сотрудника с contractStartDate < contractExpirationDate" +
            "приведет к успешному созданию сотрудника")
    public void CreatingEmployee_With_ContractStartDate_LessThan_ContractExpirationDate_CompletesSuccessfully() {

        //arrange
        /* Конструктор GregorianCalendar принимает год, месяц (от 0 до 11, 0 - январь, 11 - декабрь), день месяца, час, минута, секунда */
        Date startDate = new GregorianCalendar(2018,1,19).getTime();
        Date endDate = new GregorianCalendar(2018,7,19).getTime();
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        Division division1 = new Division("0101", "First division", new LinkedHashSet<Position>());
        Position currentPosition = new Position("Преподаватель", 50, 30, division1, new LinkedHashSet<Employee>());

        //act
        employee = new Employee("Сергеев", "Владислав", "Сергеевич", birthday, 4014, 712945, currentPosition,
                111, "Доцент", "Кандидат наук", "test@domain.com", startDate, endDate);

        //assert
        Assertions.assertEquals(4014, employee.getPassportSeries());
        Assertions.assertEquals(712945, employee.getPassportNumber());
        Assertions.assertEquals(111, employee.getPersonnelNumber());

    }

    /* Тесты на проверку корректности Научных званий, Научных степеней */
    @Test
    @DisplayName("Тест: попытка создания сотрудника с существующими scientificTitle и scienceDegree" +
            "приведет к успешному созданию сотрудника")
    public void CreatingEmployee_WithExistent_ScientificTitleAndScienceDegree_CompletesSuccessfully() {
        //arrange
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date endDate = new GregorianCalendar(2018,7,19).getTime();
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        Division division1 = new Division("0101", "First division", new LinkedHashSet<Position>());
        Position currentPosition = new Position("Директор", 50, 30, division1, new LinkedHashSet<Employee>());

        //act
        employee = new Employee("Сергеев", "Владислав", "Сергеевич", birthday, 4014, 712945, currentPosition,
                111, "Отсутствует", "Отсутствует", "test@domain.com", startDate, endDate);

        //assert
        Assertions.assertEquals(4014, employee.getPassportSeries());
        Assertions.assertEquals(712945, employee.getPassportNumber());
        Assertions.assertEquals(111, employee.getPersonnelNumber());
    }

    @Test
    @DisplayName("Тест: попытка создания сотрудника с scientificTitle и scienceDegree null" +
            "вызовет RuntimeException")
    public void CreatingEmployee_With_ScientificTitleNullOrScienceDegreeNull_ThrowsException() {
        //arrange
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date endDate = new GregorianCalendar(2018,7,19).getTime();
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        Division division1 = new Division("0101", "First division", new LinkedHashSet<Position>());
        Position currentPosition = new Position("Преподаватель", 50, 30, division1, new LinkedHashSet<Employee>());

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            employee = new Employee("Сергеев", "Владислав", "Сергеевич", birthday, 4014, 712945, currentPosition,
                    111, null, null, "test@domain.com", startDate, endDate);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания сотрудника с некорректным scientificTitle вызовет RuntimeException")
    public void CreatingEmployee_With_IncorrectScientificTitle_ThrowsException() {
        //arrange
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date endDate = new GregorianCalendar(2018,7,19).getTime();
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        Division division1 = new Division("0101", "First division", new LinkedHashSet<Position>());
        Position currentPosition = new Position("Преподаватель", 50, 30, division1, new LinkedHashSet<Employee>());

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            employee = new Employee("Сергеев", "Владислав", "Сергеевич", birthday, 4014, 712945, currentPosition,
                    111, "Воробей", "Кандидат наук", "test@domain.com", startDate, endDate);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания сотрудника с некорректным scientificDegree вызовет RuntimeException")
    public void CreatingEmployee_With_IncorrectScienceDegree_ThrowsException() {
        //arrange
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date endDate = new GregorianCalendar(2018,7,19).getTime();
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        Division division1 = new Division("0101", "First division", new LinkedHashSet<Position>());
        Position currentPosition = new Position("Преподаватель", 50, 30, division1, new LinkedHashSet<Employee>());

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            employee = new Employee("Сергеев", "Владислав", "Сергеевич", birthday, 4014, 712945, currentPosition,
                    111, "Доцент", "Воробей", "test@domain.com", startDate, endDate);
        });
    }

    /* Тесты на заполнение текущей должности сотрудника */
    @Test
    @DisplayName("Тест: попытка создания сотрудника без должности вызовет RuntimeException")
    public void CreatingEmployee_With_CurrentPositionNull_ThrowsException() {
        //arrange
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date endDate = new GregorianCalendar(2018,7,19).getTime();
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            employee = new Employee("Сергеев", "Владислав", "Сергеевич", birthday, 4014, 712945, null,
                    111, "Доцент", "Кандидат наук", "test@domain.com", startDate, endDate);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания сотрудника с корректной должностью приведет к успешному созданию сотрудника")
    public void CreatingEmployee_With_CorrectCurrentPosition_CompletesSuccessfully() {
        //arrange
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date endDate = new GregorianCalendar(2018,7,19).getTime();
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        Division division1 = new Division("0101", "First division", new LinkedHashSet<Position>());
        Position currentPosition = new Position("Преподаватель", 50, 30, division1, new LinkedHashSet<Employee>());

        //act
        employee = new Employee("Сергеев", "Владислав", "Сергеевич", birthday, 4014, 712945, currentPosition,
                111, "Отсутствует", "Отсутствует", "test@domain.com", startDate, endDate);

        //assert
        Assertions.assertEquals(4014, employee.getPassportSeries());
        Assertions.assertEquals(712945, employee.getPassportNumber());
        Assertions.assertEquals(111, employee.getPersonnelNumber());
    }

    /* Тесты на табельный номер */
    @Test
    @DisplayName("Тест: попытка создания сотрудника с некорректным табельным номером вызовет RuntimeException")
    public void CreatingEmployee_With_IncorrectPersonnelNumber_ThrowsException() {
        //arrange
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date endDate = new GregorianCalendar(2018,7,19).getTime();
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        Division division1 = new Division("0101", "First division", new LinkedHashSet<Position>());
        Position currentPosition = new Position("Преподаватель", 50, 30, division1, new LinkedHashSet<Employee>());

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            employee = new Employee("Сергеев", "Владислав", "Сергеевич", birthday, 4014, 712945, currentPosition,
                    -999, "Доцент", "Кандидат наук", "test@domain.com", startDate, endDate);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания сотрудника с корректным табельным номером приведет к успешному созданию сотрудника")
    public void CreatingEmployee_With_CorrectPersonnelNumber_CompletesSuccessfully() {
        //arrange
        Date startDate = new GregorianCalendar(2018,0,21).getTime();
        Date endDate = new GregorianCalendar(2018,7,19).getTime();
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        Division division1 = new Division("0101", "First division", new LinkedHashSet<Position>());
        Position currentPosition = new Position("Преподаватель", 50, 30, division1, new LinkedHashSet<Employee>());

        //act
        employee = new Employee("Сергеев", "Владислав", "Сергеевич", birthday, 4014, 712945, currentPosition,
                111, "Отсутствует", "Отсутствует", "test@domain.com", startDate, endDate);

        //assert
        Assertions.assertEquals(4014, employee.getPassportSeries());
        Assertions.assertEquals(712945, employee.getPassportNumber());
        Assertions.assertEquals(111, employee.getPersonnelNumber());
    }

    /** upd. Логика была изменена. Связывание происходит через методы родительских классов **/
    @Disabled
    @Test
    @DisplayName("Тест: после успешного создания Сотрудника, Должность, на которую ссылаеся currentPosition, должна " +
            "в массиве position.employees содержать нашего Сотрудника")
    public void CreatingEmployee_With_CorrectArguments_CompletesSuccessfully_AndCurrentPositionHasThisEmployeeInItsArrayOfEmployees() {
        //arrange
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date endDate = new GregorianCalendar(2018,7,19).getTime();
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        Division division1 = new Division("0101", "First division", new LinkedHashSet<Position>());
        Position currentPosition = new Position("Преподаватель", 50, 30, division1, new LinkedHashSet<Employee>());

        //act
        employee = new Employee("Сергеев", "Владислав", "Сергеевич", birthday, 4014, 712945, currentPosition,
                111, "Отсутствует", "Отсутствует", "test@domain.com", startDate, endDate);

        //assert
        Assertions.assertEquals(4014, employee.getPassportSeries());
        Assertions.assertEquals(712945, employee.getPassportNumber());
        Assertions.assertEquals(111, employee.getPersonnelNumber());
        Assertions.assertTrue(employee.getCurrentPosition().getEmployees().contains(employee));
    }

    /** upd. Логика была изменена. Связывание происходит через методы родительских классов **/
    @Disabled
    @Test
    @DisplayName("Тест: при изменении Employee.currentPosition с помощью setCurrentPosition новая должность может " +
            "отличаться от текущей должности сотрудника. В этом случае необходимо удалить сотрудника из предыдущей " +
            "должности и добавить в новую. Это реализовано в setCurrentPosition")
    public void SettingDifferentPositionInEmployeeCurrentPosition_ShouldRemoveThisEmployeeFromOldCurrentPosition_AndAddToNewCurrentPosition() {
        //arrange
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date endDate = new GregorianCalendar(2018,7,19).getTime();
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        Division division1 = new Division("0101", "First division", new LinkedHashSet<Position>());
        Position oldCurrentPosition = new Position("Преподаватель", 50, 30, division1, new LinkedHashSet<Employee>());
        Position newCurrentPosition = new Position("Секретарь", 40, 10, division1, new LinkedHashSet<Employee>());

        employee = new Employee("Сергеев", "Владислав", "Сергеевич", birthday, 4014, 712945, oldCurrentPosition,
                111, "Отсутствует", "Отсутствует", "test@domain.com", startDate, endDate);

        //Проверка успешного создания
        Assertions.assertEquals(4014, employee.getPassportSeries());
        Assertions.assertEquals(712945, employee.getPassportNumber());
        Assertions.assertEquals(111, employee.getPersonnelNumber());
        Assertions.assertTrue(oldCurrentPosition.getEmployees().contains(employee));

        //Изменяем текущую должность сотрудника на другую
        employee.setCurrentPosition(newCurrentPosition);

        //Проверяем, что новой должностью сотрудника стала newCurrentPosition
        Assertions.assertEquals(newCurrentPosition, employee.getCurrentPosition());

        //Проверяем, что oldCurrentPosition больше не содержит этого сотрудника
        Assertions.assertFalse(oldCurrentPosition.getEmployees().contains(employee));

        //Проверяем, что newCurrentPosition содержит этого сотрудника
        Assertions.assertTrue(newCurrentPosition.getEmployees().contains(employee));
    }

}
