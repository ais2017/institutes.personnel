package com.institutes.personnel.modelTest;

import com.institutes.personnel.model.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Date;
import java.util.GregorianCalendar;

//When using this mode, a new test instance will be created for each test method or test factory method.
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class PersonTests {

    /* Тесты на not null */
    @Test
    @DisplayName("Тест: попытка создания Person с lastName == null вызовет RuntimeException")
    public void CreatingPerson_With_LastNameNull_ThrowsException() {

        //arrange
        /* Методы класса Date являются deprecated. Для работы с Date используется GregorianCalendar
         *  метод getTime() возвращает значение в миллисекундах от January 1, 1970, 0:00:00 GMT.*/

        /* Конструктор GregorianCalendar принимает год, месяц (от 0 до 11, 0 - январь, 11 - декабрь), день месяца, час, минута, секунда */
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Person person = new Person(null, "Святослав", "Сергеевич", birthday, 1001, 100001);
        });

    }

    @Test
    @DisplayName("Тест: попытка создания Person с firstName == null вызовет RuntimeException")
    public void CreatingPerson_With_FirstNameNull_ThrowsException() {
        //arrange
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Person person = new Person("Григорьев", null, "Сергеевич", birthday, 1001, 100001);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания Person с middleName == null вызовет RuntimeException")
    public void CreatingPerson_With_MiddleNameNull_ThrowsException() {
        //arrange
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Person person = new Person("Григорьев", "Святослав", null, birthday, 1001, 100001);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания Person с birthday == null вызовет RuntimeException")
    public void CreatingPerson_With_BirthdayNull_ThrowsException() {
        //arrange
        Date birthday = null;

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Person person = new Person("Григорьев", null, "Сергеевич", birthday, 1001, 100001);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания Person с lastName == '' вызовет RuntimeException")
    public void CreatingPerson_With_LastNameEmpty_ThrowsException() {
        //arrange
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Person person = new Person("", "Святослав", "Сергеевич", birthday, 1001, 100001);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания Person с firstName == '' вызовет RuntimeException")
    public void CreatingPerson_With_FirstNameEmpty_ThrowsException() {
        //arrange
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Person person = new Person("Григорьев", "", "Сергеевич", birthday, 1001, 100001);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания Person с middleName == '' вызовет RuntimeException")
    public void CreatingPerson_With_MiddleNameEmpty_ThrowsException() {
        //arrange
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Person person = new Person("Григорьев", "Святослав", "", birthday, 1001, 100001);
        });
    }

    /* Тесты на паспортные данные */
    @Test
    @DisplayName("Тест: попытка создания Person с passportSeries < 1000 вызовет RuntimeException")
    public void CreatingPerson_With_PassportSeriesLessThan1000_ThrowsException() {
        //arrange
        int passportSeries = 999;
        int passportNumber = 712549;
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Person person = new Person("Григорьев", "Святослав", "Сергеевич", birthday, passportSeries, passportNumber);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания Person с passportSeries > 9999 вызовет RuntimeException")
    public void CreatingPerson_With_PassportSeriesMoreThan9999_ThrowsException() {
        //arrange
        int passportSeries = 10000;
        int passportNumber = 712549;
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Person person = new Person("Григорьев", "Святослав", "Сергеевич", birthday, passportSeries, passportNumber);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания Person с passportNumber < 100000 вызовет RuntimeException")
    public void CreatingPerson_With_PassportNumberLessThan100000_ThrowsException() {
        //arrange
        int passportSeries = 4014;
        int passportNumber = 99999;
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Person person = new Person("Григорьев", "Святослав", "Сергеевич", birthday, passportSeries, passportNumber);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания Person с passportNumber > 999999 вызовет RuntimeException")
    public void CreatingPerson_With_PassportNumberMoreThan999999_ThrowsException() {
        //arrange
        int passportSeries = 4014;
        int passportNumber = 1000000;
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Person person = new Person("Григорьев", "Святослав", "Сергеевич", birthday, passportSeries, passportNumber);
        });
    }

    /* Тест на корректное создание сотрудника */
    @Test
    @DisplayName("Тест: попытка создания Person с требуемыми параметрами приведет к успешному созданию")
    public void CreatingPerson_With_RequiredParameters_CompletesSuccessfully() {
        //arrange
        String lastName = "Григорьев";
        String firstName = "Святослав";
        String middleName = "Сергеевич";
        int passportSeries = 4014;
        int passportNumber = 829314;
        Date birthday = new GregorianCalendar(1989,10,1).getTime();

        //act
        Person person = new Person(lastName, firstName, middleName, birthday, passportSeries, passportNumber);

        //assert
        Assertions.assertEquals("Григорьев", person.getLastName());
        Assertions.assertEquals("Святослав", person.getFirstName());
        Assertions.assertEquals(4014, person.getPassportSeries());
        Assertions.assertEquals(829314, person.getPassportNumber());
    }

}
