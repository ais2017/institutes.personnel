package com.institutes.personnel.modelTest;

import com.institutes.personnel.model.Division;
import com.institutes.personnel.model.Employee;
import com.institutes.personnel.model.Position;
import org.junit.jupiter.api.*;

import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;

//When using this mode, a new test instance will be created for each test method or test factory method.
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class PositionTests {

    /* Тесты конструктора Position */
    @Test
    @DisplayName("Тест: попытка создания Position с positionName == null вызовет RuntimeException")
    public void CreatingPosition_With_PositionNameNull_ThrowsException() {
        //arrange
        String positionName = null;
        int maxEmployees = 10;
        int vacantPositions = 10;
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());;
        Collection<Employee> employees = new LinkedHashSet<Employee>();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Position position = new Position(positionName, maxEmployees, vacantPositions, division, employees);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания Position с positionName == '' вызовет RuntimeException")
    public void CreatingPosition_With_PositionNameEmpty_ThrowsException() {
        //arrange
        String positionName = "";
        int maxEmployees = 10;
        int vacantPositions = 10;
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());;
        Collection<Employee> employees = new LinkedHashSet<Employee>();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Position position = new Position(positionName, maxEmployees, vacantPositions, division, employees);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания Position с maxEmployees < 1 вызовет RuntimeException")
    public void CreatingPosition_With_MaxEmployeesLessThanOne_ThrowsException() {
        //arrange
        String positionName = "Преподаватель";
        int maxEmployees = 0;
        int vacantPositions = 0;
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());;
        Collection<Employee> employees = new LinkedHashSet<Employee>();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Position position = new Position(positionName, maxEmployees, vacantPositions, division, employees);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания Position с vacantPositions < 0 вызовет RuntimeException")
    public void CreatingPosition_With_VacantPositionsLessThanZero_ThrowsException() {
        //arrange
        String positionName = "Преподаватель";
        int maxEmployees = 1;
        int vacantPositions = -1;
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());;
        Collection<Employee> employees = new LinkedHashSet<Employee>();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Position position = new Position(positionName, maxEmployees, vacantPositions, division, employees);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания Position с vacantPositions > maxEmployees вызовет RuntimeException")
    public void CreatingPosition_With_VacantPositionsMoreThanMaxEmployees_ThrowsException() {
        //arrange
        String positionName = "Преподаватель";
        int maxEmployees = 10;
        int vacantPositions = 11;
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());;
        Collection<Employee> employees = new LinkedHashSet<Employee>();

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Position position = new Position(positionName, maxEmployees, vacantPositions, division, employees);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания Position с employees = null вызовет RuntimeException")
    public void CreatingPosition_With_EmployeesNull_ThrowsException() {
        //arrange
        String positionName = "Преподаватель";
        int maxEmployees = 10;
        int vacantPositions = 10;
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());;
        Collection<Employee> employees = null;

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            Position position = new Position(positionName, maxEmployees, vacantPositions, division, employees);
        });
    }

    @Test
    @DisplayName("Тест: попытка создания Position с правильными параметрами приведет к успешному созданию объекта")
    public void CreatingPosition_With_CorrectArguments_CompletesSuccessfully() {
        //arrange
        String positionName = "Преподаватель";
        int maxEmployees = 10;
        int vacantPositions = 10;
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());;
        Collection<Employee> employees = new LinkedHashSet<Employee>();

        //act
        Position position = new Position(positionName, maxEmployees, vacantPositions, division, employees);

        //assert
        Assertions.assertEquals("Преподаватель", position.getPositionName());
        Assertions.assertEquals(10, position.getMaxEmployees());
        Assertions.assertEquals(10, position.getVacantPositions());
        Assertions.assertEquals("0101", position.getParentDivision().getDivisionCode());
        Assertions.assertEquals("First Institute Division", position.getParentDivision().getDivisionName());
    }

    /* Тесты сеттеров maxEmployees и vacantPositions */
    @Test
    @DisplayName("Попытка изменения maxEmployees на величину < 1 вызовет RuntimeException")
    public void SettingMaxEmployeesLessThanOne_ThrowsException() {
        //arrange
        String positionName = "Преподаватель";
        int maxEmployees = 10;
        int vacantPositions = 10;
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());;
        Collection<Employee> employees = new LinkedHashSet<Employee>();

        Position position = new Position(positionName, maxEmployees, vacantPositions, division, employees);

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            position.setMaxEmployees(0);
        });
    }

    @Test
    @DisplayName("Попытка изменения vacantPositions на величину < 0 вызовет RuntimeException")
    public void SettingVacantPositionsLessThanZero_ThrowsException() {
        //arrange
        String positionName = "Преподаватель";
        int maxEmployees = 10;
        int vacantPositions = 10;
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());;
        Collection<Employee> employees = new LinkedHashSet<Employee>();

        Position position = new Position(positionName, maxEmployees, vacantPositions, division, employees);

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            position.setVacantPositions(-1);
        });
    }

    @Test
    @DisplayName("Попытка изменения maxEmployees на величину < vacantPositions вызовет RuntimeException")
    public void SettingMaxEmployeesLessThanVacantPositions_ThrowsException() {
        //arrange
        String positionName = "Преподаватель";
        int maxEmployees = 10;
        int vacantPositions = 10;
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());;
        Collection<Employee> employees = new LinkedHashSet<Employee>();

        Position position = new Position(positionName, maxEmployees, vacantPositions, division, employees);

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            position.setMaxEmployees(6);
        });
    }

    @Test
    @DisplayName("Попытка изменения vacantPositions на величину > maxEmployees вызовет RuntimeException")
    public void SettingVacantPositionsMoreThanMaxEmployees_ThrowsException() {
        //arrange
        String positionName = "Преподаватель";
        int maxEmployees = 10;
        int vacantPositions = 10;
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());;
        Collection<Employee> employees = new LinkedHashSet<Employee>();

        Position position = new Position(positionName, maxEmployees, vacantPositions, division, employees);

        //act, assert
        Assertions.assertThrows(RuntimeException.class, ()-> {
            position.setVacantPositions(14);
        });
    }

    @Test
    @DisplayName("Попытка изменения maxEmployees на корректную величину приведет к успешному изменению")
    public void SettingMaxEmployeesWithCorrectValue_CompletesSuccessfully() {
        //arrange
        String positionName = "Преподаватель";
        int maxEmployees = 10;
        int vacantPositions = 10;
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());;
        Collection<Employee> employees = new LinkedHashSet<Employee>();

        Position position = new Position(positionName, maxEmployees, vacantPositions, division, employees);

        //act
        position.setMaxEmployees(14);

        //assert
        Assertions.assertEquals(14, position.getMaxEmployees());
    }

    /** upd. Логика была изменена. Связывание происходит через методы родительских классов **/
    @Disabled
    @Test
    @DisplayName("Тест: после успешного создания Сотрудника, Должность, на которую ссылаеся currentPosition, должна " +
            "в массиве position.employees содержать нашего Сотрудника")
    public void CreatingPosition_With_CorrectArguments_CompletesSuccessfully_AndParentDivisionHasThisPositionInItsArrayOfPositions() {
        //arrange
        String positionName = "Преподаватель";
        int maxEmployees = 10;
        int vacantPositions = 10;
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());;
        Collection<Employee> employees = new LinkedHashSet<Employee>();

        Position position = new Position(positionName, maxEmployees, vacantPositions, division, employees);

        //act
        position.setMaxEmployees(14);

        //assert
        Assertions.assertEquals(14, position.getMaxEmployees());
        Assertions.assertTrue(position.getParentDivision().getPositions().contains(position));
    }

    /** upd. Логика была изменена. Связывание происходит через методы родительских классов **/
    @Disabled
    @Test
    @DisplayName("Тест: при изменении position.currentDivision с помощью setCurrentDivision новое подразделение может " +
            "отличаться от текущего подразделения должности. В этом случае необходимо удалить должность из предыдущего " +
            "подразделения и добавить в новое. Это реализовано в setParentDivision")
    public void SettingDifferentPositionInEmployeeCurrentPosition_ShouldRemoveThisEmployeeFromOldCurrentPosition_AndAddToNewCurrentPosition() {
        //arrange
        String positionName = "Преподаватель";
        int maxEmployees = 10;
        int vacantPositions = 10;
        Division oldParentDivision = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());
        Division newParentDivision = new Division("0102", "Second Institute Division", new LinkedHashSet<Position>());
        Collection<Employee> employees = new LinkedHashSet<Employee>();

        Position position = new Position(positionName, maxEmployees, vacantPositions, oldParentDivision, employees);

        //Проверка успешного создания
        Assertions.assertTrue(oldParentDivision.getPositions().contains(position));

        //Изменяем текущее подразделение должности на другое
        position.setParentDivision(newParentDivision);

        //Проверяем, что новой должностью сотрудника стала newParentDivision
        Assertions.assertEquals(newParentDivision, position.getParentDivision());

        //Проверяем, что oldParentDivision больше не содержит эту должность
        Assertions.assertFalse(oldParentDivision.getPositions().contains(position));

        //Проверяем, что newParentDivision содержит эту должность
        Assertions.assertTrue(newParentDivision.getPositions().contains(position));
    }

    /* Тесты метода поиска searchEmployeesByFullName */
 /*   @Test
    @DisplayName("Тест: поиск сотрудника с именем Сергеев Владислав Сергеевич должен вернуть сет из двух сотрудников")
    public void TestThat_SearchEmployeesByFullName_ReturnsSeveralEmployees_IfTheyHaveEqualName() {
        //arrange
        String lastName = "Сергеев";
        String firstName = "Владислав";
        String middleName = "Сергеевич";

        //act
        Collection<Employee> employees = this.staff.searchEmployeesByFullName(lastName, firstName, middleName);

        //assert
        Assertions.assertEquals(2, employees.size());
    }

    @Test
    @DisplayName("Тест: поиск сотрудника с именем Максимов Игорь Сергеевич должен вернуть сет из одного сотрудника")
    public void TestThat_SearchEmployeesByFullName_ReturnsOneEmployee_IfHeHasUniqueNameInStaff() {
        //arrange
        String lastName = "Максимов";
        String firstName = "Игорь";
        String middleName = "Сергеевич";

        //act
        Collection<Employee> employees = this.staff.searchEmployeesByFullName(lastName, firstName, middleName);

        //assert
        Assertions.assertEquals(1, employees.size());
    }

    @Test
    @DisplayName("Тест: поиск сотрудника по имени, которого нет в штате, вернет null")
    public void TestThat_SearchEmployeesByFullName_ReturnsNull_IfStaffContainsNoEmployeesWithSuchName() {
        //arrange
        String lastName = "Максимов";
        String firstName = "Николай";
        String middleName = "Сергеевич";

        //act
        Collection<Employee> employees = this.staff.searchEmployeesByFullName(lastName, firstName, middleName);

        //assert
        Assertions.assertNull(employees);
    } */

    /* Тесты метода поиска searchEmployeeByPersonnelNumber */
 /*   @Test
    @DisplayName("Тест: поиск сотрудника с табельным номером 114 должен вернуть найденного сотрудника")
    public void TestThat_SearchEmployeeByPersonnelNumber114_ReturnsCorrectEmployee() {
        //arrange
        int personnelNumber = 114;

        //act
        Employee employee = this.staff.searchEmployeeByPersonnelNumber(personnelNumber);

        //assert
        Assertions.assertEquals(114, employee.getPersonnelNumber());
        Assertions.assertEquals(4058, employee.getPassportSeries());
        Assertions.assertEquals(417812, employee.getPassportNumber());
    }

    @Test
    @DisplayName("Тест: поиск сотрудника с табельным номером, которого нет ни у одного сотрудник в штате, вернет null")
    public void TestThat_SearchEmployeeByPersonnelNumber_ReturnsNull_IfNoEmployeeWithSuchNumber() {
        //arrange
        int personnelNumber = 119;

        //act
        Employee employee = this.staff.searchEmployeeByPersonnelNumber(personnelNumber);

        //assert
        Assertions.assertNull(employee);
    } */

    /* Тесты метода поиска searchEmployeeByPassportData */
/*    @Test
    @DisplayName("Тест: поиск сотрудника с паспортными данными 4058 417812 должен вернуть найденного сотрудника")
    public void TestThat_SearchEmployeeByPassportData_ReturnsCorrectEmployee() {
        //arrange
        int passportSeries = 4058;
        int passportNumber = 417812;

        //act
        Employee employee = this.staff.searchEmployeeByPassportData(passportSeries, passportNumber);

        //assert
        Assertions.assertEquals(114, employee.getPersonnelNumber());
        Assertions.assertEquals(4058, employee.getPassportSeries());
        Assertions.assertEquals(417812, employee.getPassportNumber());
    }

    @Test
    @DisplayName("Тест: поиск сотрудника с паспортными данными 4060 417812 должен вернуть null")
    public void TestThat_SearchEmployeeByPassportData_ReturnsNull_IfNoEmployeeWithSuchPassportData() {
        //arrange
        int passportSeries = 4060;
        int passportNumber = 417812;

        //act
        Employee employee = this.staff.searchEmployeeByPassportData(passportSeries, passportNumber);

        //assert
        Assertions.assertNull(employee);
    } */

    /*

    @Test
    @DisplayName("Попытка создания объекта с корректными maxEmployees и vacantPositions" +
            "приведет к созданию объекта")
    public void CreatingPositionWithCorrectMaxEmployeesAndVacantPositions_CompletesSuccessfully() {
        //arrange
        String positionName = "Преподаватель";
        int maxEmployees = 10;
        int vacantPositions = 8;

        //act

        //assert
        Assertions.assertEquals("Преподаватель", positionInStaffList.getPositionName());
        Assertions.assertEquals(10, positionInStaffList.getMaxEmployees());
        Assertions.assertEquals(8, positionInStaffList.getVacantPositions());
    }

    @Test
    @DisplayName("Тест: попытка создания Position с корректным positionName приведет к успешному созданию должности")
    public void CreatingPosition_With_CorrectPositionName_ThrowsException() {
        //arrange
        String positionName = "Преподаватель";

        //act
        Position position = new Position(positionName);

        // assert
        Assertions.assertEquals("Преподаватель", position.getPositionName());
    } */

}
