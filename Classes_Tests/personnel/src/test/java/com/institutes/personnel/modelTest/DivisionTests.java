package com.institutes.personnel.modelTest;

import com.institutes.personnel.model.Division;
import com.institutes.personnel.model.Employee;
import com.institutes.personnel.model.Position;
import javafx.geometry.Pos;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.LinkedHashSet;

//When using this mode, a new test instance will be created for each test method or test factory method.
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class DivisionTests {

    /* Проверки на добавление/удаление элементов Position */
    @Test
    @DisplayName("Тест: добавление элемента Position в сет Division.positions, в котором" +
            "было 0 элементов, сделает его размер равным 1")
    public void AddPosition_To_Empty_Division_ReturnSizeOne() {
        //arrange
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());
        Position position = new Position("Преподаватель", 50, 30, division, new LinkedHashSet<Employee>());

        //act
        division.addPosition(position);

        //assert
        Assertions.assertEquals(1, division.getPositions().size());
    }

    @Test
    @DisplayName("Тест: Удаление элемента Position из сета Division.positions" +
            "размером 1, сделает его размер равным 0")
    public void RemovingExistingPosition_FromDivisionWithSizeOne_ReturnsSizeZero() {
        //arrange
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());
        Position position = new Position("Преподаватель", 50, 30, division, new LinkedHashSet<Employee>());

        //act
        division.addPosition(position);
        division.deletePosition(position);

        //assert
        Assertions.assertEquals(0, division.getPositions().size());
    }

    /* Проверки на методы поиска */
    @Test
    @DisplayName("Тест: успешный поиск должен вернуть объект класса Position" +
            "(Поиск по имени должности, которая есть в сете Division.positions")
    public void SearchByExistingPositionName_ReturnsThisPosition() {
        //arrange
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());
        Position position1 = new Position("Преподаватель", 50, 30, division, new LinkedHashSet<Employee>());
        Position position2 = new Position("Директор", 50, 30, division, new LinkedHashSet<Employee>());
        Position position3 = new Position("Секретарь", 50, 30, division, new LinkedHashSet<Employee>());

        division.addPosition(position1);
        division.addPosition(position2);
        division.addPosition(position3);

        //Проверка успешного предусловия
        Assertions.assertEquals(3, division.getPositions().size());

        //act
        Position foundPosition = division.getPositionByName("Секретарь");

        //assert
        Assertions.assertEquals(position3, foundPosition);
    }

    @Test
    @DisplayName("Тест: неуспешный поиск должен вернуть null")
    public void SearchByNonExistingPositionName_ReturnsNull() {
        //arrange
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());
        Position position1 = new Position("Преподаватель", 50, 30, division, new LinkedHashSet<Employee>());
        Position position2 = new Position("Директор", 50, 30, division, new LinkedHashSet<Employee>());
        Position position3 = new Position("Секретарь", 50, 30, division, new LinkedHashSet<Employee>());

        division.addPosition(position1);
        division.addPosition(position2);
        division.addPosition(position3);

        //Проверка успешного предусловия
        Assertions.assertEquals(3, division.getPositions().size());

        //act
        Position foundPosition = division.getPositionByName("Баскетболист");

        //assert
        Assertions.assertNull(foundPosition);
    }

    @Test
    @DisplayName("Тест: поиск при Division.getPositions().size() == 0 должен вернуть null")
    public void SearchByEmptyDivision_ReturnsNull() {
        //arrange
        Division division = new Division("0101", "First Institute Division", new LinkedHashSet<Position>());

        //act
        Position foundPosition = division.getPositionByName("Баскетболист");

        //assert
        Assertions.assertNull(foundPosition);
    }
}
